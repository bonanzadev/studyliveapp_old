export function editLogin(text){
    return {
        type: 'EDIT_LOGIN',
        text
    }
}

export function editUserdatageral(state){
    return {
        type: 'EDIT_USERDATAGERAL',
        state
    }
}
export function editSaldo(state){
    return {
        type: 'EDIT_SALDO',
        state
    }
}