import React,{ Component } from 'react';
import { View, Text,Image,StyleSheet,TouchableOpacity, TextInput, ActivityIndicator} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';
import api from '../services/api';

import Avatar from '../components/avatar';
import Modal from "react-native-modal";
import moment from "moment";
import 'moment/locale/pt-br';
import Incoming from '../components/Incoming';  
import firebase from 'react-native-firebase';
import Stars from 'react-native-stars';
import Ionicons from 'react-native-vector-icons/Ionicons';
class Historico extends Component {

  state = {
    aulas: [],
    visibleModal: false,
    visibleModalAvalia:false,
    aulaAtual: '',
    incoming: false,
    incomingAulaObj: [],
    avaliacao: 0
  };

  verificaStorage = async () => {
    return await AsyncStorage.getItem('@StudyLiveApp:id') || false;
  }

  loadData = async () => {

    try{

      const id = await this.verificaStorage();
            
      const response = await api.get(`/historicoAulasAluno/${id}`);
      console.log(response)
      
      
      this.setState({
        aulas: response.data.aulas,
      },() => {
        this.renderItems()
      })
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }
  }

  componentDidMount(){
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      this.IncomingAula(notification.data.dados);
    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notification) => {
      this.IncomingAula(notification.notification.data.dados);
    });

    this.willFocus = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.loadData() 
      }
    ) 

    this.loadData()
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
    this.willFocus.remove()
  }

  IncomingAula = (obj) => {
    /*var obj = {
      id: 169,
      id_usuario_aluno: 1,
      nomeAluno: 'Mauricio Ferg',
      displayCategoria: 'Portugues',
      avatar: '1571789106',
      channel: '11A',

      assuntos: []
    }*/
    console.log("AQUI2: ",obj);
    this.setState({ 
      incomingAulaObj: obj
    },() => {
        /* Chama o Component depois de preencher as variaveis acima */
        this.setState({ incoming: true  })
      });
  }

  closeModal = () => {
    this.setState({ visibleModal: null, incoming: false, visibleModalAvalia: false, avaliacao: 0, avaliacaoMsg: '' })
  }
  openModal = (aula) => {
    this.setState({ visibleModalAvalia: true, aulaAtual: aula })
  }
  handleAvalia = async () => {
    this.setState({ isLoading: true});
    try { 
      const response = await api.post(`/avaliacaoProfessor/${this.state.aulaAtual.id}`,this.state);
      console.log("avaliacao: ",response) 
      if( response.data.affectedRows != undefined ){    

          this.setState({ isLoading: false, avaliado: true, tooltip2: "PROFESSOR AVALIADO"});

      } else {
          this.setState({ isLoading: false, errors: "Dificuldades Técnicas.."});
      }
    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, errors: "Houve um erro..."});
    }
    this.setState({ isLoading: false});
  }
  
  renderItems = () => {
    const {navigate} =this.props.navigation;
    
    if(this.state.aulas.length > 0){
      return this.state.aulas.map((item) => {

        moment.locale('pt-BR')
        
        var dt_ini = moment(item.dt_ini_data).calendar().replace("às","de");  
        
        return (
          <TouchableOpacity onPress={() => this.setState({ visibleModal: 'aulas', aulaAtual: item })  } >
            
            <View style={styles.box}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <Avatar 
                            rounded
                            size="medium"
                            source={require("../images/avatar.png")}
                            onPress={() => console.log("Works!")}
                            activeOpacity={0.7}
                        />
                    </View>
                    <View style={{ marginTop: 0, marginLeft: 10}}>
                        <Text style={styles.txt1}>
                        <Text style={styles.marcar}>{item.apelido || item.nome || 'Aguardando Professor'}</Text></Text>
                        <Text style={styles.txt1}>{item.categoria}</Text>
                        <Text style={styles.txt1}>{dt_ini} às {item.dt_fim_hora} </Text>
                    </View>    
                </View>    
                <View style={{ marginTop: 10, marginLeft: 0,borderTopWidth: 0 , borderTopColor: '#f5f5f5',paddingTop: 5,}}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style={{ flexDirection: 'row', marginTop: 0}}>
                    {item.assuntos != undefined && item.assuntos.map((assunto) => {
                        return ( 
                            <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
                                <Text style={styles.txt4}>{assunto.nome}</Text>
                            </View>
                        )
                    })}
                    </ScrollView>
                </View>    
                <View style={{ marginTop: 10,}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopColor: '#f5f5f5', borderTopWidth: 1, paddingTop: 7}}>
                        <TouchableOpacity onPress={() => null } >
                            <Text style={{ fontSize: 14, color: '#777', fontFamily: 'Montserrat-Light', }}>Avaliar Professor</Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', marginTop: 20, marginBottom: 20}}>
                            <Icon 
                                name='star'
                                size={22}
                                color='gold'
                            />
                            <Icon 
                                name='star'
                                size={22}
                                color='gold'
                            />
                            <Icon 
                                name='star'
                                size={22}
                                color='gold'
                            />
                            <Icon 
                                name='star'
                                size={22}
                                color='gold'
                            />
                            <Icon 
                                name='star-o'
                                size={22}
                                color='#e9e9e9'
                            />
                        </View>
                    </View>
                </View>
                <View style={{ width:'96%', marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' , borderTopColor: '#f5f5f5', borderTopWidth: 1, paddingTop: 10}}>
                        
                    <TouchableOpacity style={styles.btn} onPress={() => null  } >
                        <Text style={styles.btnTxt}>AJUDA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} hitSlop={{top: 30, left: 30, bottom: 30, right: 30}}  onPress={() => this.openModal(item)  } >
                        <Text style={styles.btnTxt}>AVALIAR</Text>
                    </TouchableOpacity>
                    
                    
                </View>
            </View>
            
          </TouchableOpacity>
        )
      })
    } else {
      return (
        <View style={{ 
          marginLeft: 5, 
          marginRight: 0, 
          marginTop: 20,
          }}>
            
          <TouchableOpacity style={{justifyContent: 'center',alignItems: 'center',}} onPress={() => navigate('Historico')  } >

            <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 13, color: '#777' }}>Nenhuma aula ainda...</Text>      
                  
          </TouchableOpacity>
        </View>
      )
    }
  }

  renderAssuntos = () => {
    if(this.state.aulaAtual.assuntos != undefined){
      return this.state.aulaAtual.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  
  render() {
    const {navigate} =this.props.navigation;
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f5f5'}}>
        { this.state.incoming && 
          <Incoming dadosAula={this.state.incomingAulaObj} navigation={this.props.navigation} toggleStatus={this.state.incoming} />
        }
        <Modal
        isVisible={this.state.visibleModalAvalia}
        useNativeDriver={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        onBackdropPress={() => this.closeModal() }
        >
          <View style={styles.modal}>
            <View style={{marginBottom: 10}}>
                <Text style={General.inputLabel}>Como foi a sua experiência?</Text>
                <Text style={styles.textEscolhaSub}>Escolha de 1 a 5 estrelas para classificar.</Text>
            </View>
            <Stars
                half={false}
                default={0}
                update={(val)=>{this.setState({avaliacao: val})}}
                spacing={5}
                count={5}
                fullStar={
                    <Ionicons
                        name="ios-star"
                        size={40} 
                        color={'#ffcc66'}  
                    />
                }
                emptyStar={
                    <Ionicons
                        name="ios-star-outline"
                        size={40} 
                        color={'#ffcc66'}  
                    />
                }
                halfStar={
                    <Ionicons
                        name="ios-star-half"
                        size={40} 
                        color={'#ffcc66'}  
                    />
                }
            />
            <Text style={[General.inputLabel,{marginTop: 10}]}>Deixar comentário</Text>
            <TextInput
            value={this.state.avaliacaoMsg}
            autoCorrect={false}
            onChangeText={(val)=>{this.setState({avaliacaoMsg: val})}}
            multiline={true}
            textAlignVertical={'top'}
            placeholder='Digite sua mensagem'
            style={[General.formInput,{height: 70,width: '90%', backgroundColor: 'white', borderRadius: 10}]}
            />

            <TouchableOpacity disabled={ this.state.avaliacao == 0 ? true : false } style={[styles.btnModal, {opacity: this.state.avaliacao == 0 ? 0.4 : 1}]} onPress={() => this.handleAvalia() } >
              {this.state.isLoading && <ActivityIndicator color="#fff" size="large" />}
              {!this.state.isLoading && <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 13, color: '#f1f1f1' }}>{this.state.errors || 'Avaliar'}</Text>}
                  
            </TouchableOpacity>
          </View>  
        </Modal>
        <ScrollView style={{ flex: 1, padding: 15 }}>
            
            { this.renderItems() }
            
        </ScrollView>   
        
        
      </View>
    )
  }
}

export default Historico;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center'
  },
  labelData: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 15,
  },
  content: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  content2: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  box: {
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    width: '94%', 
    minHeight: 100, 
    
    backgroundColor: '#FFF',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    borderRadius: 10,
  },
  marcar: {
    fontSize: 14,
    fontFamily: 'Montserrat-Regular',
  },
  txt1:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 2
  },
  txt2:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
  },
  btn: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 0,
    paddingBottom: 7,
    
    borderRadius: 30
  },
  modal: {
    alignItems: 'center', 
    backgroundColor: '#f5f5f5', 
    borderColor: '#f1f1f1', 
    borderWidth: 1, 
    borderRadius: 25, 
    paddingTop: 10, 
    paddingHorizontal: 10,
  },
  btnModal: {
    backgroundColor:'#2FA29A', 
    borderWidth: 1, 
    borderColor: '#00B2B2',
    borderRadius: 36,
    flexDirection: 'row',
    height: '15%',
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    shadowColor: "#999",
    shadowOpacity: 0.3,
    shadowRadius: 20,
    elevation: 1,
    shadowOffset: {
        height: 1,
        width: 1
    } 
  },
  txt3:{
    fontSize: 16,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 5
  },
  txt4:{
    fontSize: 14,
    color: '#FFF',
    fontFamily: 'Montserrat-Light'
  },
  btnTxt: {
     fontSize: 14, 
     color: '#29A9AB' ,
     fontFamily: 'Montserrat-Light',
  },
  textEscolhaSub: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    marginTop: 2,
    color: '#8b8b8b'
  }
});