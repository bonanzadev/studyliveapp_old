import React,{ Component } from 'react';
import { View, Text,Image,TextInput,BackHandler,StyleSheet,TouchableOpacity,ActivityIndicator   } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';

import Avatar from '../components/avatar';
import Modal from "react-native-modal";

import fonts from '../styles/fonts';
import FotoAvatar from '../components/FotoAvatar';
import api from '../services/api';
import AsyncStorage from '@react-native-community/async-storage';

import { connect } from 'react-redux';
import * as todoActions from '../actions/todos';
    
class Avaliacao extends Component {

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            qtdaula: "",
            totalMinutos: "",
            id_usuario_aluno: '',
            id_usuario_professor: '',
            displayCategoria: '',
            id_aula: '',
            avatarProfessor: '',
            nomeProfessor: '',
            avaliacao: 0,
            avaliacaoMsg: '',
            avaliado: false,
            tooltip2: 'FINALIZAR AVALIAÇÃO',
            visibleModal: '',
        };

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    componentDidMount(){

        //console.log('st ',this.props.todos)
        let saldoatual = this.props.todos.saldo;
        saldoFinal = saldoatual - this.state.qtdaula;

        this.setState({ saldoFinal });

        this.getDadosAula();
    }

    getDadosAula = async() => {

        try { 

            var dados = this.props.navigation.getParam('dados','');
            console.log('dados ',dados)
            this.setState({
                id_aula: dados.id_aula || 261,
            }, async () => {

                console.log("inicia busca ",this.state)
                const response = await api.get(`/loadaula/${this.state.id_aula}`,{});
                console.log("resp aula: ",response) 
                if( response.data.data != undefined ){
                let dados = response.data.data;
                let av = FotoAvatar(dados.id_usuario_professor,dados.avatarProfessor);
                let pr = dados.nomeProfessor;
                
                this.setState({ 
                    isLoading: false, 
                    avatarProfessor: av, 
                    nomeProfessor: pr, 
                    id_usuario_professor: dados.id_usuario_professor,
                    id_usuario_aluno: dados.id_usuario_aluno,
                    qtdaula: dados.qtdaula,
                    displayCategoria: dados.displayCategoria,
                    totalMinutos: (dados.qtdaula*30),
                    id_categoria: dados.id_categoria,
                    mensagem: dados.mensagem || '',
                    assuntos: dados.selectedBoxes,
                });
        
                } else {
                    this.setState({ isLoading: false, tooltip2: "Dificuldades Técnicas.."});
                }

            })
  
        } catch (response) {
            console.log(response)
            this.setState({ isLoading: false, tooltip2: "Houve um erro"});
        }
    }

    avaliarProfessor = (avaliacao) => {
        this.setState({ avaliacao });
    }

    communicate = (tooltip,time) => {
        this.setState({ tooltip,visibleModal: 'tooltip' });

        setTimeout(() => {
        this.setState({ visibleModal: '' });
        }, time);
    }

    handleSaveForm = async () => {
        if( this.state.avaliacao == 0 || this.state.avaliacaoMsg == "" ){
            this.communicate("Avalie seu professor para finalizar",3000)
        } else {
            this.setState({ isLoading: true, tooltip2: "ENVIANDO..."});
            try { 

                const response = await api.post(`/avaliacaoProfessor/${this.state.id_aula}`,this.state);
                console.log("avaliacao: ",response) 
                if( response.data.affectedRows != undefined ){    
        
                    this.setState({ isLoading: false, avaliado: true, tooltip2: "PROFESSOR AVALIADO"});

                } else {
                    this.setState({ isLoading: false, tooltip2: "Dificuldades Técnicas.."});
                }
        
            } catch (response) {
                console.log(response)
                this.setState({ isLoading: false, tooltip2: "Houve um erro"});
            }
            
        }
    }

    handleMsg = (msg) => { this.setState({ avaliacaoMsg: msg }) }

    renderModalTooltipContent = () => (
        <View style={styles.tooltip} onPress={ this.closeModal }>
        
        <ScrollView>
        <Text style={{ color: 'white', fontSize: 18, textAlign:'center'}}>{this.state.tooltip}</Text>
        </ScrollView>
        
        </View>
    );

    voltar = async () => {
        this.props.navigation.navigate('Home');
    }

    render() {
        const {navigate} =this.props.navigation;

        return (
        <View style={{ flex: 1, backgroundColor: '#f5f5f5', justifyContent: 'center', alignItems: 'center' }}>
            <Modal
                isVisible={this.state.visibleModal === 'tooltip'}
                hasBackdrop={false}
                useNativeDriver={true}
                animationIn="fadeIn"
                animationOut="fadeOut"
                onBackdropPress={() => this.closeModal() }
            >
                {this.renderModalTooltipContent()}
            </Modal>
            { this.state.avaliado && 
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Icon style={{ marginRight: 5 }}
                        name='check-square-o'
                        size={52}
                        color='#29A9AB'
                    />
                    <Text style={{ fontSize: 18, marginTop: 15, fontFamily: 'Montserrat-Bold' }}>Obrigado por usar o StudyLive</Text>
                    <View style={{ width: 180, marginTop: 15}}>
                        <TouchableOpacity style={General.submit} onPress={() => this.voltar()   } >
                            <Text style={General.submitText}>VOLTAR PARA HOME</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            }
            { this.state.isLoading && 
                <ActivityIndicator size="large" color="green"/>
            }  
            { !this.state.isLoading && !this.state.avaliado && 
                <ScrollView style={{ width:"100%", }}>
                    <View style={{ width:"100%",backgroundColor: '#FFF',  paddingLeft: 15, paddingRight: 15, paddingTop: 40, paddingBottom: 40, justifyContent: 'center', alignItems: 'center'  }}>

                        <TouchableOpacity style={{ borderWidth: 21, borderColor: 'white', borderRadius: 50, marginTop: 30, marginBottom: 40 }} >
                            <Avatar 
                                rounded
                                size="xlarge"
                                source={{uri: this.state.avatarProfessor }}
                            />
                        </TouchableOpacity> 
                    
                        <Text style={styles.name1}>{this.state.nomeProfessor}</Text>

                    </View>

                    <View style={{ width:'100%', borderTopColor: '#f0f0f0', borderTopWidth: 1, justifyContent: 'center', alignItems: 'center',}}>

                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 18, marginTop: 20 }}>Avalie seu Professor</Text>

                            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 15 }}>
                                <TouchableOpacity onPress={() => this.avaliarProfessor(1)   } >
                                    { this.state.avaliacao >= 1 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='gold'
                                        />
                                    }
                                    { this.state.avaliacao < 1 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='#CCC'
                                        />
                                    }
                                </TouchableOpacity>    
                                <TouchableOpacity onPress={() => this.avaliarProfessor(2)   } >
                                    { this.state.avaliacao >= 2 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='gold'
                                        />
                                    }
                                    { this.state.avaliacao < 2 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='#CCC'
                                        />
                                    }
                                </TouchableOpacity>    
                                <TouchableOpacity onPress={() => this.avaliarProfessor(3)   } >
                                    { this.state.avaliacao >= 3 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='gold'
                                        />
                                    }
                                    { this.state.avaliacao < 3 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='#CCC'
                                        />
                                    }
                                </TouchableOpacity>    
                                <TouchableOpacity onPress={() => this.avaliarProfessor(4)   } >
                                    { this.state.avaliacao >= 4 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='gold'
                                        />
                                    }
                                    { this.state.avaliacao < 4 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='#CCC'
                                        />
                                    }
                                </TouchableOpacity>    
                                <TouchableOpacity onPress={() => this.avaliarProfessor(5)   } >
                                    { this.state.avaliacao >= 5 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='gold'
                                        />
                                    }
                                    { this.state.avaliacao < 5 &&
                                        <Icon style={{ marginRight: 5 }}
                                            name='star'
                                            size={28}
                                            color='#CCC'
                                        />
                                    }
                                </TouchableOpacity>    
                            </View>

                            <View style={{ width: '94%', marginTop: 20 }}>
                                <Text style={General.inputLabel}>Comentários</Text>
                                <TextInput
                                value={this.state.avaliacaoMsg}
                                autoCorrect={false}
                                onChangeText={this.handleMsg}
                                returnKeyType='ok'
                                multiline={true}
                                textAlignVertical={'top'}
                                placeholder='Digite sua mensagem'
                                style={[General.formInput,{height: 80, backgroundColor: 'white'}]}
                                />
                            </View>  

                            <View style={{ marginTop: 0, width:'94%', marginLeft: 10, marginBottom: 30}}>
                                <TouchableOpacity style={General.submit} onPress={() => this.handleSaveForm()   } >
                                <Text style={General.submitText}>{this.state.tooltip2}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                        

                    </View>

                    
                    
                </ScrollView>   
            }  
            
        </View>
        )
    }
}

const mapStateToProps = state => ({
    todos: state.todos,
});

const mapDispatchToProps = {
...todoActions,
};

export default connect(mapStateToProps,mapDispatchToProps)(Avaliacao)
  


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    inputqtdaula: {
        width: '34%', 
        textAlign: 'center', 
        fontWeight: '600', 
        fontSize: 18, 
        color: '#333', 
        paddingTop: 9, 
        height: 44, 
        borderWidth: 1, 
        borderColor: '#e9e9e9', 
        backgroundColor: 'white',
        borderTopWidth: 0, 
        borderBottomWidth: 0
    },
    name1: {
        fontSize: 18,
        fontFamily: 'Montserrat-Bold',
        marginTop: 15,
        color: '#555'
      },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        marginRight: 10
    },
    circle: {
        height: 24,
        width: 24,
        borderRadius: 12,
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: '#ACACAC',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10
    },
    checkedCircle: {
        width: 18,
        height: 18,
        borderRadius: 9,
        backgroundColor: '#555',
    },
    radioLabel: {
        color: '#555',
    },
    contentTitle: {
        fontSize: 18,
        marginBottom: 12,
        width:'100%',
        textAlign:'center'
    },
    content: {
        backgroundColor: 'white',
        paddingTop: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    content2: {
        backgroundColor: 'white',
        paddingTop: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
        height: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        paddingTop: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
        width: '94%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentInput: {
        paddingTop: -10,
        paddingBottom: 0,
        marginBottom: 0
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    inputQtdAulas: {
        width: '34%', 
        textAlign: 'center', 
        fontWeight: '600', 
        fontSize: 18, 
        color: '#333', 
        paddingTop: 9, 
        height: 44, 
        borderWidth: 1, 
        borderColor: '#e9e9e9', 
        backgroundColor: 'white',
        borderTopWidth: 0, 
        borderBottomWidth: 0
    }
  
});