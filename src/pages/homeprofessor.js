import React,{ Component } from 'react';
import { 
    View, 
    Text,
    Image,
    Platform,
    PermissionsAndroid,
    StyleSheet,
    TouchableOpacity, 
    Dimensions,
    ImageBackground,
    AppState,
    BackHandler
} from 'react-native';

var {width, height} = Dimensions.get('window');

import firebase from 'react-native-firebase';

import Icon from 'react-native-vector-icons/Ionicons';
import IconAws from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import CreditoAtual from '../components/CreditoAtual';
import Mural from '../components/Mural';
import Meusagendamentos from '../components/Meusagendamentos';
import Incoming from '../components/Incoming';
import Avatar from '../components/avatar';
import Notificacao from '../components/Notificacao';
import getToken from '../components/TokenFcm';
import IncomingAgendamento from '../components/IncomingAgendamento';
import Modal from "react-native-modal";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as todoActions from '../actions/todos';
import AsyncStorage from '@react-native-community/async-storage';
import Fonts from '../styles/fonts';

import api from '../services/api';

async function requestCameraAndAudioPermission() {
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO]);
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the camera');
    } else {
      console.log('Camera permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
}  
    
class Homeprofessor extends Component  {

  constructor(props){
    super(props);

    this.state = {
      appState: AppState.currentState,
      visibleModal: false,
      tooltip: '',
      id: '',
      nome: '',
      email: '',
      cidade: '',
      uf: '',
      avatar:'',
      bgimage: '',
      agendamentos: [],
      receivedProps: false,
      index: 0,
      status: true,
      btnStatus: false,
      incoming: false,
      refreshNotifications: true,
      tabSelected: true,
    };

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    });
  }

  componentWillMount(){
    this.loadDataRedux();
  }

  componentDidMount() {
    
    getToken();
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const aula = JSON.parse(notification.data.dados);
      if(aula.origem === "findteacher")  
        this.IncomingAula(aula);
      else if(aula.origem === "agendamento")
        IncomingAgendamento(aula, this.props.navigation);
      
    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notification) => {
      const aula = JSON.parse(notification.notification.data.dados);
      if(aula.origem === "findteacher")  
        this.IncomingAula(aula);
      else if(aula.origem === "agendamento")
        IncomingAgendamento(aula, this.props.navigation);
    });

    this.entrarListaEspera();
    if (Platform.OS === 'android') {
      requestCameraAndAudioPermission().then(_ => {
        
      });
    }
  }

  componentWillUnmount() {
    // AppState.removeEventListener('change', this.handleAppStateChange);
    //this.willFocus.remove()
    this.notificationListener();
    this.notificationOpenedListener();
    this.backHandler.remove();
  }

  componentWillReceiveProps(){    
    this.loadDataRedux();
  }

  entrarListaEspera = async () => {
    const id = await AsyncStorage.getItem('@StudyLiveApp:id');
    const tokenFcm = await AsyncStorage.getItem('@StudyLiveApp:tokenFcm');
    try{
      const responseCategorias = await api.get('/SubcategoriaUsuario/'+id);
      const { todos  } = this.props;
      const response = await api.post('/listaEspera/'+id,{
        categorias: responseCategorias.data,
        nome: todos.nome,
        tokenFcm: tokenFcm,
        celular: "99999999",
      });
    }catch (response) {
    }

  }

  sairListaEspera = async () => {
    
    const id = await AsyncStorage.getItem('@StudyLiveApp:id');
    try{
      const response = await api.delete('/listaEspera/'+id);
    }catch (response) {
    }
    
  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip, visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  newAulaSuccessful = () => {
    this.setState({ visibleModal: 'newAulaSuccessful' });
  }
  
  cadastroSuccessful = () => {
    this.setState({ visibleModal: 'CadastroSuccessful' });
  }

  loadDataRedux = () => {
    var origem = this.props.navigation.getParam('origem','') ;

    if( origem == 'Configsprofessor'){
      this.props.navigation.setParams({'origem':''});
      this.cadastroSuccessful()
    }

    const { todos  } = this.props;
    
    var bgImage = "";
    
    if(todos.saldo == undefined ){
      var saldo = 0;
    } else {
      var saldo = todos.saldo;
    }
    
    this.setState({
      nome: todos.nome,
      email: todos.email,
      uf: todos.uf,
      cidade: todos.cidade,
      id: todos.id,
      bgimage: bgImage,
      avatar: todos.avatar,
      saldo: saldo,
      id_tipousuario: todos.id_tipousuario,
    },() => {
      console.log('state ',this.state)
    })
  }

  renderModalContent = () => (
    <View style={styles.content}></View>
  );

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'center'}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  renderModalNewAulaContent = () => (
    <View style={styles.newAulaSuccessful} onPress={ this.closeModal }>
      
      <View style={{ width: 100, height: 200, justifyContent: 'center', alignItems: 'center'  }}>
        <Image source={require('../images/ok.gif')} style={{width: '100%', height: '100%'}} />
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center'  }}>
        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 22, color: '#2FA29A' }}>Agendamento feito!</Text>
        <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 16, marginTop: 15, color: '#777' }}>
          Logo um professor será especialmente selecionado para você
        </Text>
      </View>

      <TouchableOpacity style={{ position: 'absolute', zIndex: 2, bottom: 50 }} onPress={() => this.closeModal()  } >
            <Text style={{ fontSize: 18, color: '#29A9AB' ,fontFamily: 'Montserrat-Regular' }}>FECHAR</Text>
        </TouchableOpacity>
      
    </View>
  );
  renderModalCadastroContent = () => (
    <View style={styles.newAulaSuccessful} onPress={ this.closeModal }>
      
      <View style={{ width: 100, height: 200, justifyContent: 'center', alignItems: 'center'  }}>
        <Image source={require('../images/ok.gif')} style={{width: '100%', height: '100%'}} />
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center'  }}>
        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 22, color: '#2FA29A' }}>Conta Configurada!</Text>
        <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 16, marginTop: 15, color: '#777' }}>
          Em até 24 hs alguém da nossa equipe entrará em contato com você para concluir seu cadastro. 
        </Text>
      </View>

      <TouchableOpacity style={{ position: 'absolute', zIndex: 2, bottom: 50 }} onPress={() => this.closeModal()  } >
            <Text style={{ fontSize: 18, color: '#29A9AB' ,fontFamily: 'Montserrat-Regular' }}>FECHAR</Text>
        </TouchableOpacity>
      
    </View>
  );
  
  closeModal = () => {
    this.setState({ visibleModal: '', incoming: false })
  }

  /* Ficar Disponível ou não */
  toggleBtnStatus = () => {
    this.setState({ btnStatus: !this.state.btnStatus, incoming: false });
  }
  
  toggleStatus = () => {
    this.setState({ status: !this.state.status, btnStatus: false },() => {
      /* CHAMAR FUNÇÃO FIREBASE AQUI */
      if(this.state.status)
        this.entrarListaEspera();
      else
        this.sairListaEspera();
      
    })
  }
  /* FIM - Ficar Disponível ou não */

  IncomingAula = (obj) => {
    /*var obj = {
      id: 169,
      id_usuario_aluno: 1,
      nomeAluno: 'Mauricio Ferg',
      displayCategoria: 'Portugues',
      avatar: '1571789106',
      channel: '11A',
      mensagem: 'Ashdliu ashlduh alisudhaliushdliuahsd aliiusdhilausdh aisd Ashdliu ashlduh alisudhaliushdliuahsd aliiusdhilausdh aisd  aliiusdhilausdh aisd Ashdliu ashlduh alisudhaliushdliuahsd aliiusdhilausdh aisd  aliiusdhilausdh aisd Ashdliu ashlduh alisudhaliushdliuahsd aliiusdhilausdh aisd ',
      assuntos: [
        { id: 1, nome: "AAAAAA" },
        { id: 2, nome: "BBBBB" },
        { id: 3, nome: "CCCCC" },
      ]
    }
    console.log("AQUI2: ",obj);*/
    this.setState({ 
      incomingAulaObj: obj
    },() => {
        /* Chama o Component depois de preencher as variaveis acima */
        this.setState({ incoming: true  })
      });
  }

  toggleRefreshNotifications = () => {
    this.setState({ refreshNotifications: !this.state.refreshNotifications })
  }

  render() {
    
    return (

      <View style={{ flex: 1, zIndex: 10}}>
        
          <Modal
            isVisible={this.state.visibleModal === 'tooltip'}
            
            useNativeDriver={true}
            animationIn="fadeIn"
            animationOut="fadeOut"
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalTooltipContent()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 'newAulaSuccessful'}
            deviceWidth={width}
            backdropColor={'#EFF1EF'}
            backdropOpacity={1}
            useNativeDriver={true}
            animationIn="slideInLeft"
            animationOut="slideOutRight"
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalNewAulaContent()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 'CadastroSuccessful'}
            deviceWidth={width}
            backdropColor={'#EFF1EF'}
            backdropOpacity={1}
            useNativeDriver={true}
            animationIn="slideInLeft"
            animationOut="slideOutRight"
          >
            {this.renderModalCadastroContent()}
          </Modal>
          
          <View style={styles.menu}>
            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()  }  style={{ width:'100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
              <Icon style={{  }}
                name="md-menu"
                size={26}
                color={'#104B59'}  
              />
            </TouchableOpacity>  
          </View>

          { this.state.incoming && 
            <Incoming dadosAula={this.state.incomingAulaObj} navigation={this.props.navigation} toggleStatus={this.state.incoming}  />
          }
          
          { this.state.status && 
            <TouchableOpacity  onPress={() => this.toggleBtnStatus()  } style={[styles.alert]}>
              <Icon style={{ marginTop: '3%',marginLeft: '2%' }}
                    name="ios-radio-button-on"
                    size={26}
                    color={'#6DD900'}  
                />
            </TouchableOpacity >  
          }
          { !this.state.status && 
            <TouchableOpacity  onPress={() => this.toggleBtnStatus()  } style={[styles.alert]}>
              <Icon style={{  }}
                    name="ios-radio-button-on"
                    size={26}
                    color={'red'}  
                />
            </TouchableOpacity >  
          }
          
          <TouchableOpacity style={styles.box1} onPress={ () => this.toggleBtnStatus() }>
            
            <Image source={require('../images/studentlearning_backdrop.jpg')} resizeMode={'stretch'} style={{width: '100%', height: '100%'}} />
          </TouchableOpacity>
          

          { this.state.btnStatus && this.state.status && 
            <TouchableOpacity style={styles.status}  onPress={ () => this.toggleStatus() }>
              <Text style={{ fontFamily: Fonts.Bold }}>Ficar Indisponível</Text>
            </TouchableOpacity>
          }
          
          { this.state.btnStatus && !this.state.status && 
            <TouchableOpacity style={styles.status}  onPress={ () => this.toggleStatus() }>
              <Text style={{ fontFamily: Fonts.Bold }}>Ficar Disponível</Text>
            </TouchableOpacity>
          }
          
          <View style={styles.box2}>
            <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', }}>

              <View style={{ position: 'absolute', zIndex: 20, top: '20%'}}>
                
              <Notificacao id={this.state.id} refreshNotifications={this.state.refreshNotifications} origem={'professor'}/>
                <View style={{ borderWidth: 21, borderColor: 'white', borderRadius: 50, }}>
                  <Avatar 
                    rounded
                    size="large"
                    source={{uri: this.state.avatar }}
                  />
                </View>   
                
              </View>

              <View style={{ position: 'absolute', zIndex: 20, top: '52%', width: '100%'}}>
                <View style={{ marginTop: 10, alignItems: 'center', justifyContent: 'center'  }}>
                  <Text style={styles.name1}>{this.state.nome}</Text> 
                </View>  
              </View>  
              <View style={{ position: 'absolute', zIndex: 20, bottom: 0, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                <CreditoAtual navigation={this.props.navigation} tipo={this.state.id_tipousuario}  creditoAtual={this.state.saldo} />     
              </View>
            </ImageBackground>
          </View>

          <View style={styles.box3}>
            { this.state.tabSelected && 
              <Mural navigation={this.props.navigation} id={this.props.todos.id} myFunc={this.toggleRefreshNotifications}  />
            }
            { !this.state.tabSelected && 
              <Meusagendamentos navigation={this.props.navigation} id={this.props.todos.id} myFunc={this.toggleRefreshNotifications}  />
            }
          </View>

          <View style={styles.customTab}>
            { this.state.tabSelected && 
              <TouchableOpacity onPress={() => this.setState({ tabSelected: true }) } style={{ width: '50%', marginTop: -1, justifyContent: 'center', alignItems: 'center', borderTopColor: '#9BD3CE', borderTopWidth: 2,}}>
                <IconAws style={{ marginTop: 3 }}
                    name="newspaper-o"
                    size={22}
                    color={'#9BD3CE'}  
                />
                <Text style={styles.tabTxt}>MURAL</Text>
              </TouchableOpacity>
            }     
            { !this.state.tabSelected && 
              <TouchableOpacity onPress={() => this.setState({ tabSelected: true }) }  style={{ width: '50%', marginTop: -1, justifyContent: 'center', alignItems: 'center',}}>
                <IconAws style={{ marginTop: 3 }}
                    name="newspaper-o"
                    size={22}
                    color={'#CCC'}  
                />
                <Text style={[styles.tabTxt,{ color: '#CCC' }]}>MURAL</Text>
              </TouchableOpacity>
            }     

            { this.state.tabSelected && 
              <TouchableOpacity onPress={() => this.setState({ tabSelected: false }) }  style={{ width: '50%', marginTop: -1, justifyContent: 'center', alignItems: 'center'}}>
                <IconAws style={{ marginTop: 3 }}
                    name="calendar"
                    size={22}
                    color={'#CCC'}  
                />
                <Text style={[styles.tabTxt,{ color: '#CCC' }]}>AGENDAMENTOS</Text>
              </TouchableOpacity>
            }
            { !this.state.tabSelected && 
              <TouchableOpacity onPress={() => this.setState({ tabSelected: false }) }  style={{ width: '50%', marginTop: -1, justifyContent: 'center', alignItems: 'center', borderTopColor: '#9BD3CE', borderTopWidth: 2,}}>
                <IconAws style={{ marginTop: 3 }}
                    name="calendar"
                    size={22}
                    color={'#9BD3CE'}  
                />
                <Text style={styles.tabTxt}>AGENDAMENTOS</Text>
              </TouchableOpacity>
            }
          </View>
        
      </View>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Homeprofessor)


const styles = StyleSheet.create({
  status: {
    position: 'absolute', 
    zIndex: 13, 
    width: 140, 
    height: 40, 
    backgroundColor: 'white', 
    right: 65, 
    ...Platform.select({
      ios: {
        top: '3.7%', 
      },
      android: {
        top: 13, 
      },
    }),
    justifyContent: 'center', 
    alignItems: 'center', 
    elevation: 5
  },
  customTab: {
    position: 'absolute', 
    zIndex: 20, 
    bottom: 0, 
    width: '100%', 
    height: 55, 
    backgroundColor: '#FFF', 
    borderTopWidth: 1, 
    borderTopColor: '#f0f0f0',
    flexDirection: 'row',
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 1,
    shadowOffset: {
        height: 2,
        width: 1
    } 
  },
  
  tabTxt: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: '#9BD3CE',
    marginTop: 3
  },
  marcar: {
    fontWeight: 'bold'
  },
  txt1:{
    fontSize: 13,
    color: '#999',
    textAlign: 'left',
    fontWeight: '600'
  },
  txt2:{
    fontSize: 13,
    color: '#76C3D7',
    textAlign: 'left',
    fontWeight: '600'
  },
  menu: {
    position: 'absolute', 
    zIndex: 10, 
    width: 40, 
    height: 40,
    borderRadius: 40,
    backgroundColor: '#e9e9e9',
    ...Platform.select({
      ios: {
        marginTop: 25, 
      },
      android: {
        marginTop: 10, 
      },
    }),
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOpacity: 0.8,
    shadowRadius: 20,
    borderColor: '#FFF',
    borderWidth: 0.5,
    elevation: 10,
    shadowOffset: {
        height: 20,
        width: 20
    } 
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 5,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  newAulaSuccessful: {
    backgroundColor: '#EFF1EF',
    marginLeft: -5,
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    
  },
  alert: {
    position: 'absolute', 
    zIndex: 10, 
    width: 40, 
    height: 40,
    borderRadius: 40,
    right: 15,
    backgroundColor: '#e9e9e9',
    ...Platform.select({
      ios: {
        marginTop: 25, 
      },
      android: {
        marginTop: 10, 
      },
    }),
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 5,
    shadowOffset: {
        height: 20,
        width: 20
    } 
  },
  box0: {
    position: 'absolute', 
    zIndex: 2,  
    width: '100%', 
    backgroundColor: 'white',
    
    height: 100, 
    marginTop: 0,
  },
  box1: {
    position: 'absolute', 
    zIndex: 2, 
    width: '100%', 
    height: (height*0.5), 
  },
  textBox1: {
    position: 'absolute', 
    zIndex: 10, 
    ...Platform.select({
      ios: {
        top: '11%', 
      },
      android: {
        top: 22, 
      },
    }),
    right: 70
  },
  box2: {
    position: 'absolute', 
    zIndex: 3,  
    width: '100%', 
    height: (height*0.32), 
    opacity: 0.92
  },
  box3: {
    position: 'absolute', 
    zIndex: 4, 
    backgroundColor: '#f5f5f5', 
    width: '100%', 
    height: (height*0.68)+50, 
    marginTop: (height*0.32), 
    borderTopWidth: 2,
    borderTopColor: '#fff'
  },
  name1: {
    fontSize: 22,
    fontFamily: 'Montserrat-Bold',
    marginLeft: 5,
    color: 'white'
  },
  endereco: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: 'white',
    marginTop: 5,
    marginLeft: 5,
  },
});