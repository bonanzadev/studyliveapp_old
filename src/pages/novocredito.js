import React,{ Component } from 'react';
import { View, Text,Image,TextInput,Platform,Dimensions,StyleSheet,TouchableOpacity,TouchableWithoutFeedback   } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';

import CreditoAtual from '../components/CreditoAtual';
import Categorias from '../components/Categorias';
import Assuntos from '../components/Assuntos';
import Divisao from '../components/Divisao';
import Avatar from '../components/avatar';
import Modal from "react-native-modal";
import { TextField } from 'react-native-material-textfield';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actions/todos'
import { Tooltip } from 'react-native-elements';

var {height, width} = Dimensions.get('window');
    
class Novaaula extends Component {

  constructor(props) {
    super(props);
    this.tooltip1 = React.createRef();
  }

  state = {
    visibleModal: false,
    totalMinutos: '60',
    totalValor: '120,00',
    saldoatual: '',
    saldoFinal: '',
    qtdAulas: "1",
    cupom: 'Procurar...',
    tipo: '',
    numcartao: '',
    validademes: '',
    validadeano: '',
    nome: '',
    ccv: '',
  };

  componentDidMount(){
    const didBlurSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {

            var saldoatual = this.props.todos.userDataConfig.creditoAtual;

            this.setState({ saldoatual });
                    
        }
    );
  }

  editNovaaula = () => {
    this.props.editNovaaula(this.state);
  }

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'center'}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  communicate = (tooltip,time) => {
    this.setState({ tooltip,visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  
  closeModal = () => {
    this.setState({ visibleModal: null, toggleModal: false })
  }

  handleQtde = (qtdAulas) => { this.setState({ qtdAulas })}

  plusQtdAula = () => {
    
    var atual = Number(this.state.qtdAulas);
    var totalMinutos = this.state.totalMinutos;
    var saldoatual = this.props.todos.userDataConfig.creditoAtual

    atual = atual + 1;
    totalMinutos = atual * 30;
    saldoFinal = saldoatual - atual;
    this.setState({ qtdAulas: atual,totalMinutos,saldoFinal });
    
  }

  minusQtdAula = () => {
    
    var atual = Number(this.state.qtdAulas);
    var totalMinutos = this.state.totalMinutos;
    var saldoatual = this.props.todos.userDataConfig.creditoAtual;

    if(atual>1){
      atual = atual - 1;
      totalMinutos = atual * 30;
      saldoFinal = saldoatual - atual;
      this.setState({ qtdAulas: atual,totalMinutos,saldoFinal });
    } else {
      this.communicate("O mínimo é 1 aula",1000);
    }
  }

  render() {
    const {navigate} =this.props.navigation;

    return (
      <View style={{ flex: 1, backgroundColor: '#FFF'}}>

        
        <Modal
          isVisible={this.state.visibleModal === 'tooltip'}
          hasBackdrop={false}
          useNativeDriver={true}
          animationIn="fadeIn"
          animationOut="fadeOut"
          onBackdropPress={() => this.closeModal() }
        >
          {this.renderModalTooltipContent()}
        </Modal>
        
        <ScrollView>
            <View style={{ width:"100%", paddingLeft: 15, paddingRight: 15, marginTop: 30  }}>

                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    
                    <View style={{ width: '50%' }}>
                        <Text style={General.inputLabel}>Quantas Aulas?</Text>
                        <View style={General.formInputNoPadding}>
                            <View style={{ flexDirection: 'row',   }}>
                            <TouchableOpacity style={{ width: '34%',  }} onPress={() => this.minusQtdAula() }>
                                <Icon style={{ alignSelf: 'center', marginTop: 10}} 
                                name='minus'
                                size={24}
                                color='black'
                                />
                            </TouchableOpacity>  
                            <Text style={styles.inputQtdAulas}>{this.state.qtdAulas}</Text>
                            <TouchableOpacity style={{ width: '30%' }} onPress={() => this.plusQtdAula() }>
                                <Icon style={{ alignSelf: 'center', marginTop: 10 }} 
                                name='plus'
                                size={24}
                                color='black'
                                />
                            </TouchableOpacity>  
                            </View>
                        </View> 
                    </View>
                    <View style={{ width: '50%' }}>
                        <View style={{  }}>
                            <Text style={General.inputLabel}>Tempo Total</Text>
                            <Text style={{ textAlign: 'center', fontSize: 16, color: '#5BCCC2', fontWeight: 'bold', marginTop: 12 }}>
                            {this.state.totalMinutos} Minutos
                            </Text>
                        </View>  
                    </View>
                </View> 

                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    
                    <View style={{ width: '50%' }}>
                        <Text style={General.inputLabel}>Cupom ?</Text>
                        <View >
                            <TouchableOpacity  style={General.formInput} onPress={() => this.openModalCategoria() }>
                                <Text style={{ textAlign: 'left', fontSize: 14, fontWeight: '600', color: '#777', marginTop: 12 }}>{this.state.cupom}</Text>
                            </TouchableOpacity >
                        </View> 
                    </View>
                    <View style={{ width: '50%' }}>
                        <View style={{  }}>
                            <Text style={General.inputLabel}>Valor Total</Text>
                            <Text style={{ textAlign: 'center', fontSize: 16, color: '#5BCCC2', fontWeight: 'bold', marginTop: 12 }}>
                            R$ {this.state.totalValor} 
                            </Text>
                        </View>  
                    </View>
                </View> 

                

                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 30, marginBottom: 0, marginLeft: 15 }}>
                    
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Realizar Pagamento</Text>
                    
                </View>
                
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10, marginBottom: 20, marginLeft: 15 }}>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={styles.circle}
                            onPress={() => this.setState({ tipo: 'credito' })} 
                        >
                            { this.state.tipo === 'credito' && (<View style={styles.checkedCircle} />) } 
                        </TouchableOpacity>  
                        <Text style={styles.radioLabel}>Cartão de Crédito</Text>
                    </View>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={styles.circle}
                            onPress={() => this.setState({ tipo: 'boleto' })} 
                        >
                            { this.state.tipo === 'boleto' && (<View style={styles.checkedCircle} />) } 
                        </TouchableOpacity>  
                        <Text style={styles.radioLabel}>Boleto</Text>
                    </View>
                </View>
                
                <View style={{  }}>
                    <Text style={General.inputLabel}>Número do Cartão</Text>
                    <TextInput
                    ref = {(input) => this.input1 = input}
                    onSubmitEditing={() => this.input2.focus()}
                    onChangeText={this.handleNumero}
                    value={this.state.numcartao}                  
                    returnKeyType='next'
                    placeholder='Número do Cartão'
                    style={General.formInput}
                    />
                </View> 
                
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '60%' }}>
                        <View style={{  }}>
                            <Text style={General.inputLabel}>Validade (mm/AA)</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '50%' }}>
                                    <TextInput
                                        ref = {(input) => this.input2 = input}
                                        onSubmitEditing={() => this.input3.focus()}
                                        onChangeText={this.handleValidademes}
                                        value={this.state.validademes}                  
                                        returnKeyType='next'
                                        placeholder='Mês'
                                        style={General.formInput}
                                    />
                                </View>
                                <View style={{ width: '50%' }}>
                                    <TextInput
                                        ref = {(input) => this.input3 = input}
                                        onSubmitEditing={() => this.input4.focus()}
                                        onChangeText={this.handleValidadeano}
                                        value={this.state.validadeano}                  
                                        returnKeyType='next'
                                        placeholder='Ano'
                                        style={General.formInput}
                                    />
                                </View>
                            </View>    
                        </View> 
                    </View>
                    <View style={{ width: '40%' }}>
                    <View style={{ width: '90%'}}>
                        <Text style={General.inputLabel}>CCV</Text>
                        <TextInput
                        ref = {(input) => this.input4 = input}
                        onSubmitEditing={() => this.input5.focus()}
                        onChangeText={this.handleCcv}
                        value={this.state.ccv}                           
                        returnKeyType='next'
                        placeholder='CCV'
                        style={General.formInput}
                        />
                    </View>  
                    </View>
                </View> 

                <View style={{  }}>
                    <Text style={General.inputLabel}>Nome escrito</Text>
                    <TextInput
                    ref = {(input) => this.input5 = input}
                    onSubmitEditing={() => this.input6.focus()}
                    onChangeText={this.handleNome}
                    value={this.state.nome}                  
                    returnKeyType='next'
                    placeholder='Nome escrito no Cartão'
                    style={General.formInput}
                    />
                </View> 

            </View>
            
        </ScrollView>   
        
        
      </View>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Novaaula)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        marginRight: 10
    },
    circle: {
        height: 24,
        width: 24,
        borderRadius: 12,
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: '#ACACAC',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10
    },
    checkedCircle: {
        width: 18,
        height: 18,
        borderRadius: 9,
        backgroundColor: '#555',
    },
    radioLabel: {
        color: '#555',
    },
    contentTitle: {
        fontSize: 18,
        marginBottom: 12,
        width:'100%',
        textAlign:'center'
    },
    content: {
        backgroundColor: 'white',
        paddingTop: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    content2: {
        backgroundColor: 'white',
        paddingTop: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
        height: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        paddingTop: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
        width: '94%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentInput: {
        paddingTop: -10,
        paddingBottom: 0,
        marginBottom: 0
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    inputQtdAulas: {
        width: '34%', 
        textAlign: 'center', 
        fontWeight: '600', 
        fontSize: 18, 
        color: '#333', 
        paddingTop: 9, 
        height: 44, 
        borderWidth: 1, 
        borderColor: '#e9e9e9', 
        backgroundColor: 'white',
        borderTopWidth: 0, 
        borderBottomWidth: 0
    }
  
});