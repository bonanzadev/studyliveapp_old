/*This is an Example of Animated Splash Screen*/
import React, { Component } from 'react';
import { 
  View, 
  Text, 
  Image, 
  StyleSheet,
  ScrollView, 
  ActivityIndicator,
  FlatList,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  AsyncStorage
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import api from '../services/api';
import url from '../services/url';
import domain from '../services/domain';
import FotoAvatar from '../components/FotoAvatar';
import CheckBoxItem from '../components/CheckBoxItem';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import Swiper from 'react-native-swiper';
import { TextInputMask } from 'react-native-masked-text';
import General from '../styles/general';
import Fonts from '../styles/fonts';
import ImagePicker from 'react-native-image-crop-picker';
import Avatar from '../components/avatar';
import Bancos from '../components/Bancos';
import {StackActions, NavigationActions } from 'react-navigation';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as todoActions from '../actions/todos';

class Configsprofessor extends Component {
  state = {
      data: [],
      datasubcategoria: [],
      selectedBoxes: [],
      selected: [],
      refreshing: false,
      isLoading: false,
      isLoading2: false,
      isLoadingAvatar: false,
      showPaginate: true,
      visibleModal: '',
      categoriaClicada: '',
      idcategoriaClicada: '',
      togglePhoto: '',
      id: '',
      nome: '',
      apelido: '',
      login: '',
      cpf: '',
      rg: '',
      email: '',
      celular: '',
      dt_nascimento: '',
      uf: '',
      cep: '',
      logradouro: '',
      numero: '',
      complemento: '',
      bairro: '',
      cidade: '',
      bgimage: '',
      avatar: '',
      id_banco: '',
      displayBanco: '',
      agencia: '',
      tipoconta: 1,
      contacorrente: '',
      poupanca: '',
      variacaopoupanca: '',
      configurouconta: '',
      saldo: '',
  };

  componentDidMount(){
    this.verificaStorage();
    this.loadData()
  }

  verificaStorage = async () => {
    try{
      let id = await AsyncStorage.getItem('@StudyLiveApp:id') || null;
      
      console.log('CONFIGPROF:', id);
      this.setState({ 
        id,
        isLoading: true, 
      }, async () => {

        const response = await api.get(`/usuario/${id}`);
        const {usuario,subcategorias} = response.data.data;
        
        if(usuario){
          this.setState({ 
            id: usuario.id,
            nome: usuario.nome|| "",
            apelido: usuario.apelido|| "",
            login: usuario.login|| "",
            id_tipousuario: usuario.id_tipousuario|| "",
            bgimage: usuario.bgimage || "",
            cpf: usuario.cpf|| "",
            rg: usuario.rg|| "",
            email: usuario.email|| "",
            dt_nascimento: usuario.dt_nasc|| "",
            celular: usuario.celular|| "",
            uf: usuario.uf|| "",
            cep: usuario.cep|| "",
            logradouro: usuario.logradouro|| "",
            numero: usuario.numero|| "",
            complemento: usuario.complemento|| "",
            cidade: usuario.cidade|| "",
            bairro: usuario.bairro|| "",
            id_banco: usuario.id_banco|| "",
            displayBanco: usuario.displayBanco|| "",
            agencia: usuario.agencia|| "",
            tipoconta: usuario.tipoconta || 1|| "",
            contacorrente: usuario.contacorrente|| "",
            poupanca: usuario.poupanca|| "",
            saldo: usuario.saldo || "",
            variacaopoupanca: usuario.variacaopoupanca|| "",
            configurouconta: usuario.configurouconta|| "",
            avatar: FotoAvatar(id,usuario.avatar),
            isLoading: false,
            selectedBoxes: subcategorias|| "",
          });
        }
      });
    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, tooltip2: "Houve um erro"});
    }
  }

  loadData = async () => {
    try{
      this.setState({isLoading: true});
    
      const response = await api.get(`/categoria`);
      console.log(response)
      
      this.setState({
          data: response.data,
          refreshing: false,
          isLoading: false,
      })
    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, tooltip2: "Houve um erro"});
    }
  }

  loadDataSubcategoria = async (id,nome) => {
    try{
      this.setState({isLoading2: true});
    
      const response = await api.get(`/subcategoria/${id}`);
      console.log(response)
      
      this.setState({
          datasubcategoria: response.data,
          refreshing: false,
          isLoading2: false,
          visibleModal: 'subcategorias',
          categoriaClicada: nome,
          idcategoriaClicada: id
      })
    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, tooltip2: "Houve um erro"});
    }
  }

  selectGaleriaImage = () => {
    ImagePicker.openPicker({
      width: 200,
      height: 200,
      cropping: true,
      compressImageMaxWidth: 200,
      compressImageMaxHeight: 200,
      includeBase64: true
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleAvatarimage(image.data)
    });
  }

  selectCameraImage = async () => {
    ImagePicker.openCamera({
      width: 200,
      height: 200,
      cropping: true,
      compressImageMaxWidth: 200,
      compressImageMaxHeight: 200,
      includeBase64: true
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleAvatarimage(image.data)
    });
  }

  handleAvatarimage = async (avatar) => { 
    try {
      console.log('antes: ', this.state);
      this.setState({ isLoadingAvatar: true });
      const response = await api.post(`/avatar/${this.state.id}`,{data: avatar});
      console.log('depois: ', response);
      if(response.data.data){
        let newavatar = domain+"/photos/"+response.data.data
        this.setState({ avatar: newavatar, });
      }
      this.setState({ isLoadingAvatar: false });
    } catch (error) {
      console.log(response)
      this.setState({ isLoading: false, tooltip2: "Houve um erro"});
    }
    
  }

  renderModalContent = () => (
    <View style={[styles.content,{height: 100}]}>
      
      <View style={{ flexDirection:'row', paddingBottom: 15 }}>
        <TouchableOpacity style={{ width: '50%', alignItems:'center'   }} onPress={this.selectGaleriaImage}>
          <Icon style={General.searchIcon} 
            name='picture-o'
            size={24}
            color='black'
          />
          <Text style={{ fontSize: 16,  }}>Galeria</Text>
        </TouchableOpacity>
        
        <TouchableOpacity style={{ width: '50%',alignItems:'center'   }}  onPress={this.selectCameraImage}>
          <Icon style={General.searchIcon} 
            name='camera'
            size={24}
            color='black'
          />
          <Text style={{ fontSize: 16,  }}>Camera</Text>
        </TouchableOpacity>
      </View>
      
    </View>
  );

  _renderItem = ({item}) => {
    let index = this.state.selectedBoxes.findIndex(obj => obj.idcat === item.id)
    return index !== -1 ?
    (
      <TouchableOpacity activeOpacity={0.9} key={item.id} style={styles.box} onPress={() => {this.loadDataSubcategoria(item.id,item.nome)}}>
        <View style={[styles.backdrop,{backgroundColor: 'orange'}]}></View>
        <Text style={styles.categoria}>{item.nome}</Text>
        <Image source={{uri: url+item.nomefoto+".png" }} resizeMode="cover"  style={styles.img} />
      </TouchableOpacity>
    ) : (
      <TouchableOpacity activeOpacity={0.9} key={item.id} style={styles.box} onPress={() => {this.loadDataSubcategoria(item.id,item.nome)}}>
        <View style={styles.backdrop}></View>
        <Text style={styles.categoria}>{item.nome}</Text>
        <Image source={{uri: url+item.nomefoto+".png" }} resizeMode="cover"  style={styles.img} />
      </TouchableOpacity>
    )
  }

  renderMoralSubcategorias = () => (
    <View style={styles.content}>
      
      <View style={{ flexDirection: 'row', justifyContent: "space-between", width: '100%', marginBottom: 10  }}>
        <Text style={[styles.title,{marginTop: 5, color: '#555' }]}>{this.state.categoriaClicada}</Text>
        <TouchableOpacity onPress={this.closeModal}>
          <Icon style={{ margin: 5 }}
            name="close"
            size={22}
            color={'#29A9AB'}  
          />
        </TouchableOpacity>
      </View>
      <ScrollView style={{ height: '80%', width: '100%' }}>
                    
          <FlatList
              style={{ marginTop: 5}}
              data={this.state.datasubcategoria}
              renderItem={
                  this._renderItemSubcategoria
              }
              keyExtractor={item => item.id_assunto}
          />
          
      </ScrollView>
      <TouchableOpacity style={styles.btn} onPress={() => {this.closeModal()}}>
        <Icon 
            style={{ marginTop: 3}} 
            name='check'
            size={28}
            color='#FFF'
        />
      </TouchableOpacity>
    </View>
  );

  closeModal = () => {
    this.setState({ visibleModal: ''})
  }

  _renderItemSubcategoria = ({item}) => {

        
    let index = this.state.selectedBoxes.findIndex(obj => obj.id === item.id)

    if (index != -1) {
        var check = true;
    } else {
        var check = false;
    }
    
    return (
        <View key={item.id} style={styles.subcategoria_box}>
            
            <CheckBoxItem label={item.nome} checked={check} onUpdate={this.onUpdate.bind(this,item.id,item.nome)}/>
            
        </View>
    )
  }

  setBanco = (id,nome) => {
    this.setState({
        id_banco: id,
        displayBanco: nome
    })

    this.closeModal()
  }

  renderModalBancoContent = () => (
    <View style={styles.content} onPress={ this.closeModal }>
      
      <Text style={styles.contentTitle}>Selecione seu Banco</Text>
      
      <Bancos myFunc={this.setBanco} />
      
    </View>
  );

  onUpdate = (id,nome) => {
        
    this.setState(previous => {
      
        let selectedBoxes = previous.selectedBoxes;
        //let index = selectedBoxes[id].indexOf(id) 
        let index = selectedBoxes.findIndex(obj => obj.id === id)
        if (index === -1) {
            selectedBoxes.push({
                id: id, nome: nome, idcat: this.state.idcategoriaClicada
            }) 
        } else {
            //selectedBoxes[id].splice(index, 1)
            selectedBoxes.splice(index, 1)
        }
        return { selectedBoxes }; 

    }); 
  }

  handleApelido = (apelido) => { this.setState({ apelido }) }
  handleCelular = (celular) => { this.setState({ celular })}
  handleCPF = (cpf) => { this.setState({ cpf })}
  handleCep = (cep) => { this.setState({ cep })}
  handleUF = (uf) => { this.setState({ uf })}
  handleLogradouro = (logradouro) => { this.setState({ logradouro })}
  handleNumero = (numero) => { this.setState({ numero })}
  handleComplemento = (complemento) => { this.setState({ complemento })}
  handleBairro = (bairro) => { this.setState({ bairro })}
  handleCidade = (cidade) => { this.setState({ cidade })}
  handleAgencia = (agencia) => { this.setState({ agencia })}
  handleCC = (contacorrente) => { this.setState({ contacorrente })}
  handlePoupanca = (poupanca) => { this.setState({ poupanca })}
  handleVariacao = (variacao) => { this.setState({ variacao })}

  handleSaveFormUser = async () => {
    var resp = "";

    if(this.state.apelido==""){ resp += "\n- Como você quer ser chamado?"; }
    if(this.state.celular==""){ resp += "\n- Número de Celular"; }
    if(this.state.cpf==""){ resp += "\n- CPF"; }
    if(this.state.cep==""){ resp += "\n- CEP"; }
    if(this.state.uf==""){ resp += "\n- Estado"; }
    if(this.state.logradouro==""){ resp += "\n- Logradouro"; }
    if(this.state.uf==""){ resp += "\n- Estado"; }
    if(this.state.numero==""){ resp += "\n- Número"; }
    
    if(this.state.bairro==""){ resp += "\n- Bairro"; }
    if(this.state.cidade==""){ resp += "\n- Cidade"; }
    

    
    if(resp != ""){
      this.communicate("Preencha os campos: \n"+resp,5000);
    } else {

      try{ 
        console.log('s', this.state);
        const response = await api.put(`/usuario/${this.state.id}`,this.state);
        console.log('r', response);
        if( response.data.affectedRows > 0){

          this.editUserdatageral()

          const resetAction = StackActions.reset({
            index: 0,
            
            actions: [NavigationActions.navigate({ routeName: "Homeprofessor",params: {
              origem:"Configsprofessor"
            }, })],
          });
        
          this.props.navigation.dispatch(resetAction)
        }
      } catch (response) {
        console.log(response)
        this.setState({ isLoading: false, tooltip2: "Houve um erro"});
      }
      
    }
  }

  editUserdatageral = () => {

    this.props.editUserdatageral(this.state);
  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip, visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  openModalBancos = () => {
    this.setState({ visibleModal: 'bancos'})
  }

  consultaCep = async () => {
        
    var cep = this.state.cep.replace("-","");

    if( cep == "" ){
        return false;
    }

    try{
        
        const response = await api.get(`https://viacep.com.br/ws/${cep}/json/`)
        console.log(response)
        if( response ){
            this.setState({
                logradouro: response.data.logradouro, 
                bairro: response.data.bairro,
                cidade: response.data.localidade,
                uf: response.data.uf
            });
        }
    } catch (response) {
        
        console.log(response)
        this.setState({error: response, buttonDisabled: false});
    }

  }

  apresentacao = () => {
    return(
      <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
        <Image source={require("../images/studylive_colorful.png")} style={{ width: 100, height: 80 }} />
        <Text style={[styles.descricao,{fontSize: 22, marginTop: 15, color: '#555'}]}>
          Olá 
          <Text style={{ color: '#29A9AB' }}>&nbsp;{this.state.nome}</Text>
        </Text>
        <Text style={[styles.descricao,{fontSize: 16, marginTop: 5, color: '#555'}]}>Bem vindo ao Studylive</Text>
        <Text style={[styles.descricao,{fontSize: 16, marginTop: 5, color: '#555'}]}>Vamos configurar sua conta</Text>
        <Text style={[styles.descricao,{fontSize: 22, marginTop: 5, color: '#555'}]}>Bora lá?</Text>
      </View>
    )
  }

  slide0 = () => {

    const avatar1 = this.state.avatar;

    return(
      <KeyboardAvoidingView
        style={{ flex:1}}
        behavior={Platform.select({
            ios: 'padding',
            android: null,
        })} 
      >
        <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <Text style={styles.descricao}>Dados Pessoais</Text>
          
          
          {this.state.isLoading && <ActivityIndicator size="large" color="green"/>}    
          <ScrollView style={{width: '95%', height: '100%' }}>

            <View style={{ flex: 1, paddingRight: 10, justifyContent: 'center', alignItems: 'center' }}>
              
              <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                { this.state.avatar != ""  && !this.state.isLoadingAvatar &&
                  <Avatar 
                    rounded
                    size="xlarge"
                    onPress={() => this.setState({ visibleModal: 'photos',togglePhoto: 'avatar' })}
                    source={{uri: avatar1, }}
                    activeOpacity={0.7}
                  />
                }
                { this.state.avatar === "" && !this.state.isLoadingAvatar &&
                  <Avatar 
                    rounded
                    size="xlarge"
                    onPress={() => this.setState({ visibleModal: 'photos',togglePhoto: 'avatar' })}
                    source={require('../images/user-icon.jpg')}
                    activeOpacity={0.7}
                  />
                }
                { this.state.isLoadingAvatar && 
                  <ActivityIndicator size="large" color="green"/>
                }
                  
              </View> 
              
              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Apelido</Text>
                <TextInput
                  ref = {(input) => this.input1 = input}
                  onSubmitEditing={() => this.input2.focus()}
                  onChangeText={this.handleApelido}
                  value={this.state.apelido}                  
                  returnKeyType='next'
                  placeholder='Como quer ser chamado(a) ?'
                  style={General.formInput}
                />
              </View>  
              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Celular</Text>
                <TextInputMask
                  refInput={(ref) => this.input2 = ref}
                  onSubmitEditing={() => this.input3.focus()}
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(99) '
                  }}
                  onChangeText={this.handleCelular}
                  value={this.state.celular}                  
                  returnKeyType='next'
                  placeholder="(DDD) Numero"
                  style={General.formInput}
                />
              </View>  
              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>CPF</Text>
                <TextInputMask
                  refInput={(ref) => this.input3 = ref}
                  onChangeText={this.handleCPF} 
                  type={'cpf'}
                  value={this.state.cpf}           
                  returnKeyType='next'
                  placeholder='CPF'
                  style={General.formInput}
                />
              </View>  
              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Data de Nascimento</Text>
                <TextInputMask
                  refInput={(ref) => this.input4 = ref}
                  type={'datetime'}
                  options={{
                    format: 'DD/MM/YYYY'
                  }}
                  value={this.state.dt_nascimento}
                  returnKeyType='next'
                  placeholder='Data de Nascimento'
                  style={General.formInput}
                  onChangeText={text => {
                    this.setState({
                      dt_nascimento: text
                    })
                  }}
                />
              </View>  
              
              
            </View> 
            <View style={{ width: '100%', height: 50 }}></View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>  
    )
  }

  slide1 = () => {
    return(
      <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
        
        <Text style={styles.descricao}>Selecione o que você deseja lecionar?</Text>
        
        {this.state.isLoading && <ActivityIndicator size="large" color="green"/>}    
        <ScrollView style={{width: '90%', height: '100%' }}>
            <FlatList
                data={this.state.data}
                renderItem={
                    this._renderItem
                }
                keyExtractor={item => item.id}
            />
            
            <View style={{ width: '100%', height: 50 }}></View>
        </ScrollView>
      </View>
    )
  }

  slide2 = () => {
    return(
      <KeyboardAvoidingView
        style={{ flex:1}}
        behavior={Platform.select({
            ios: 'padding',
            android: null,
        })} 
      >
        <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
          
          <Text style={styles.descricao}>Endereço Atual</Text>
          
          {this.state.isLoading && <ActivityIndicator size="large" color="green"/>}    
          <ScrollView style={{width: '95%', height: '100%' }}>

            <View style={{ flex: 1, paddingRight: 10, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>CEP</Text>
                    <TextInputMask
                      refInput={(ref) => this.input5 = ref}
                      onSubmitEditing={() => this.input6.focus()}
                      onChangeText={this.handleCep}
                      onBlur={this.consultaCep}
                      value={this.state.cep}
                      type={'zip-code'}
                      style={General.formInput}
                      returnKeyType = "next"
                      placeholder="CEP"
                      underlineColorAndroid="transparent"
                      blurOnSubmit={false}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>UF</Text>
                    <TextInput
                      ref = {(input) => this.input6 = input}
                      onSubmitEditing={() => this.input7.focus()}
                      onChangeText={this.handleUF}
                      value={this.state.uf}
                      autoCorrect={false}
                      returnKeyType='next'
                      maxLength={2}
                      placeholder='UF'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View>  

              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Logradouro</Text>
                <TextInput
                  ref = {(input) => this.input7 = input}
                  onSubmitEditing={() => this.input8.focus()}
                  onChangeText={this.handleLogradouro}
                  value={this.state.logradouro}                  
                  returnKeyType='next'
                  placeholder='Logradouro'
                  style={General.formInput}
                />
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Numero</Text>
                    <TextInput
                      ref = {(input) => this.input8 = input}
                      onSubmitEditing={() => this.input9.focus()}
                      onChangeText={this.handleNumero}
                      value={this.state.numero}                  
                      returnKeyType='next'
                      placeholder='Número'
                      style={General.formInput}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Complemento</Text>
                    <TextInput
                      ref = {(input) => this.input9 = input}
                      onSubmitEditing={() => this.input10.focus()}
                      onChangeText={this.handleComplemento}
                      value={this.state.complemento}                           
                      returnKeyType='next'
                      placeholder='Complemento'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View> 

              <View style={{ width: '100%' }}>
                
                <Text style={General.inputLabel}>Bairro</Text>
                <TextInput
                  ref = {(input) => this.input10 = input}
                  onSubmitEditing={() => this.input11.focus()}
                  onChangeText={this.handleBairro}
                  value={this.state.bairro}                           
                  returnKeyType='next'
                  placeholder='Bairro'
                  style={General.formInput}
                />
              
              </View>
                
              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Cidade</Text>
                <TextInput
                  ref = {(input) => this.input11 = input}
                  
                  onChangeText={this.handleCidade}
                  value={this.state.cidade}                           
                  returnKeyType='next'
                  placeholder='Cidade'
                  style={General.formInput}
                />
              </View>  
                
              
            </View> 
            <View style={{ marginTop: 10, width:'96%', marginLeft: 10}}>
              <TouchableOpacity style={General.submit} onPress={() => this.handleSaveFormUser()   } >
                <Text style={General.submitText}>SALVAR</Text>
              </TouchableOpacity>
            </View>

            <View style={{ width: '100%', height: 50 }}></View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    )
  }

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'left', fontFamily: Fonts.Light}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  render() {
    return (
        <View
          style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
          }}>

          <Modal
            isVisible={this.state.visibleModal === 'subcategorias'}
            
            useNativeDriver={true}
            animationInTiming={300}
            animationOutTiming={300}
            backdropTransitionInTiming={500}
            backdropTransitionOutTiming={500}
          >
            {this.renderMoralSubcategorias()}
          </Modal>   
          <Modal
            isVisible={this.state.visibleModal === 'photos'}
            useNativeDriver={true}
            onSwipeComplete={() => this.setState({ visibleModal: null })}
            swipeDirection={['down','up','left']}
            onBackdropPress={() => this.setState({ visibleModal: null })}
          >
            {this.renderModalContent()}
          </Modal>      
          <Modal
            isVisible={this.state.visibleModal === 'bancos'}
            useNativeDriver={true}
            animationIn="zoomInDown"
            animationOut="fadeOut"
            animationInTiming={600}
            animationOutTiming={600}
            backdropTransitionInTiming={300}
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalBancoContent()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 'tooltip'}
            hasBackdrop={false}
            useNativeDriver={true}
            animationIn="fadeIn"
            animationOut="fadeOut"
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalTooltipContent()}
          </Modal>
          <Swiper 
            bounces={true}
            loop={false}
            scrollsToTop={true}
            showsButtons={true}
            showsPagination={false}
            nextButton={<Text style={styles.buttonText}>PRÓXIMO</Text>}
            prevButton={<Text style={styles.buttonText}>ANTERIOR</Text>}
            buttonWrapperStyle={styles.buttons}
            
            //dot={<View style={{backgroundColor: '#CCC', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
            //activeDot={<View style={{backgroundColor: '#76C3D7', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
            paginationStyle={{
              bottom: 30,
              position: 'relative'
            }}
          >
            {this.apresentacao()} 
            {this.slide0()} 
            {this.slide1()} 
            {this.slide2()} 
            
          </Swiper>      
        </View>
    );
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Configsprofessor)


const styles = StyleSheet.create({
  box: {
    width: '100%', 
    justifyContent: 'center',
    alignItems: 'center',
    height: 83, 
    marginTop: 10
  },
  buttons: {
    backgroundColor: 'transparent', 
    flexDirection: 'row', 
    position: 'absolute', 
    bottom: 0, 
    left: 0, 
    flex: 1, 
    paddingHorizontal: 10, 
    paddingVertical: 10, 
    justifyContent: 'space-between', 
    alignItems: 'flex-end',
  },
  buttonText: {
    fontFamily: Fonts.Light,
    fontSize: 14,
    margin: 10
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  content: {
    backgroundColor: '#FFF',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: '80%',
    borderRadius: 10
  },
  backdrop: {
    width: '100%', 
    height: '100%', 
    backgroundColor: 'black', 
    opacity: 0.5, 
    position: 'absolute', 
    zIndex: 2, 
    borderRadius: 5, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  categoria: {
    fontFamily: Fonts.Bold,
    fontSize: 22,
    color: 'white',
    position: 'absolute', 
    zIndex: 3, 
  },
  title: {
    fontFamily: Fonts.Bold,
    fontSize: 16,
    marginTop: 30
  },
  descricao: {
    fontFamily: Fonts.Bold,
    fontSize: 16,
    marginTop: 20,
    marginBottom: 20,
    color: '#555'
  },
  img: {
    width:'100%', height: '100%', borderRadius: 5
  },
  btn: {
    width: 50, 
    height: 50, 
    position: 'absolute', 
    backgroundColor: '#29A9AB', 
    borderRadius: 50, 
    bottom: 10, 
    right: 10, 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
    marginRight: 10
  },
  circle: {
      height: 24,
      width: 24,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#ACACAC',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 10
  },
  checkedCircle: {
      width: 18,
      height: 18,
      borderRadius: 9,
      backgroundColor: '#999',
  },
  radioLabel: {
    color: '#555',
    fontFamily: Fonts.Light,
  }
})