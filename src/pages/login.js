import React, { Component,useState, useEffect  } from 'react';

import { 
  View,  
  TextInput, 
  Text,
  StyleSheet,
  ImageBackground,
  Keyboard, 
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  SafeAreaView,
  Platform,
  BackHandler
} from 'react-native';

import {StackActions, NavigationActions } from 'react-navigation';
import Svg,{ SvgUri, Image, Circle, ClipPath, Path } from 'react-native-svg';
import Icon from 'react-native-vector-icons/FontAwesome';
import api from '../services/api';
import url from '../services/url';

var {width, height} = Dimensions.get('window');
import AsyncStorage from '@react-native-community/async-storage';
import Animated, { Easing } from 'react-native-reanimated';
import {TapGestureHandler,State} from 'react-native-gesture-handler';
import Modal from "react-native-modal";
import FotoAvatar from '../components/FotoAvatar';
import FotoBgimage from '../components/FotoBgimage';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actions/todos';

import {
  
  MaterialIndicator,
} from 'react-native-indicators';


const {
  Value,
  event,
  block,
  cond,
  eq,
  set,
  Clock,
  startClock,
  stopClock,
  debug,
  timing,
  clockRunning,
  interpolate,
  Extrapolate,
  concat,
} = Animated;

function runTiming(clock, value, dest) {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0),
    visibleModal: false,
  };

  const config = {
    duration: 1000,
    toValue: new Value(0),
    easing: Easing.inOut(Easing.ease)
  };

  return block([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, value),
      set(state.frameTime, 0),
      set(config.toValue, dest),
      startClock(clock)
    ]),
    timing(clock, state, config),
    cond(state.finished, debug('stop clock', stopClock(clock))),
    state.position
  ]);
}

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = { 
      id: '', 
      apelido: '', 
      nome: '', 
      email: '', 
      login: '', 
      senha: '', 
      repitasenha: '', 
      id_tipousuario: '', 
      configurouconta: '', 
      tokenFcm: '', 
      tooltip: 'CONCLUIR CADASTRO' ,
      tooltip2: 'ENTRAR' ,
      tooltipSenha: 'RESETAR SENHA',
      cadastroConcluido: false,
      reenvioConcluido: false,
      digitando: false,
      isLoading: false,
      logged: false,
      username: '', 
      password: '', 
      error: false ,
      mgTop: 0,
      assets: url
    };

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    });

    this.buttonOpacity = new Value(1);

    this.onStateChange = event([
      {
        nativeEvent: ({ state }) =>
          block([
            cond(
              eq(state, State.END),
              set(this.buttonOpacity, runTiming(new Clock(), 1, 0))
            )
          ])
      }
    ]);

    this.onCloseState = event([
      {
        nativeEvent: ({ state }) =>
          block([
            cond(
              eq(state, State.END),
              set(this.buttonOpacity, runTiming(new Clock(), 0, 1))
            )
          ])
      }
    ]);

    this.textInputZindex = new Value(3);
    this.textInputZindex2 = new Value(1);

    this.buttonY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [100, 0],
      extrapolate: Extrapolate.CLAMP
    });

    this.bgY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [-height / 3-130, 0],
      extrapolate: Extrapolate.CLAMP
    });

    this.textInputZindex = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [1,3],
      extrapolate: Extrapolate.CLAMP
    });
    this.textInputZindex2 = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [3,1],
      extrapolate: Extrapolate.CLAMP
    });

    this.textInputY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [0,100],
      extrapolate: Extrapolate.CLAMP
    });
    
    this.textInputOpacity = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [1,0],
      extrapolate: Extrapolate.CLAMP
    });
  }

  componentWillUnmount(){
    this.backHandler.remove();
  }

  handleNome = (nome) => { this.setState({ nome }) };
  handleEmail = (email) => { this.setState({ email }) };
  handleLogin = (login) => { this.setState({ login }) };
  handleSenha = (senha) => { this.setState({ senha }) };
  handleRepitasenha = (repitasenha) => { this.setState({ repitasenha }) };

  verificaSenhas = () => { 
    if( this.state.repitasenha != this.state.senha && this.state.repitasenha != ""){
      this.communicate("Senhas diferentes");
    } else {
      return true;
    }
  };
  verificaVazio = () => { 
    
    if( this.state.nome=="" || this.state.email=="" || this.state.login=="" || this.state.repitasenha=="" || this.state.senha=="" ){
      this.communicate("Preencha todos os campos");
    } else {
      this.communicate("CONCLUIR CADASTRO");
      return true;
    }
  };
  
  communicate = (tooltip) => {
    this.setState({ tooltip,error: true });
  }

  validacao = () =>{
    if( this.verificaVazio() && this.verificaSenhas() ){
      this.setState({ error: false });
      Keyboard.dismiss();
      return true;
    } else {
      this.setState({ error: true });  
      return false;
    }
  }

  voltar = () => {
    setTimeout(() => {
        this.props.navigation.navigate('Produto',{
            reload: true
        });
    }, 500);
  }

  handleSignUp = async () =>{
    if( this.validacao() ){

      this.setState({isLoading: true});
      
      try{
        
        const response = await api.post('/cadastro',{
          id: this.state.id,
          nome: this.state.nome,
          email: this.state.email,
          login: this.state.login,
          senha: this.state.senha,
          id_tipousuario: this.state.id_tipousuario,
        });

        console.log(response)

        if( response.data.errors == "" ){

          this.setState({
            error: false, 
            cadastroConcluido: true,
            tooltip: "CONCLUÍDO",
            nome: "",
            email: "",
            senha: "",
            repitasenha: ""
          },() => {
            setTimeout(() => {
              this.closeModal()
            },5000)
          });

          
        } else {
          this.setState({error: true, tooltip: response.data.errors});
        }
        
      } catch (response) {
        
        this.setState({error: true, tooltip: "Houve um erro"});
      }

      this.setState({isLoading: false});
    }
  }

  handleEsqueciSenha = async () =>{
    if( this.state.email != "" ){

      this.setState({isLoading: true});
      
      try{
        
        const response = await api.get(`/esquecisenha/${this.state.email}`);

        console.log(response)

        if( response.data.errors == "" ){

          this.setState({
            
            reenvioConcluido: true,
            tooltipSenha: "CONCLUÍDO",
          },() => {
            setTimeout(() => {
              this.closeModal()
            },5000)
          });

          
        } else {
          this.setState({error: true, tooltipSenha: response.data.errors});
        }
        
      } catch (response) {
        
        this.setState({error: true, tooltipSenha: "Houve um erro"});
      }

      this.setState({isLoading: false});
    }
  }
  
  handleSignIn = async () => {

    this.setState({isLoading: true});
      
    try{
      //console.log('antes: ',this.state);
      const response = await api.post('/login',{
        login: this.state.login,
        senha: this.state.senha,
      });
      //console.log('depois: ',response);

      const{ data } = response.data;

      if( response.data.errors == "" ){
        
        if(data.bgimage==null){
          var bg = "default";
        } else {
          var bg = FotoBgimage(data.id,data.bgimage);
        }

        if(data.avatar==null){
          var av = "default";
        } else {
          var av = FotoAvatar(data.id,data.avatar);
        }

        if(data.saldo==null){
          var saldo = "0";
        } else {
          var saldo = data.saldo;
        }
        
        this.setState({ 
          id: data.id, 
          id_tipousuario: data.id_tipousuario, 
          apelido: data.apelido, 
          nome: data.nome, 
          login: data.login, 
          email: data.email, 
          celular: data.celular,
          configurouconta: data.configurouconta, 
          tokenFcm: data.token_fcm || '', 
          saldo: saldo,
          bgimage: bg,
          avatar: av,
          logged: true,
        });

          /* Salva dados no Redux  */
        this.editUserdatageral();
        
        await AsyncStorage.multiSet([
          ['@StudyLiveApp:id',String(this.state.id)], 
          ['@StudyLiveApp:login',this.state.login],
          ['@StudyLiveApp:senha',this.state.senha],
          ['@StudyLiveApp:tipousuario',String(this.state.id_tipousuario)],
          ['@StudyLiveApp:tokenFcm',this.state.tokenFcm],
        ]);
        
        this.setState({ visibleModal: '' })

        if( this.state.id_tipousuario == 1 ){
          var rt = "Home";
        } else {
          
          if(this.state.configurouconta==null){
            var rt = "Configsprofessor";
          } else {
            var rt = "Homeprofessor";
          }
        }
        
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: rt, origem:"login" })],
        });
      
        this.props.navigation.dispatch(resetAction)

      } else {
        this.setState({error: true, tooltip2: response.data.errors});
      }
      
    } catch (response) {
      console.log(response)
      this.setState({error: true, tooltip2: "Houve um erro"});
    }

    this.setState({isLoading: false});
  }

  randomBgimage = () => {
    const min = 1;
    const max = 7;
    const rand = Math.floor(Math.random() * max) + min ;
    return rand
  }

  editUserdatageral = () => {
    this.props.editUserdatageral(this.state);
  }

  handleUserNameChange = (username) => {
    this.setState({ username });
  };
    
  handlePasswordChange = (password) => {
    this.setState({ password });
  };

  _keyboardDidShow = () => {
    this.setState({ digitando: true })
  }

  _keyboardDidHide = () => {
    this.setState({ digitando: false })
  }

  closeModal = () => {
    this.setState({ visibleModal: null })
  }

  callTestegratuito = () => {
    if( this.state.cadastroConcluido ){
      
      this.setState({ visibleModal: "cadastro" },() => {
        setTimeout(() => {
          this.setState({ visibleModal: "" })
        }, 2000);
      })

    } else {
      this.setState({ visibleModal: "cadastro" })
    }
  }

  renderModalAsyncStorage = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <MaterialIndicator size={50} color='#FFF' />
      </View>
    )
  }

  renderModal = () => {
    return !this.state.cadastroConcluido ? 
    (
      <KeyboardAvoidingView style={{ width: '100%', height:'98%', justifyContent: 'center', alignItems: 'center', paddingTop: 30 }} behavior="height" enabled>

        
          <View style={{ position: 'absolute', top: 20,width: '90%', flexDirection: 'row' }} >
            <View style={{ position: 'absolute', left: 0, marginLeft: 15 }} >
              <Text style={styles.contentTitle}>Meu Cadastro</Text>
            </View>
            <View style={{ position: 'absolute', right: 0, }} >
              <Icon 
                onPress={() => this.closeModal() }
                name='times'
                size={44}
                color='#FFF'
              />
            </View>
          </View>
        
      
        <ScrollView style={{ width: '100%', marginTop: 30,}}>
          
          <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'  }}>

          
          <TextInput 
            style={styles.inputCadastro} 
            placeholderTextColor="#AAA" 
            keyboardType="email-address"
            ref = {(input) => this.input1 = input}
            onSubmitEditing={() => this.input2.focus()}
            value={this.state.nome}
            onChangeText={this.handleNome}
            blurOnSubmit={true} 
            autoCapitalize="none"
            returnKeyType="next"
            autoCorrect={false}
            onBlur={this.validacao}
            placeholder="SEU NOME" />
          <TextInput 
            style={styles.inputCadastro} 
            placeholderTextColor="#AAA" 
            keyboardType="email-address"
            ref = {(input) => this.input2 = input}
            blurOnSubmit={false} 
            onSubmitEditing={() => this.input3.focus()}
            value={this.state.email}
            onChangeText={this.handleEmail}
            autoCapitalize="none"
            returnKeyType="next"
            autoCorrect={false}
            onBlur={this.validacao}
            placeholder="E-MAIL" />
          <TextInput 
            style={styles.inputCadastro} 
            placeholderTextColor="#AAA" 
            keyboardType="email-address"
            ref = {(input) => this.input3 = input}
            blurOnSubmit={false} 
            onSubmitEditing={() => this.input4.focus()}
            onChangeText={this.handleLogin}
            value={this.state.login}
            autoCapitalize="none"
            returnKeyType="next"
            autoCorrect={false}
            onBlur={this.validacao}
            placeholder="LOGIN" />
          <TextInput 
            style={styles.inputCadastro} 
            placeholderTextColor="#AAA" 
            ref = {(input) => this.input4 = input}
            blurOnSubmit={false} 
            onSubmitEditing={() => this.input5.focus()}
            value={this.state.senha}
            onChangeText={this.handleSenha}
            secureTextEntry={true}
            returnKeyType="next"
            autoCorrect={false}
            onBlur={this.validacao}
            placeholder="SENHA" />
          <TextInput 
            style={styles.inputCadastro} 
            placeholderTextColor="#AAA" 
            ref = {(input) => this.input5 = input}
            blurOnSubmit={false} 
            onSubmitEditing={() => this.validacao()}
            value={this.state.repitasenha}
            onChangeText={this.handleRepitasenha}
            onKeyPress={this.validacao}
            secureTextEntry={true}
            returnKeyType="go"
            autoCorrect={false}
            placeholder="REPITA A SENHA" />

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginBottom: 10 }}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.circle}
                onPress={() => this.setState({ id_tipousuario: 1 })} 
              >
                { this.state.id_tipousuario === 1 && (<View style={styles.checkedCircle} />) } 
              </TouchableOpacity>  
              <Text style={styles.radioLabel}>Sou Aluno</Text>
            </View>

            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.circle}
                onPress={() => this.setState({ id_tipousuario: 2 })} 
              >
                { this.state.id_tipousuario === 2 && (<View style={styles.checkedCircle} />) } 
              </TouchableOpacity>  
              <Text style={styles.radioLabel}>Sou Professor</Text>
            </View>
          </View>
          

          
          <TouchableOpacity style={[styles.submit,{width: '90%',flexDirection: 'row', top: Platform.OS === 'ios' ? this.state.mgTop : null}]} onPress={() => this.handleSignUp()   } >  
            {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
            {!this.state.isLoading && <Text style={styles.submitText}>{this.state.tooltip}</Text>}
          </TouchableOpacity>
          
          </View>
          <View style={{width:'100%',height: 70}}></View>
        </ScrollView>  
      </KeyboardAvoidingView>
    ) :  (
      <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} >
        <View style={{ maringTop: 0 }} >
          <Icon 
            
            name='check-square-o'
            size={44}
            color='#FFF'
          />
        </View>
        <View style={{ marginTop: 15 }} >
          <Text style={styles.contentTitle}>Cadastro Concluído</Text>
        </View>
        <View style={{ marginTop: 15 }} >
          <Text style={[styles.contentTitle,{ fontSize: 14 }]}>Um e-mail de ativação foi enviado</Text>
        </View>
        <View style={{ marginTop: 5 }} >
          <Text style={[styles.contentTitle,{ fontSize: 14 }]}>Verifique sua caixa de entrada</Text>
        </View>
        
      </View>
    )
  }
  renderModalEsqueci = () => {
    return !this.state.reenvioConcluido ? 
      (
      <KeyboardAvoidingView style={{ width: '100%', height:250, justifyContent: 'center', alignItems: 'center', paddingTop: 30 }} behavior="height" enabled>

        <ScrollView style={{ width: '100%', }}>
          
          <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'  }}>

          <View style={{ width: '90%', flexDirection: 'row', justifyContent: 'space-between' }} >
            <View style={{ marginLeft: 15 }} >
              <Text style={styles.contentTitle}>Recuperar Senha</Text>
            </View>
            <View style={{  }} >
              <Icon 
                onPress={() => this.closeModal() }
                name='times'
                size={44}
                color='#FFF'
              />
            </View>
          </View>
          
          <TextInput 
            style={styles.inputCadastro} 
            placeholderTextColor="#AAA" 
            keyboardType="email-address"
            value={this.state.email}
            onChangeText={this.handleEmail}
            blurOnSubmit={false} 
            autoCapitalize="none"
            returnKeyType="next"
            autoCorrect={false}
            placeholder="E-MAIL DE CADASTRO" />
          
          <TouchableOpacity style={[styles.submit,{width: '90%',flexDirection: 'row'}]} onPress={() => this.handleEsqueciSenha()   } >  
            {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
            {!this.state.isLoading && <Text style={styles.submitText}>{this.state.tooltipSenha}</Text>}
          </TouchableOpacity>
          
          </View>
        </ScrollView>  
      </KeyboardAvoidingView>

  ) : (
      <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} >
        <View style={{ maringTop: 0 }} >
          <Icon 
            
            name='check-square-o'
            size={44}
            color='#FFF'
          />
        </View>
        <View style={{ marginTop: 15 }} >
          <Text style={styles.contentTitle}>Senha enviada</Text>
        </View>
        <View style={{ marginTop: 15 }} >
          <Text style={[styles.contentTitle,{ fontSize: 14 }]}>Um e-mail com seu novo acesso foi enviado</Text>
        </View>
        <View style={{ marginTop: 5 }} >
          <Text style={[styles.contentTitle,{ fontSize: 14 }]}>Verifique sua caixa de entrada</Text>
        </View>
        
      </View>
  )
}

  render() {
    const {navigate} =this.props.navigation;

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'flex-end'
        }}
      >
        <Modal
          isVisible={this.state.visibleModal === 'cadastro'}
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          backdropColor="#222"
          backdropOpacity={0.9}
          animationInTiming={800}
        animationOutTiming={800}
        backdropTransitionInTiming={800}
        backdropTransitionOutTiming={800}
          style={{justifyContent: 'flex-start', margin: 0,}}
        >
          {this.renderModal()}
        </Modal>
        <Modal
          isVisible={this.state.visibleModal === 'esqueciminhasenha'}
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          backdropColor="#000"
          backdropOpacity={0.8}
          onBackdropPress={() => this.closeModal()}
          style={{justifyContent: 'center', margin: 0,}}
        >
          {this.renderModalEsqueci()}
        </Modal>
        <Modal
          isVisible={this.state.visibleModal === 'asyncStorage'}
          useNativeDriver={true}
          backdropColor="#000"
          backdropOpacity={0.8}
        >
          {this.renderModalAsyncStorage()}
        </Modal>
        <View style={{ position: 'absolute', zIndex: 4, top:0, height: 60, width: '100%', backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'  }}>
          <ImageBackground
              source={require('../images/logo1.png')}
              resizeMode="cover"
              style={{ width: 160, height: 30 }}
              
            />
        </View>
        <Animated.View
          style={{
            ...StyleSheet.absoluteFill,
            position: 'absolute',
            zIndex: 3,
            transform: [{ translateY: this.bgY,}]
          }}
        >
          <TouchableOpacity onPress={() => Keyboard.dismiss()} activeOpacity={1}>
            <Svg height={height+50} width={width}>
              <ClipPath id="clip">
                <Circle r={height+50} cx={width/2}  />
              </ClipPath>
              <Image
                href={require('../images/studentlearning.jpg')}
                preserveAspectRatio='xMidYMid slice'
                resizeMode="cover"
                width={width}
                height={height+50}
                clipPath="url(#clip)"
              />
            </Svg>
          </TouchableOpacity>
        </Animated.View>
        
        

          <Animated.View style={{ 
              position: 'absolute',
              zIndex: this.textInputZindex, 
              
              ...StyleSheet.absoluteFill,
              top: null,
              height: height / 3, 
              width: '100%',
              justifyContent: 'center', alignItems: 'center' }}>

              <TapGestureHandler onHandlerStateChange={this.onStateChange}>
                <Animated.View
                  style={{
                    ...styles.button,
                    opacity: this.buttonOpacity,
                    width: '90%',
                    transform: [{ translateY: this.buttonY }]
                  }}
                >
                  <Text style={{ fontSize: 20, fontWeight: 'bold' }}>IR PARA O LOGIN</Text>
                </Animated.View>
              </TapGestureHandler>
              
            
              
              <TouchableOpacity style={{ width: '100%', }} onPress={() => this.callTestegratuito() }>
                <Animated.View
                  style={{
                    ...styles.button,
                    backgroundColor: '#2E71DC',
                    opacity: this.buttonOpacity,
                    width: '90%',
                    transform: [{ translateY: this.buttonY }]
                  }}
                >
                  <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                    QUERO ME CADASTRAR
                  </Text>
                </Animated.View>
                </TouchableOpacity>
          </Animated.View>

          <Animated.View style={{ 
            zIndex: this.textInputZindex2, 
            transform: [{ translateY: this.textInputY }],
            ...StyleSheet.absoluteFill,
            top: null,
            height: height / 3, 
              width: '100%',
            justifyContent: 'center', alignItems: 'center' }}>
            
            
              <TapGestureHandler onHandlerStateChange={this.onCloseState}>
                <Animated.View style={styles.titleLogin}>
                  <Animated.Text style={{ fontSize: 15}}>VOLTAR</Animated.Text>
                </Animated.View>
              </TapGestureHandler>

              <TextInput 
                style={[styles.inputLogin, {top: Platform.OS === 'ios' ? this.state.mgTop : null}]} 
                placeholderTextColor="#AAA" 
                keyboardType="email-address"
                ref = {(input) => this.input1A = input}
                onSubmitEditing={() => this.input2A.focus()}
                onChangeText={this.handleLogin}
                value={this.state.login}
                autoCapitalize="none"
                returnKeyType="next"
                blurOnSubmit={false} 
                autoCorrect={false}
                onBlur={ () => this.setState({mgTop: 0}) }
                onFocus={ () => this.setState({mgTop: '-130%'}) }
                placeholder="LOGIN" />
              <TextInput 
                style={[styles.inputLogin, {top: Platform.OS === 'ios' ? this.state.mgTop : null}]} 
                placeholderTextColor="#AAA" 
                ref = {(input) => this.input2A = input}
                onSubmitEditing={() => this.handleSignIn()}
                value={this.state.senha}
                onChangeText={this.handleSenha}
                secureTextEntry={true}
                blurOnSubmit={false} 
                returnKeyType="go"
                autoCorrect={false}
                onBlur={ () => this.setState({mgTop: 0}) }
                onFocus={ () => this.setState({mgTop: '-130%'}) }
                placeholder="SENHA" />

              <TouchableOpacity style={[styles.submit, {top: Platform.OS === 'ios' ? this.state.mgTop : null}]} onPress={() => this.handleSignIn()   } >  
              <Animated.View >
                {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
                {!this.state.isLoading && <Text style={styles.submitText}>{this.state.tooltip2}</Text>}
                
              </Animated.View>
              </TouchableOpacity>

              <TouchableOpacity style={{ width: '100%', height: 90, justifyContent: 'center', alignItems: 'center'  }} onPress={() => this.setState({ visibleModal: 'esqueciminhasenha' })}>
                <Animated.Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 20, height: 70, color: '#777' }}>ESQUECI MINHA SENHA</Animated.Text>
              </TouchableOpacity>
          </Animated.View>

          
          
        
      </SafeAreaView>
    );
  }

}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Login)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentTitle: {
    fontSize: 26,
    marginBottom: 12,
    color: 'white',
    marginTop: 7,
    marginRight: 15,
    fontWeight: '600'
  },
  content2: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingBottom: 25,
    width: '100%',
    height: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  button: {
    backgroundColor: '#e9e9e9',
    height: 70,
    marginHorizontal: 20,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  },
  titleLogin: {
    width: 100,
    height: 30,
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 4,
    top: '-37%',
    left: width/2-50
  },
  input: {
    borderRadius: 56,
    backgroundColor: '#F0F0F0',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#f5f5f5',
    width: '80%',
    marginTop: 10,
    paddingLeft: 20,
    height: 56, 
    fontSize: 12,
    fontWeight: 'bold'
  },
  inputLogin: {
    borderRadius: 56,
    backgroundColor: '#F0F0F0',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#f5f5f5',
    width: '80%',
    marginTop: 10,
    paddingLeft: 20,
    height: 56, 
    fontSize: 12,
    fontWeight: 'bold'
  },
  inputCadastro: {
    borderRadius: 56,
    backgroundColor: '#F0F0F0',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#f5f5f5',
    width: '90%',
    marginTop: 10,
    paddingLeft: 20,
    height: 56, 
    fontSize: 12,
    fontWeight: 'bold'
  },
  submit:{
    marginTop:10,
    backgroundColor:'#34A59C',
    borderRadius:56,
    height: 56, 
    borderWidth: 1,
    borderColor: '#00B2B2',
    shadowColor: "#999",
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 1,
    shadowOffset: {
        height: 2,
        width: 1
    } 
  },
  submitCadastro:{
    marginTop:10,
    backgroundColor:'#34A59C',
    borderRadius:56,
    height: 56, 
    borderWidth: 1,
    borderColor: '#00B2B2',
    shadowColor: "#000",
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 5,
    shadowOffset: {
        height: 20,
        width: 20
    } 
  },
  submitText:{
      color:'#fff',
      fontSize: 12,
      fontWeight: 'bold',
  },
  submitError:{
    marginTop:10,
    backgroundColor: '#f5f5f5',
    borderRadius:56,
    height: 56, 
    borderWidth: 1,
    borderColor: '#FFF',
    shadowColor: "#000",
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  submitTextError:{
      color:'#AAA',
      fontSize: 12,
      fontWeight: 'bold',
  },
  buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      height: 40,
      marginRight: 10
  },
  circle: {
      height: 24,
      width: 24,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#ACACAC',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 10
  },
  checkedCircle: {
      width: 18,
      height: 18,
      borderRadius: 9,
      backgroundColor: '#FFF',
  },
  radioLabel: {
    color: 'white',
  }
});


