import React,{ Component } from 'react';
import { 
  View, 
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  StyleSheet,
  Platform,
  StatusBar,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';

import api from '../services/api';
import Fonts from '../styles/fonts';

import Avatar from '../components/avatar';
import Modal from "react-native-modal";
import { TextInputMask } from 'react-native-masked-text';
import FotoAvatar from '../components/FotoAvatar';
import FotoBgimage from '../components/FotoBgimage';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actions/todos'
import ImagePicker from 'react-native-image-crop-picker';

    
class Meusdados extends Component {
  state = {
    visibleModal: '',
    nome: '',
    apelido: '',
    login: '',
    id_tipousuario: 1,
    email: '',
    celular: '',
    dt_nascimento: '',
    uf: '',
    cep: '',
    logradouro: '',
    numero: '',
    complemento: '',
    bairro: '',
    cidade: '',
    bgimage: '',
    avatar: '',
    saldo: '',
    togglePhoto: '',
    isLoading: false,
    isLoadingAvatar: false,
    isLoadingBg: false,
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  editUserdatageral = () => {
    this.props.editUserdatageral(this.state);
    //this.voltar();
  }

  voltar = () => {
    setTimeout(() => {
        this.props.navigation.navigate('Home',{origem: 'Meusdados',msg:""});
    }, 300);
  }

  verificaStorage = async () => {
    let id = await AsyncStorage.getItem('@StudyLiveApp:id') || null;
    
    this.setState({ id, isLoading: true,isLoadingAvatar: true, isLoadingBg: true, });

    const response = await api.get(`/usuario/${id}`);
    const {usuario} = response.data.data;
    
    if(usuario){
      
      let avatar = FotoAvatar(id,usuario.avatar);
      let bgimage = FotoBgimage(id,usuario.bgimage);
      
      this.setState({ 
        nome: usuario.nome|| "",
        apelido: usuario.apelido|| "",
        login: usuario.login|| "",
        id_tipousuario: 1,
        email: usuario.email|| "",
        dt_nascimento: usuario.dt_nasc|| "",
        celular: usuario.celular|| "",
        uf: usuario.uf|| "",
        cep: usuario.cep|| "",
        logradouro: usuario.logradouro|| "",
        numero: usuario.numero|| "",
        complemento: usuario.complemento|| "",
        cidade: usuario.cidade|| "",
        bairro: usuario.bairro|| "",
        saldo: usuario.saldo || "",
        avatar: avatar,
        bgimage: bgimage || "",
        isLoading: false,
        isLoadingAvatar: false,
        isLoadingBg: false,
      });
    }
  }

  componentDidMount() {

    this.verificaStorage();

    this.scrollY = new Animated.Value(0)

    this.startHeaderHeight = 230
    this.endHeaderHeight = 50
    if (Platform.OS == 'android') {
        this.startHeaderHeight = 200 + StatusBar.currentHeight
        this.endHeaderHeight = 70 + StatusBar.currentHeight
    }

    this.animatedHeaderHeight = this.scrollY.interpolate({
        inputRange: [0, 230],
        outputRange: [this.startHeaderHeight, this.endHeaderHeight],
        extrapolate: 'clamp'
    })

    this.animatedOpacity = this.animatedHeaderHeight.interpolate({
        inputRange: [this.endHeaderHeight, this.startHeaderHeight],
        outputRange: [0.3, 1],
        extrapolate: 'clamp'
    })
    this.animatedOpacity2 = this.animatedHeaderHeight.interpolate({
        inputRange: [this.endHeaderHeight, this.startHeaderHeight],
        outputRange: [0.5, 1],
        extrapolate: 'extend'
    })
    this.animatedTagTop = this.animatedHeaderHeight.interpolate({
        inputRange: [this.endHeaderHeight, this.startHeaderHeight],
        outputRange: [-10, 10],
        extrapolate: 'clamp'
    })
    this.animatedMarginTop = this.animatedHeaderHeight.interpolate({
        inputRange: [this.endHeaderHeight, this.startHeaderHeight],
        outputRange: [50, 30],
        extrapolate: 'clamp'
    })
  }

  handleApelido = (apelido) => { this.setState({ apelido })}
  handleNome = (nome) => { this.setState({ nome })}
  handleEmail = (email) => { this.setState({ email: email.trim() })}
  handleCelular = (celular) => { this.setState({ celular })}
  handleNascimento = (dt_nascimento) => { this.setState({ dt_nascimento })}
  handleUF = (uf) => { this.setState({ uf })}
  handleCep = (cep) => { this.setState({ cep })}
  handleLogradouro = (logradouro) => { this.setState({ logradouro })}
  handleNumero = (numero) => { this.setState({ numero })}
  handleComplemento = (complemento) => { this.setState({ complemento })}
  handleBairro = (bairro) => { this.setState({ bairro })}
  handleCidade = (cidade) => { this.setState({ cidade })}

  handleSaveFormUser = async () => {
    var resp = "";

    if(this.state.apelido==""){ resp += "\n- Como você quer ser chamado?"; }
    if(this.state.nome==""){ resp += "\n- Nome Completo"; }
    if(this.state.email==""){ resp += "\n- E-mail"; }
    if(this.state.dt_nascimento==""){ resp += "\n- Data de Nascimento"; }
    if(this.state.celular==""){ resp += "\n- Número de Celular"; }
  
    if(resp != ""){
      this.communicate("Preencha os campos: \n"+resp,5000);
    } else {
      const response = await api.put(`/usuario/${this.state.id}`,this.state);
      console.log(response)
      if( response.data.affectedRows > 0){
        /* SAVE DATA REDUX */
        this.editUserdatageral();
        this.voltar();
      }
      
    }
  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip, visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'left', fontFamily: Fonts.Light}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  selectGaleriaAvatar = () => {
    this.setState({ isLoadingAvatar: true, })
    
    ImagePicker.openPicker({
      width: 200,
      height: 200,
      cropping: true,
      compressImageMaxWidth: 200,
      compressImageMaxHeight: 200,
      includeBase64: true,
      isLoadingAvatar: true,
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleAvatarimage(image.data)
    });
  }

  selectCameraAvatar = async () => {
    this.setState({ isLoadingAvatar: true, })

    ImagePicker.openCamera({
      width: 200,
      height: 200,
      cropping: true,
      compressImageMaxWidth: 200,
      compressImageMaxHeight: 200,
      includeBase64: true,
      
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleAvatarimage(image.data)
    });
  }

  handleAvatarimage = async (avatar) => { 
    
    try{

      const response = await api.post(`/avatar/${this.state.id}`,{data: avatar});
    
      if(response.data.data){
        
        let newavatar = FotoAvatar(this.state.id,response.data.data);
        
        this.setState({ avatar: newavatar, isLoadingAvatar: false  });
        this.editUserdatageral();
      } else {
        this.setState({ isLoadingAvatar: false });
      }
    } catch (response) {
      
      this.setState({ isLoadingAvatar: false });
    }
  }
  handleBgimage = async (bgimage) => { 
    
    try{
      const response = await api.post(`/bgimage/${this.state.id}`,{data: bgimage});
    
      if(response.data.data){
        
        let newbgimage = FotoBgimage(this.state.id,response.data.data);
        
        this.setState({ bgimage: newbgimage, isLoadingBg: false });
        this.editUserdatageral()

      } else {
        this.setState({ isLoadingBg: false });
      }
    } catch (response) {
      this.setState({ isLoadingBg: false });
    }
  }

  selectGaleriaBgimage = () => {
    this.setState({ isLoadingBg: true, })

    ImagePicker.openPicker({
      width: 500,
      height: 300,
      cropping: true,
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 300,
      compressImageQuality: 1,
      includeBase64: true,
      
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleBgimage(image.data)
    });
  }

  selectCameraBgimage = async () => {
    this.setState({ isLoadingBg: true, })

    ImagePicker.openCamera({
      width: 500,
      height: 300,
      cropping: true,
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 300,
      compressImageQuality: 1,
      includeBase64: true,
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleBgimage(image.data)
    });
  }

  renderModalContent = () => {
    return this.state.togglePhoto == 'avatar' ? (
      <View style={styles.content}>

        <Text style={styles.contentTitle}>Escolher foto de perfil</Text>

        <View style={{ flexDirection:'row', paddingBottom: 15 }}>
          <TouchableOpacity style={{ width: '50%', alignItems:'center'   }} onPress={this.selectGaleriaAvatar}>
            <Icon style={General.searchIcon} 
              name='picture-o'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,  }}>Galeria</Text>
          </TouchableOpacity>
          
          <TouchableOpacity style={{ width: '50%',alignItems:'center'   }}  onPress={this.selectCameraAvatar}>
            <Icon style={General.searchIcon} 
              name='camera'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,  }}>Camera</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    ) : (
      <View style={styles.content}>

        <Text style={styles.contentTitle}>Escolher foto de capa</Text>

        <View style={{ flexDirection:'row', paddingBottom: 15 }}>
          <TouchableOpacity style={{ width: '50%', alignItems:'center'   }} onPress={this.selectGaleriaBgimage}>
            <Icon style={General.searchIcon} 
              name='picture-o'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,  }}>Galeria</Text>
          </TouchableOpacity>
          
          <TouchableOpacity style={{ width: '50%',alignItems:'center'   }}  onPress={this.selectCameraBgimage}>
            <Icon style={General.searchIcon} 
              name='camera'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,  }}>Camera</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
    
  }

  render() {
    if( this.state.bgimage === "" ){
      var bgImage = "";
      
    } else {
      if( this.state.bgimage.substr(0,4) == 'http' ){
        var bgImage = this.state.bgimage;
      } else {
        var bgImage = "data:image/jpeg;base64,"+this.state.bgimage;
      }
    }
    
    return (
      
      <View style={{ flex: 1, backgroundColor: '#f9f9f9', padding: 0}}>

        <View style={{ width: '100%', height: '100%', backgroundColor: 'white', borderRadius: 5, }}>
          
          <Modal
            isVisible={this.state.visibleModal === 'photos'}
            useNativeDriver={true}
            onSwipeComplete={() => this.setState({ visibleModal: null })}
            swipeDirection={['down','up','left']}
            onBackdropPress={() => this.setState({ visibleModal: null })}
          >
            {this.renderModalContent()}
          </Modal>

          <Modal
            isVisible={this.state.visibleModal === 'tooltip'}
            hasBackdrop={false}
            useNativeDriver={true}
            animationIn="fadeIn"
            animationOut="fadeOut"
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalTooltipContent()}
          </Modal>
          
          <Animated.View style={{ zIndex: 1, marginTop:0, height: this.animatedHeaderHeight, backgroundColor: 'white', borderBottomWidth: 1, borderBottomColor: '#FFF',borderTopWidth: 0, borderTopColor: '#FFF' }}>

            { this.state.isLoadingBg && 
              <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color="green"/>
              </View>
            }
            { !this.state.isLoadingBg && 
              <Animated.View>
                <TouchableOpacity onPress={() => this.setState({ visibleModal: 'photos',togglePhoto: 'bgImage' })}>
                { this.state.bgimage === '' && 
                  <Image source={require('../images/studentlearning_default.jpg')} resizeMode={'cover'} style={{ width:'100%', height: '100%'}}/>
                } 
                { this.state.bgimage !== '' && 
                  <Image source={{ uri: bgImage }} resizeMode="cover" style={{ width:'100%', height: '100%', backgroundColor: '#e9e9e9'}}/>
                }
                </TouchableOpacity>
              </Animated.View>
            }     

          </Animated.View>    
        
          <ScrollView
            scrollEventThrottle={16}
            style={{ backgroundColor: '#FFF',width: '100%'}}
            onScroll={Animated.event(
                [
                    { nativeEvent: { contentOffset: { y: this.scrollY } } },
                    
                ]
            )}> 
            <KeyboardAvoidingView style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
            <View style={{ width: '90%', padding: 0, marginTop: 10,   }}>
              
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                <View style={{ marginTop: 8 }}>
                  { this.state.isLoadingAvatar && 
                    <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                      <ActivityIndicator size="large" color="green"/>
                    </View>
                  }
                  { !this.state.isLoadingAvatar && 
                    <TouchableOpacity style={{ marginTop: 20, marginRight: 20, marginLeft: 20 }} onPress={() => this.setState({ visibleModal: 'photos',togglePhoto: 'avatar' })}>
                    { this.state.avatar === undefined && 
                      <Avatar 
                        rounded
                        size="large"
                        source={require('../images/avatar.png')}
                        activeOpacity={1}
                      />
                    } 
                    { this.state.avatar !== undefined && 
                      <Avatar 
                        rounded
                        size="large"
                        source={{uri: this.state.avatar }}
                        activeOpacity={0.9}
                      />
                    }
                    </TouchableOpacity>
                  }
                </View> 
              
                <View style={{ width: '74%', marginLeft: 10 }}>
                  <Text style={General.inputLabel}>Apelido</Text>
                  <TextInput
                    ref = {(input) => this.input1 = input}
                    onSubmitEditing={() => this.input2.focus()}
                    onChangeText={this.handleApelido}
                    value={this.state.apelido}                  
                    returnKeyType='next'
                    placeholder='Como quer ser chamado(a) ?'
                    style={General.formInput}
                  />
                </View>  
            
              </View>  

              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Nome</Text>
                <TextInput
                  ref = {(input) => this.input2 = input}
                  onSubmitEditing={() => this.input3.focus()}
                  value={this.state.nome}
                  autoCorrect={false}
                  onFocus={this.onFocus}
                  onChangeText={this.handleNome}
                  returnKeyType='next'
                  placeholder='Nome Completo'
                  style={General.formInput}
                  />
              </View>  

              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>E-mail</Text>
                <TextInput
                  ref = {(input) => this.input3 = input}
                  onSubmitEditing={() => this.input4.focus()}
                  autoCompleteType={'email'}
                  value={this.state.email}
                  autoCorrect={false}
                  onFocus={this.onFocus}
                  onChangeText={this.handleEmail}
                  returnKeyType='next'
                  placeholder='Digite seu e-mail'
                  style={General.formInput}
                />
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <Text style={General.inputLabel}>Nascimento</Text>
                  <TextInputMask
                    refInput={(ref) => this.input4 = ref}
                    onSubmitEditing={() => this.input5.focus()}
                    onChangeText={this.handleNascimento}
                    type={'datetime'}
                    options={{
                      format: 'DD/MM/YYYY'
                    }}
                    style={General.formInput}
                    value={this.state.dt_nascimento}
                    returnKeyType = {"next"}
                    placeholder="Nascimento"
                    underlineColorAndroid="transparent"
                    blurOnSubmit={false}
                  />
                </View>  
                <View style={{ width: '49%' }}>
                  <Text style={General.inputLabel}>Celular</Text>
                  <TextInputMask
                    refInput={(ref) => this.input5 = ref}
                    onSubmitEditing={() => this.input6.focus()}
                    onChangeText={this.handleCelular}
                    value={this.state.celular}
                    type={'cel-phone'}
                    options={{
                        maskType: 'BRL',
                        withDDD: true,
                        dddMask: '(99) '
                    }}
                    style={General.formInput}
                    returnKeyType = {"next"}
                    placeholder="(DDD) Celular"
                    underlineColorAndroid="transparent"
                    blurOnSubmit={false}
                  />
                </View>  
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>CEP</Text>
                    <TextInputMask
                      refInput={(ref) => this.input6 = ref}
                      onSubmitEditing={() => this.input7.focus()}
                      onChangeText={this.handleCep}
                      value={this.state.cep}
                      type={'zip-code'}
                      style={General.formInput}
                      returnKeyType = {"next"}
                      placeholder="CEP"
                      underlineColorAndroid="transparent"
                      blurOnSubmit={false}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>UF</Text>
                    <TextInput
                      ref = {(input) => this.input7 = input}
                      onSubmitEditing={() => this.input8.focus()}
                      onChangeText={this.handleUF}
                      value={this.state.uf}
                      autoCorrect={false}
                      returnKeyType='next'
                      placeholder='UF'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View>  

              <View style={{  }}>
                <Text style={General.inputLabel}>Logradouro</Text>
                <TextInput
                  ref = {(input) => this.input8 = input}
                  onSubmitEditing={() => this.input9.focus()}
                  onChangeText={this.handleLogradouro}
                  value={this.state.logradouro}                  
                  returnKeyType='next'
                  placeholder='Logradouro'
                  style={General.formInput}
                />
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Numero</Text>
                    <TextInput
                      ref = {(input) => this.input89= input}
                      onSubmitEditing={() => this.input10.focus()}
                      onChangeText={this.handleNumero}
                      value={this.state.numero}                  
                      returnKeyType='next'
                      placeholder='Número'
                      style={General.formInput}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Complemento</Text>
                    <TextInput
                      ref = {(input) => this.input10 = input}
                      onSubmitEditing={() => this.input11.focus()}
                      onChangeText={this.handleComplemento}
                      value={this.state.complemento}                           
                      returnKeyType='next'
                      placeholder='Complemento'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View> 

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Bairro</Text>
                    <TextInput
                      ref = {(input) => this.input11 = input}
                      onSubmitEditing={() => this.input12.focus()}
                      onChangeText={this.handleBairro}
                      value={this.state.bairro}                           
                      returnKeyType='next'
                      placeholder='Bairro'
                      style={General.formInput}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Cidade</Text>
                    <TextInput
                      ref = {(input) => this.input12 = input}
                      onSubmitEditing={() => this.input13.focus()}
                      onChangeText={this.handleCidade}
                      value={this.state.cidade}                           
                      returnKeyType='next'
                      placeholder='Cidade'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View> 

              <View style={{ marginTop: 10, width:'96%', marginLeft: 10}}>
                <TouchableOpacity style={General.submit} onPress={() => this.handleSaveFormUser()   } >
                  <Text style={General.submitText}>SALVAR</Text>
                </TouchableOpacity>
              </View>
              
              <View style={{ flexDirection: 'row', height: 200 }}></View>
            </View>
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
      </View>
      
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Meusdados)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center'
  },
  content: {
    backgroundColor: 'white',
    padding: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentInput: {
    paddingTop: -10,
    paddingBottom: 0,
    marginBottom: 0
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  
});