import React,{ Component } from 'react';
import { 
    View, 
    Text,
    BackHandler,
    ImageBackground
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CountDown from 'react-native-countdown-component';
    
class Espera extends Component {

    constructor(props){
      super(props);

      this.state = {
        isLoading: true,
        timer: 300,
        channel: '',
        id_usuario_aluno: '',
        id_usuario_professor: '',
        qtdaula: '',
        totalMinutos: '',
        id_aula: '',
        comecar: false,
        msgAtual: 0,
        origem: '',
        msg: [
          'No final da contagem você será redirecionado para a sala. \nEstamos quase lá :D',
          'Já comece a organizar suas coisas antes... Dê aquela última conferida nos seus materiais, pegue o café, uma água...',
          'Não esqueça de avaliar seu professor no final da aula. \nSua opinião conta muito para nós ;)',
        ]
      };

      this.clockCall = "";
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
          return true;
      });

      this.timerFrases = null;
      this.startFrases = this.startFrases.bind(this);
    }

    changeCount() {

      const total = this.state.msg.length;

      if( this.state.msgAtual < (total-1) ){
        this.setState({ 
          msgAtual: (this.state.msgAtual+1)
        })
      } else {
        this.setState({ 
          msgAtual: 0
        })
      }

    }

    startFrases() {
      const { comecar } = this.state;
      if (!comecar) {
        this.timerFrases = setInterval(() => {
          this.changeCount()
        }, 10000);
        this.setState({ comecar: true });
      }
      else {
        clearInterval(this.timerFrases);
        this.setState({ comecar: false });
      }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    componentDidMount(){
      var channel = this.props.navigation.getParam('channelName','AAA');
      var id_aula = this.props.navigation.getParam('id_aula', 'aaa');
      var totalMinutos = this.props.navigation.getParam('totalMinutos',1);
      var origem = this.props.navigation.getParam('origem','findteacher');
      var timer  = this.props.navigation.getParam('contadorEmSegundos',0);

      if( origem == 'findteacher' ){
        var timer = 30;
      }

      this.setState({
          channel,
          id_aula,
          totalMinutos,
          origem,
          timer
      }, () => {
        this.startFrases()
      })
    }

    aulaLiberada = async () => {
      this.setState({ isLoading: false });

      var id = await AsyncStorage.getItem('@StudyLiveApp:id') || false;
      var tipousuario = await AsyncStorage.getItem('@StudyLiveApp:tipousuario') || false;
      
      clearInterval(this.timerFrases);
      
      this.props.navigation.navigate("Agora", {
        channelName: this.state.channel,
        id_aula: this.state.id_aula,
        uid: id,
        totalMinutos: this.state.totalMinutos,
        tipousuario: tipousuario,
      })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#104B59'}}>
                
                <View style={{ flex: 3, marginLeft: 15, marginRight: 15, paddingTop: 30,  }}>
                
                {this.state.isLoading &&
                    <View style={{ flex: 1, justifyContent:'space-between', paddingBottom: 20}}>
                      <View style={{  alignItems: 'flex-start'}}>
                        <Text style={{ fontSize: 22,fontFamily: 'Montserrat-Bold', color: '#FFF' }}>Sua aula começa em...</Text>
                        <View style={{ marginTop: -10 }}>
                          <CountDown
                            until={this.state.timer}
                            onFinish={() => this.aulaLiberada() }
                            digitStyle={{backgroundColor: '#104B59', marginLeft: 0, paddingLeft: 0}}
                            digitTxtStyle={{fontSize: 52, marginTop: 10,fontFamily: 'Montserrat-Regular', color: '#FFF'}}
                            size={30}
                            showSeparator
                            separatorStyle={{color: '#FFF', marginTop: 5}}
                            timeToShow={['M', 'S']}
                            timeLabels={{m: '', s: ''}}
                          />
                        </View>
                      </View>
                      <View style={{  alignItems: 'center', width: '100%', paddingRight: 10}}>
                        <Text style={{ justifyContent: 'flex-end', fontSize: 13, lineHeight: 30, fontFamily: 'Montserrat-Regular', color: '#FFF' }}>
                          {this.state.msg[this.state.msgAtual]}
                        </Text>
                      </View>  
                    </View>  
                    
                }
                </View>

                <View style={{ flex: 4, justifyContent: 'center', alignItems: 'flex-end'}}>
                  <ImageBackground
                    source={require('../images/studentlearning_zoom.jpg')}
                    resizeMode="cover"
                    style={{ width: '105%', height: '100%' }}
                  />
                </View>
            </View>
        )
    }
}

export default Espera;


