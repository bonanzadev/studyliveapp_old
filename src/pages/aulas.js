import React,{ Component } from 'react';
import { View, Text,Image,TextInput,Platform,Dimensions,StyleSheet,TouchableOpacity,TouchableWithoutFeedback   } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';

import CreditoAtual from '../components/CreditoAtual';
import Categorias from '../components/Categorias';
import Assuntos from '../components/Assuntos';
import Divisao from '../components/Divisao';
import Avatar from '../components/avatar';
import Modal from "react-native-modal";
import { TextField } from 'react-native-material-textfield';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actions/todos'
import { Tooltip } from 'react-native-elements';

var {height, width} = Dimensions.get('window');
    
class Novaaula extends Component {

  constructor(props) {
    super(props);
    this.tooltip1 = React.createRef();
  }

  state = {
    visibleModal: false,
    qtdAulas: "1",
    totalMinutos: "30",
    idCategoria: '',
    displayCategoria: 'Escolher...',
    idAssunto: '',
    displayAssunto: 'Escolher...',
  };

  componentDidMount(){
    const didBlurSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {

            this.loadDataRedux();          
            var saldoatual = this.props.todos.userDataConfig.creditoAtual;

            saldoFinal = saldoatual - this.state.qtdAulas;
            this.setState({ saldoFinal });
                    
        }
    );
  }

  loadDataRedux = () => {
    const { novaAula  } = this.props.todos;

    this.setState({
        idCategoria: novaAula.idCategoria,
        displayCategoria: novaAula.displayCategoria,
    })
  }

  editNovaaula = () => {
    this.props.editNovaaula(this.state);
  }

  renderModalContent = () => (
    <View style={styles.content} onPress={ this.closeModal }>
      
      <Text style={styles.contentTitle}>Selecione a Disciplina</Text>
      
      <Categorias hasTitle={false} myFunc={this.setDisciplina} />
      
    </View>
  );

  renderModalAssuntoContent = () => (
    <View style={styles.content2} onPress={ this.closeModal }>
      
      <Text style={styles.contentTitle}>Selecione o Assunto</Text>
      
      <ScrollView>
        <Assuntos idCategoria={this.state.idCategoria} myFunc={this.setAssunto} />
      </ScrollView>
      
    </View>
  );

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'center'}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  plusQtdAula = () => {
    
    var atual = Number(this.state.qtdAulas);
    var totalMinutos = this.state.totalMinutos;
    var saldoatual = this.props.todos.userDataConfig.creditoAtual

    if(atual<5){
      atual = atual + 1;
      totalMinutos = atual * 30;
      saldoFinal = saldoatual - atual;
      this.setState({ qtdAulas: atual,totalMinutos,saldoFinal });
    } else {
      this.communicate("Máximo de aulas é 5",1000);
    }
  }

  minusQtdAula = () => {
    
    var atual = Number(this.state.qtdAulas);
    var totalMinutos = this.state.totalMinutos;
    var saldoatual = this.props.todos.userDataConfig.creditoAtual;

    if(atual>1){
      atual = atual - 1;
      totalMinutos = atual * 30;
      saldoFinal = saldoatual - atual;
      this.setState({ qtdAulas: atual,totalMinutos,saldoFinal });
    } else {
      this.communicate("O mínimo é 1 aula",1000);
    }
  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip,visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  setDisciplina = (id,nome) => {
    this.setState({
        idCategoria: id,
        displayCategoria: nome
    })

    this.closeModal()
  }

  setAssunto = (id,nome) => {
    this.setState({
        idAssunto: id,
        displayAssunto: nome
    })

    this.closeModal()
  }

  openModalCategoria = () => {
    this.setState({ visibleModal: 'categoria'})
  }
  openModalAssunto = () => {
    if( this.state.idCategoria != "" ){
      this.setState({ visibleModal: 'assunto'})
    } else {
      this.communicate("Escolha uma disciplina antes",1000);
    }
  }
  closeModal = () => {
    this.setState({ visibleModal: null, toggleModal: false })
  }

  handleQtde = (qtdAulas) => { this.setState({ qtdAulas })}

  render() {
    const {navigate} =this.props.navigation;
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f5f5'}}>

        <Modal
          isVisible={this.state.visibleModal === 'categoria'}
          onSwipeComplete={() => this.closeModal() }
          useNativeDriver={true}
          swipeDirection={['down','up',]}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={1000}
          onBackdropPress={() => this.closeModal() }
        >
          {this.renderModalContent()}
        </Modal>

        <Modal
          isVisible={this.state.visibleModal === 'assunto'}
          onBackdropPress={() => this.closeModal() }
          useNativeDriver={true}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={1000}
        >
          {this.renderModalAssuntoContent()}
        </Modal>
        <Modal
          isVisible={this.state.visibleModal === 'tooltip'}
          hasBackdrop={false}
          useNativeDriver={true}
          animationIn="fadeIn"
          animationOut="fadeOut"
          onBackdropPress={() => this.closeModal() }
        >
          {this.renderModalTooltipContent()}
        </Modal>
        
        <ScrollView>
            <View style={{ width:"100%", paddingLeft: 15, paddingRight: 15, marginTop: 30  }}>
                
              <View style={{ width: '94%' }}>
                <Text style={General.inputLabel}>Disciplina</Text>
                  <TouchableOpacity  style={General.formInput} onPress={() => this.openModalCategoria() }>
                    <Text style={{ textAlign: 'left', fontSize: 14, fontWeight: '600', color: '#777', marginTop: 12 }}>{this.state.displayCategoria}</Text>
                  </TouchableOpacity >
              </View>
              <View style={{ width: '94%' }}>
                <Text style={General.inputLabel}>Assunto</Text>
                  <TouchableOpacity  style={General.formInput} onPress={() => this.openModalAssunto() }>
                    <Text style={{ textAlign: 'left', fontSize: 14, fontWeight: '600', color: '#777', marginTop: 12 }}>{this.state.displayAssunto}</Text>
                  </TouchableOpacity >
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ width: '50%' }}>
                  <Text style={General.inputLabel}>Quantas Aulas?</Text>
                  <View style={General.formInputNoPadding}>
                    <View style={{ flexDirection: 'row',   }}>
                      <TouchableOpacity style={{ width: '34%',  }} onPress={() => this.minusQtdAula() }>
                        <Icon style={{ alignSelf: 'center', marginTop: 10}} 
                          name='minus'
                          size={24}
                          color='black'
                        />
                      </TouchableOpacity>  
                      <Text style={styles.inputQtdAulas}>{this.state.qtdAulas}</Text>
                      <TouchableOpacity style={{ width: '30%' }} onPress={() => this.plusQtdAula() }>
                        <Icon style={{ alignSelf: 'center', marginTop: 10 }} 
                          name='plus'
                          size={24}
                          color='black'
                        />
                      </TouchableOpacity>  
                    </View>
                  </View> 
                </View>
                <View style={{ width: '50%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Tempo Total</Text>
                    <Text style={{ textAlign: 'center', fontSize: 16, color: '#5BCCC2', fontWeight: 'bold', marginTop: 12 }}>
                      {this.state.totalMinutos} Minutos
                    </Text>
                  </View>  
                </View>
              </View> 

            </View>
            
            <View style={{ width:"100%", marginTop: 15, justifyContent: 'center', alignItems: 'center'  }}>
              
              <View style={{ width:"100%", flexDirection: 'row', marginTop: 15  }}>
                <View style={{ width: '50%' }}>
                  <Text style={{ fontSize: 16, textAlign: 'right', fontWeight: 'bold', marginLeft: 10, marginTop: 10 }}>Saldo Atual</Text>
                  <Text style={{ fontSize: 16, textAlign: 'right', fontWeight: 'bold', marginLeft: 10, marginTop: 10 }}>Selecionado</Text>
                  <Text style={{ fontSize: 16, textAlign: 'right', fontWeight: 'bold', marginLeft: 10, marginTop: 10 }}>Saldo Restante</Text>
                </View>
                <View style={{ width: '50%' }}>
                  <Text style={{ fontSize: 16, color: '#5BCCC2', textAlign: 'center', fontWeight: 'bold', marginLeft: 10, marginTop: 10 }}>{this.props.todos.userDataConfig.creditoAtual} Aulas</Text>
                  <Text style={{ fontSize: 16, color: '#777', textAlign: 'center', fontWeight: 'bold', marginLeft: 10, marginTop: 10 }}>{this.state.qtdAulas} Aulas</Text>
                  <Text style={{ fontSize: 16, color: '#5BCCC2', textAlign: 'center', fontWeight: 'bold', marginLeft: 10, marginTop: 10 }}>{this.state.saldoFinal} Aulas</Text>
                </View>
              </View>
              
              <View style={{ marginTop: 20, width:'90%', marginLeft: 15}}>
                <TouchableOpacity style={General.submit} onPress={() => navigate('Findteacher') }>
                  <Text style={General.submitText}>CONFIRMAR</Text>
                </TouchableOpacity>
              </View>
            </View>
        </ScrollView>   
        
        
      </View>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Novaaula)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center'
  },
  content: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  content2: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentInput: {
    paddingTop: -10,
    paddingBottom: 0,
    marginBottom: 0
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  inputQtdAulas: {
    width: '34%', 
    textAlign: 'center', 
    fontWeight: '600', 
    fontSize: 18, 
    color: '#333', 
    paddingTop: 9, 
    height: 44, 
    borderWidth: 1, 
    borderColor: '#e9e9e9', 
    backgroundColor: 'white',
    borderTopWidth: 0, 
    borderBottomWidth: 0
  }
  
});