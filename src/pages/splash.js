/*This is an Example of Animated Splash Screen*/
import React, { Component } from 'react';
import { View, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {StackActions, NavigationActions } from 'react-navigation';
import api from '../services/api';
import url from '../services/url';
import Modal from "react-native-modal";
import { PulseIndicator } from 'react-native-indicators';

import FotoAvatar from '../components/FotoAvatar';
import FotoBgimage from '../components/FotoBgimage';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actions/todos';
class Splash extends Component {

  state = { 
    id: '', 
    apelido: '', 
    nome: '', 
    email: '', 
    login: '', 
    senha: '', 
    repitasenha: '', 
    id_tipousuario: '', 
    configurouconta: '', 
    tokenFcm: '', 
    isLoading: false,
    username: '', 
    password: '', 
    assets: url,
    visibleModal: ''
  };
  
  componentDidMount(){
    this.verificaStorage();
  }

  verificaStorage = async () => {

    this.setState({ visibleModal: 'asyncStorage' })

    const login = await AsyncStorage.getItem('@StudyLiveApp:login') || false;
    const senha = await AsyncStorage.getItem('@StudyLiveApp:senha') || false;
    
    if( login !== false && senha !== false){
      
      this.setState({ 
        login: login, 
        senha: senha, 
      })
        
      this.handleSignIn();
    } else {
      this.setState({ visibleModal: '' })
    
      setTimeout(() => {
        this.prosseguir('Login');
      }, 1000 );
    }

    this.setState({ visibleModal: '',isLoading: false })
  }

  prosseguir = (rt, par = {} ) => {
    
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: rt, origem:"login", params: par })],
    });
  
    this.props.navigation.dispatch(resetAction)
  }

  handleSignIn = async () => {

    this.setState({isLoading: true});
      
    try{
      //console.log('antes: ',this.state);
      const response = await api.post('/login',{
        login: this.state.login,
        senha: this.state.senha,
      });
      console.log('depois: ',response);

      const{ data } = response.data;

      if( response.data.errors == "" ){
        
        if(data.bgimage==null){
          var bg = "default";
        } else {
          var bg = FotoBgimage(data.id,data.bgimage);
        }

        if(data.avatar==null){
          var av = "default";
        } else {
          var av = FotoAvatar(data.id,data.avatar);
        }

        if(data.saldo==null){
          var saldo = "0";
        } else {
          var saldo = data.saldo;
        }
        
        this.setState({ 
          id: data.id, 
          id_tipousuario: data.id_tipousuario, 
          apelido: data.apelido, 
          nome: data.nome, 
          login: data.login, 
          email: data.email, 
          celular: data.celular,
          configurouconta: data.configurouconta, 
          tokenFcm: data.token_fcm || '', 
          saldo: saldo,
          bgimage: bg,
          avatar: av,
          logged: true,
        });

          /* Salva dados no Redux  */
        this.editUserdatageral();
        
        await AsyncStorage.multiSet([
          ['@StudyLiveApp:id',String(this.state.id)], 
          ['@StudyLiveApp:login',this.state.login],
          ['@StudyLiveApp:senha',this.state.senha],
          ['@StudyLiveApp:tipousuario',String(this.state.id_tipousuario)],
          ['@StudyLiveApp:tokenFcm',this.state.tokenFcm],
        ]);
        
        const respAula = await api.get(`/estouEmAula/${this.state.id}`);
        
        if( respAula.data.data != "" ){
          
          const resp = respAula.data.data;
          
          var rt = "Agora";

          this.prosseguir( rt,{
            channelName: resp.channel,
            id_aula: resp.id,
            uid: this.state.id,
            totalMinutos: (resp.qtdaula*30), /* Valor em minutos */
            tempoAtual: Math.round(resp.contadorEmSegundos/60), /* Valor em minutos */
            tipousuario: this.state.id_tipousuario,
          })

        } else {
          
          this.setState({ visibleModal: '' })

          if( this.state.id_tipousuario == 1 ){
            var rt = "Home";
          } else {
            
            if(this.state.configurouconta==null){
              var rt = "Configsprofessor";
            } else {
              var rt = "Homeprofessor";
            }
          }
          
          this.prosseguir(rt);
        }
        
      /*  */  
      } else {
        
        this.prosseguir('Login');
      }
      
    } catch (response) {
      console.log('ui ',response)
      this.prosseguir('Login');
    }

    this.setState({isLoading: false});
  }

  editUserdatageral = () => {
    this.props.editUserdatageral(this.state);
  }
 
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#156C7F'
        }}>
          <View style={{ position: 'absolute', zIndex: 2, opacity: 0.5 }}>    
            <PulseIndicator size={200} color='#3981A5' />
          </View>
          <ImageBackground
            source={require('../images/studylogo1.png')}
            resizeMode="center"
            style={{ width: 92, height: 62, position: 'absolute', zIndex: 3 }}    
          />
      </View>
    );
  }
}


const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Splash)