import React,{ Component } from 'react';
import { 
  View, 
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  KeyboardAvoidingView,
  FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';

import api from '../services/api';
import url from '../services/url';
import domain from '../services/domain';
import Fonts from '../styles/fonts';

import Avatar from '../components/avatar';
import Modal from "react-native-modal";
import { TextInputMask } from 'react-native-masked-text';
import FotoAvatar from '../components/FotoAvatar';
import FotoBgimage from '../components/FotoBgimage';
import Bancos from '../components/Bancos';
import CheckBoxItem from '../components/CheckBoxItem';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actions/todos'
import ImagePicker from 'react-native-image-crop-picker';
import Incoming from '../components/Incoming';  
import firebase from 'react-native-firebase';
    
class Meusdados_professor extends Component {
  state = {
    data: [],
    datasubcategoria: [],
    selectedBoxes: [],
    selected: [],
    refreshing: false,
    isLoading: true,
    isLoading2: false,
    showPaginate: true,
    visibleModal: '',
    categoriaClicada: '',
    idcategoriaClicada: '',
    togglePhoto: '',
    id: '',
    nome: '',
    apelido: '',
    login: '',
    cpf: '',
    rg: '',
    email: '',
    celular: '',
    dt_nascimento: '',
    uf: '',
    cep: '',
    logradouro: '',
    numero: '',
    complemento: '',
    bairro: '',
    cidade: '',
    bgimage: '',
    avatar: '',
    id_banco: '',
    displayBanco: '',
    agencia: '',
    tipoconta: 1,
    contacorrente: '',
    poupanca: '',
    variacaopoupanca: '',
    configurouconta: '',
    tooltip2: 'SALVAR',
    isLoadingAvatar: true,
    saldo: '',
    aulaAtual: '',
    incoming: false,
    incomingAulaObj: []
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  editUserdatageral = () => {
    this.props.editUserdatageral(this.state);
    //this.voltar();
  }

  voltar = () => {
    this.props.navigation.navigate('Homeprofessor',{origem: 'Meusdados',msg:""});
  }

  verificaStorage = async () => {
    var id = await AsyncStorage.getItem('@StudyLiveApp:id') || null;

    const response = await api.get(`/usuario/${id}`);
    const {usuario,subcategorias} = response.data.data;
    
    if(usuario){
      let avatar = FotoAvatar(id,usuario.avatar);
      let bgimage = "";

      this.setState({ 
        id: id,
        nome: usuario.nome|| "",
        apelido: usuario.apelido|| "",
        login: usuario.login|| "",
        id_tipousuario: usuario.id_tipousuario|| "",
        cpf: usuario.cpf|| "",
        rg: usuario.rg|| "",
        email: usuario.email|| "",
        dt_nascimento: usuario.dt_nasc|| "",
        celular: usuario.celular|| "",
        uf: usuario.uf|| "",
        cep: usuario.cep|| "",
        logradouro: usuario.logradouro|| "",
        numero: usuario.numero|| "",
        complemento: usuario.complemento|| "",
        cidade: usuario.cidade|| "",
        bairro: usuario.bairro|| "",
        id_banco: usuario.id_banco|| "",
        displayBanco: usuario.displayBanco|| "",
        agencia: usuario.agencia|| "",
        tipoconta: usuario.tipoconta || 1|| "",
        contacorrente: usuario.contacorrente|| "",
        poupanca: usuario.poupanca|| "",
        saldo: usuario.saldo || "",
        variacaopoupanca: usuario.variacaopoupanca|| "",
        configurouconta: usuario.configurouconta|| "",
        avatar: avatar,
        bgimage: bgimage,
        isLoading: false,
        isLoadingAvatar: false,
        selectedBoxes: subcategorias|| "",
      });
    }
  }

  loadData = async () => {
    this.setState({isLoading: true});
    
    const response = await api.get(`/categoria`);
    console.log(response)
    
    this.setState({
        data: response.data,
        refreshing: false,
        isLoading: false,
    })
  }

  loadDataSubcategoria = async (id,nome) => {

    this.setState({isLoading2: true});
    
    const response = await api.get(`/subcategoria/${id}`);
    console.log(response)
    
    this.setState({
        datasubcategoria: response.data,
        refreshing: false,
        isLoading2: false,
        visibleModal: 'subcategorias',
        categoriaClicada: nome,
        idcategoriaClicada: id
    })
  }

  componentDidMount() {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      this.IncomingAula(notification.data.dados);
    });
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notification) => {
      this.IncomingAula(notification.notification.data.dados);
    });
    this.verificaStorage();
    this.loadData();
  }

  componentWillUnmount(){
    this.notificationListener();
    this.notificationOpenedListener();
  }

  IncomingAula = (obj) => {
    this.setState({ 
      incomingAulaObj: obj
    },() => {
        /* Chama o Component depois de preencher as variaveis acima */
        this.setState({ incoming: true  })
      });
  }

  closeModal = () => {
    this.setState({ visibleModal: null, incoming: false })
  }

  _renderItem = ({item}) => {
    
    let index = this.state.selectedBoxes.findIndex(obj => obj.idcat === item.id)
    return index !== -1 ?
    (
      <TouchableOpacity activeOpacity={0.9} key={item.id} style={styles.box} onPress={() => {this.loadDataSubcategoria(item.id,item.nome)}}>
        <View style={[styles.backdrop,{backgroundColor: 'orange'}]}></View>
        <Text style={styles.categoria}>{item.nome}</Text>
        <Image source={{uri: url+item.nomefoto+".png" }} resizeMode="cover"  style={styles.img} />
      </TouchableOpacity>
    ) : (
      <TouchableOpacity activeOpacity={0.9} key={item.id} style={styles.box} onPress={() => {this.loadDataSubcategoria(item.id,item.nome)}}>
        <View style={styles.backdrop}></View>
        <Text style={styles.categoria}>{item.nome}</Text>
        <Image source={{uri: url+item.nomefoto+".png" }} resizeMode="cover"  style={styles.img} />
      </TouchableOpacity>
    )
  }

  renderMoralSubcategorias = () => (
    <View style={styles.content}>
      
      <View style={{ flexDirection: 'row', justifyContent: "space-between", width: '100%', marginBottom: 10  }}>
        <Text style={[styles.title,{marginTop: 5, color: '#555' }]}>{this.state.categoriaClicada}</Text>
        <TouchableOpacity onPress={this.closeModal}>
          <Icon style={{ margin: 5 }}
            name="close"
            size={22}
            color={'#29A9AB'}  
          />
        </TouchableOpacity>
      </View>
      <ScrollView style={{ height: '80%', width: '100%' }}>
                    
          <FlatList
              style={{ marginTop: 5}}
              data={this.state.datasubcategoria}
              renderItem={
                  this._renderItemSubcategoria
              }
              keyExtractor={item => item.id_assunto}
          />
          
      </ScrollView>
      <TouchableOpacity style={styles.btn} onPress={() => {this.closeModal()}}>
        <Icon 
            style={{ marginTop: 3}} 
            name='check'
            size={28}
            color='#FFF'
        />
      </TouchableOpacity>
    </View>
  );

  closeModal = () => {
    this.setState({ visibleModal: ''})
  }

  _renderItemSubcategoria = ({item}) => {

        
    let index = this.state.selectedBoxes.findIndex(obj => obj.id === item.id)

    if (index != -1) {
        var check = true;
    } else {
        var check = false;
    }
    
    return (
        <View key={item.id} style={styles.subcategoria_box}>
            
            <CheckBoxItem label={item.nome} checked={check} onUpdate={this.onUpdate.bind(this,item.id,item.nome)}/>
            
        </View>
    )
  }

  setBanco = (id,nome) => {
    this.setState({
        id_banco: id,
        displayBanco: nome
    })

    this.closeModal()
  }

  renderModalBancoContent = () => (
    <View style={styles.content} onPress={ this.closeModal }>
      
      <Text style={styles.contentTitle}>Selecione seu Banco</Text>
      
      <Bancos myFunc={this.setBanco} />
      
    </View>
  );

  onUpdate = (id,nome) => {
        
    this.setState(previous => {
      
        let selectedBoxes = previous.selectedBoxes;
        //let index = selectedBoxes[id].indexOf(id) 
        let index = selectedBoxes.findIndex(obj => obj.id === id)
        if (index === -1) {
            selectedBoxes.push({
                id: id, nome: nome, idcat: this.state.idcategoriaClicada
            }) 
        } else {
            //selectedBoxes[id].splice(index, 1)
            selectedBoxes.splice(index, 1)
        }
        return { selectedBoxes }; 

    }); 
  }

  handleApelido = (apelido) => { this.setState({ apelido }) }
  handleNome = (nome) => { this.setState({ nome }) }
  handleCelular = (celular) => { this.setState({ celular })}
  handleCPF = (cpf) => { this.setState({ cpf })}
  handleCep = (cep) => { this.setState({ cep })}
  handleUF = (uf) => { this.setState({ uf })}
  handleLogradouro = (logradouro) => { this.setState({ logradouro })}
  handleNumero = (numero) => { this.setState({ numero })}
  handleComplemento = (complemento) => { this.setState({ complemento })}
  handleBairro = (bairro) => { this.setState({ bairro })}
  handleCidade = (cidade) => { this.setState({ cidade })}
  handleAgencia = (agencia) => { this.setState({ agencia })}
  handleCC = (contacorrente) => { this.setState({ contacorrente })}
  handlePoupanca = (poupanca) => { this.setState({ poupanca })}
  handleVariacao = (variacao) => { this.setState({ variacao })}

  handleSaveFormUser = async () => {

    this.setState({ isLoading: true });
    
    var resp = "";

    if(this.state.apelido==""){ resp += "\n- Como você quer ser chamado?"; }
    if(this.state.nome==""){ resp += "\n- Nome Completo"; }
    if(this.state.email==""){ resp += "\n- E-mail"; }
    if(this.state.dt_nascimento==""){ resp += "\n- Data de Nascimento"; }
    if(this.state.celular==""){ resp += "\n- Número de Celular"; }
    if(this.state.cpf==""){ resp += "\n- CPF"; }
    if(this.state.cep==""){ resp += "\n- CEP"; }
    if(this.state.uf==""){ resp += "\n- Estado"; }
    if(this.state.logradouro==""){ resp += "\n- Logradouro"; }
    if(this.state.uf==""){ resp += "\n- Estado"; }
    if(this.state.numero==""){ resp += "\n- Número"; }
    
    if(this.state.bairro==""){ resp += "\n- Bairro"; }
    if(this.state.cidade==""){ resp += "\n- Cidade"; }
    if(this.state.agencia==""){ resp += "\n- Agência"; }
    if(this.state.tipoconta==1){ 
      if(this.state.contacorrente==""){ resp += "\n- Conta Corrente"; }
    } else {
      if(this.state.poupança==""){ resp += "\n- Poupança"; }
    }

    if(resp != ""){
      this.communicate("Preencha os campos: \n"+resp,5000);
      this.setState({ isLoading: false });
    } else {
      
      try {
        const response = await api.put(`/usuario/${this.state.id}`,this.state);

        if( response.data.affectedRows > 0){

          this.editUserdatageral();
          this.setState({ isLoading: false });
          this.voltar();
        } else {
          this.setState({ isLoading: false });
        }
      } catch (response) {
        console.log(response)
        this.setState({error: true, isLoading: false, tooltip2: "Dificuldades técnicas..."});
      }
      
    }
  }

  openModalBancos = () => {
    this.setState({ visibleModal: 'bancos'})
  }

  consultaCep = async () => {
        
    var cep = this.state.cep.replace("-","");

    if( cep == "" ){
        return false;
    }

    try{
        
        const response = await api.get(`https://viacep.com.br/ws/${cep}/json/`)
        console.log(response)
        if( response ){
            this.setState({
                logradouro: response.data.logradouro, 
                bairro: response.data.bairro,
                cidade: response.data.localidade,
                uf: response.data.uf
            });
        }
    } catch (response) {
        
        console.log(response)
        this.setState({error: response, buttonDisabled: false});
    }

  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip, visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'left', fontFamily: Fonts.Light}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  selectGaleriaAvatar = () => {
    
    this.setState({ isLoadingAvatar: true });

    ImagePicker.openPicker({
      width: 200,
      height: 200,
      cropping: true,
      compressImageMaxWidth: 200,
      compressImageMaxHeight: 200,
      includeBase64: true
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleAvatarimage(image.data)
    });
  }

  selectCameraAvatar = async () => {
    ImagePicker.openCamera({
      width: 200,
      height: 200,
      cropping: true,
      compressImageMaxWidth: 200,
      compressImageMaxHeight: 200,
      includeBase64: true
    }).then(image => {
      if( image.data.length !== 0 ){
        this.setState({ visibleModal: '' })
      }
      this.handleAvatarimage(image.data)
    });
  }

  handleAvatarimage = async (avatar) => { 
    
    const response = await api.post(`/avatar/${this.state.id}`,{data: avatar});
    
    if(response.data.data){
      
      let newavatar = FotoAvatar(this.state.id,response.data.data);
      
      this.setState({ avatar: newavatar,isLoadingAvatar: false });
      this.editUserdatageral()
    } else {
      this.setState({ isLoadingAvatar: false });
    }
  }

  renderModalContent = () => {
    return this.state.togglePhoto == 'avatar' ? (
      <View style={[styles.content,{height: 160}]}>

        <Text style={[styles.contentTitle,{marginTop: 15}]}>Escolher foto de perfil</Text>

        <View style={{ flexDirection:'row', paddingBottom: 15 }}>
          <TouchableOpacity style={{ width: '50%', alignItems:'center'   }} onPress={this.selectGaleriaAvatar}>
            <Icon style={General.searchIcon} 
              name='picture-o'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,fontFamily: Fonts.Light  }}>Galeria</Text>
          </TouchableOpacity>
          
          <TouchableOpacity style={{ width: '50%',alignItems:'center'   }}  onPress={this.selectCameraAvatar}>
            <Icon style={General.searchIcon} 
              name='camera'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16, fontFamily: Fonts.Light }}>Camera</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    ) : (
      <View style={styles.content}>

        <Text style={styles.contentTitle}>Escolher foto de capa</Text>

        <View style={{ flexDirection:'row', paddingBottom: 15 }}>
          <TouchableOpacity style={{ width: '50%', alignItems:'center'   }} onPress={this.selectGaleriaBgimage}>
            <Icon style={General.searchIcon} 
              name='picture-o'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,  }}>Galeria</Text>
          </TouchableOpacity>
          
          <TouchableOpacity style={{ width: '50%',alignItems:'center'   }}  onPress={this.selectCameraBgimage}>
            <Icon style={General.searchIcon} 
              name='camera'
              size={24}
              color='black'
            />
            <Text style={{ fontSize: 16,  }}>Camera</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
    
  }

  render() {
    
    return (
      
      <View style={{ flex: 1, backgroundColor: '#f9f9f9', padding: 0}}>

        <View style={{ width: '100%', height: '100%', backgroundColor: 'white', borderRadius: 5, }}>
          { this.state.incoming && 
            <Incoming dadosAula={this.state.incomingAulaObj} navigation={this.props.navigation} toggleStatus={this.state.incoming} />
          }
          <Modal
            isVisible={this.state.visibleModal === 'photos'}
            useNativeDriver={true}
            onSwipeComplete={() => this.setState({ visibleModal: null })}
            swipeDirection={['down','up','left']}
            onBackdropPress={() => this.setState({ visibleModal: null })}
          >
            {this.renderModalContent()}
          </Modal>

          <Modal
            isVisible={this.state.visibleModal === 'tooltip'}
            hasBackdrop={false}
            useNativeDriver={true}
            animationIn="fadeIn"
            animationOut="fadeOut"
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalTooltipContent()}
          </Modal>

          <Modal
            isVisible={this.state.visibleModal === 'subcategorias'}
            
            useNativeDriver={true}
            animationInTiming={300}
            animationOutTiming={300}
            backdropTransitionInTiming={500}
            backdropTransitionOutTiming={500}
          >
            {this.renderMoralSubcategorias()}
          </Modal>      
          <Modal
            isVisible={this.state.visibleModal === 'bancos'}
            useNativeDriver={true}
            animationIn="zoomInDown"
            animationOut="fadeOut"
            animationInTiming={600}
            animationOutTiming={600}
            backdropTransitionInTiming={300}
            onBackdropPress={() => this.closeModal() }
          >
            {this.renderModalBancoContent()}
          </Modal>
          
        
          <ScrollView style={{ backgroundColor: '#FFF',width: '100%'}}> 
            <KeyboardAvoidingView style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
            <View style={{ width: '90%', padding: 0, marginTop: 10,   }}>
              
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                <View style={{ borderWidth: 0, borderColor: 'white', borderRadius: 50, marginTop: 8 }}>
                  {this.state.isLoadingAvatar && 
                    <View style={{ width: 75, height: 75, backgroundColor: '#999', justifyContent: 'center', alignItems: 'center', borderRadius: 75 }}>
                      <ActivityIndicator size="large" color="white"/>
                    </View>
                  }
                  
                  {!this.state.isLoadingAvatar && 
                    <Avatar 
                      rounded
                      size="large"
                      showEditButton={true}
                      onEditPress={() => this.setState({ visibleModal: 'photos',togglePhoto: 'avatar' })}
                      onPress={() => this.setState({ visibleModal: 'photos',togglePhoto: 'avatar' })}
                      source={{uri: this.state.avatar }}
                      activeOpacity={0.7}
                    />
                  }
                </View> 
              
                <View style={{ width: '74%', marginLeft: 10 }}>
                  <Text style={General.inputLabel}>Apelido</Text>
                  <TextInput
                    ref = {(input) => this.input1 = input}
                    onSubmitEditing={() => this.input2.focus()}
                    onChangeText={this.handleApelido}
                    value={this.state.apelido}                  
                    returnKeyType='next'
                    placeholder='Como quer ser chamado(a) ?'
                    style={General.formInput}
                  />
                </View>  
            
              </View>  

              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Nome</Text>
                <TextInput
                  ref = {(input) => this.input2 = input}
                  onSubmitEditing={() => this.input3.focus()}
                  value={this.state.nome}
                  autoCorrect={false}
                  onFocus={this.onFocus}
                  onChangeText={this.handleNome}
                  returnKeyType='next'
                  placeholder='Nome Completo'
                  style={General.formInput}
                  />
              </View>  

              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>E-mail</Text>
                <TextInput
                  ref = {(input) => this.input3 = input}
                  onSubmitEditing={() => this.input4.focus()}
                  autoCompleteType={'email'}
                  value={this.state.email}
                  autoCorrect={false}
                  onFocus={this.onFocus}
                  onChangeText={this.handleEmail}
                  returnKeyType='next'
                  placeholder='Digite seu e-mail'
                  style={General.formInput}
                />
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <Text style={General.inputLabel}>Nascimento</Text>
                  <TextInputMask
                    refInput={(ref) => this.input4 = ref}
                    onSubmitEditing={() => this.input5.focus()}
                    onChangeText={this.handleNascimento}
                    type={'datetime'}
                    options={{
                      format: 'DD/MM/YYYY'
                    }}
                    style={General.formInput}
                    value={this.state.dt_nascimento}
                    returnKeyType = {"next"}
                    placeholder="Nascimento"
                    underlineColorAndroid="transparent"
                    blurOnSubmit={false}
                  />
                </View>  
                <View style={{ width: '49%' }}>
                  <Text style={General.inputLabel}>Celular</Text>
                  <TextInputMask
                    refInput={(ref) => this.input5 = ref}
                    onSubmitEditing={() => this.input6.focus()}
                    onChangeText={this.handleCelular}
                    value={this.state.celular}
                    type={'cel-phone'}
                    options={{
                        maskType: 'BRL',
                        withDDD: true,
                        dddMask: '(99) '
                    }}
                    style={General.formInput}
                    returnKeyType = {"next"}
                    placeholder="(DDD) Celular"
                    underlineColorAndroid="transparent"
                    blurOnSubmit={false}
                  />
                </View>  
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>CEP</Text>
                    <TextInputMask
                      refInput={(ref) => this.input6 = ref}
                      onSubmitEditing={() => this.input7.focus()}
                      onChangeText={this.handleCep}
                      value={this.state.cep}
                      type={'zip-code'}
                      style={General.formInput}
                      returnKeyType = {"next"}
                      placeholder="CEP"
                      underlineColorAndroid="transparent"
                      blurOnSubmit={false}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>UF</Text>
                    <TextInput
                      ref = {(input) => this.input7 = input}
                      onSubmitEditing={() => this.input8.focus()}
                      onChangeText={this.handleUF}
                      value={this.state.uf}
                      autoCorrect={false}
                      returnKeyType='next'
                      placeholder='UF'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View>  

              <View style={{  }}>
                <Text style={General.inputLabel}>Logradouro</Text>
                <TextInput
                  ref = {(input) => this.input8 = input}
                  onSubmitEditing={() => this.input9.focus()}
                  onChangeText={this.handleLogradouro}
                  value={this.state.logradouro}                  
                  returnKeyType='next'
                  placeholder='Logradouro'
                  style={General.formInput}
                />
              </View>  

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Numero</Text>
                    <TextInput
                      ref = {(input) => this.input89= input}
                      onSubmitEditing={() => this.input10.focus()}
                      onChangeText={this.handleNumero}
                      value={this.state.numero}                  
                      returnKeyType='next'
                      placeholder='Número'
                      style={General.formInput}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Complemento</Text>
                    <TextInput
                      ref = {(input) => this.input10 = input}
                      onSubmitEditing={() => this.input11.focus()}
                      onChangeText={this.handleComplemento}
                      value={this.state.complemento}                           
                      returnKeyType='next'
                      placeholder='Complemento'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View> 

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Bairro</Text>
                    <TextInput
                      ref = {(input) => this.input11 = input}
                      onSubmitEditing={() => this.input12.focus()}
                      onChangeText={this.handleBairro}
                      value={this.state.bairro}                           
                      returnKeyType='next'
                      placeholder='Bairro'
                      style={General.formInput}
                    />
                  </View> 
                </View>
                <View style={{ width: '49%' }}>
                  <View style={{  }}>
                    <Text style={General.inputLabel}>Cidade</Text>
                    <TextInput
                      ref = {(input) => this.input12 = input}
                      onSubmitEditing={() => this.input13.focus()}
                      onChangeText={this.handleCidade}
                      value={this.state.cidade}                           
                      returnKeyType='next'
                      placeholder='Cidade'
                      style={General.formInput}
                    />
                  </View>  
                </View>
              </View> 

              <View style={{ width: '100%' }}>
        
                <Text style={styles.descricao}>O que você deseja lecionar?</Text>
                
                <FlatList
                    data={this.state.data}
                    renderItem={
                        this._renderItem
                    }
                    keyExtractor={item => item.id}
                />
                    
              </View>

              <View style={{ width: '100%', marginTop: 10 }}>
          
                <Text style={styles.descricao}>Dados para Recebimento</Text>
                
                  <View style={{ width: '100%' }}>
                    <Text style={General.inputLabel}>Banco</Text>
                      <TouchableOpacity  style={General.formInput} onPress={() => this.openModalBancos() }>
                        <Text style={{ textAlign: 'left', fontSize: 14, fontWeight: '600', color: '#777', fontFamily: Fonts.Light,}}>{this.state.displayBanco}</Text>
                      </TouchableOpacity >
                  </View>

                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginBottom: 10 }}>
                    <View style={styles.buttonContainer}>
                      <TouchableOpacity
                        style={styles.circle}
                        onPress={() => this.setState({ tipoconta: 1 })} 
                      >
                        { this.state.tipoconta === 1 && (<View style={styles.checkedCircle} />) } 
                      </TouchableOpacity>  
                      <Text style={styles.radioLabel}>Conta Corrente</Text>
                    </View>

                    <View style={styles.buttonContainer}>
                      <TouchableOpacity
                        style={styles.circle}
                        onPress={() => this.setState({ tipoconta: 2 })} 
                      >
                        { this.state.tipoconta === 2 && (<View style={styles.checkedCircle} />) } 
                      </TouchableOpacity>  
                      <Text style={styles.radioLabel}>Poupança</Text>
                    </View>
                  </View>
                  
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                    <View style={{ width: '49%' }}>
                      <View style={{  }}>
                        <Text style={General.inputLabel}>Agência</Text>
                        <TextInput
                          ref = {(input) => this.input12 = input}
                          onSubmitEditing={() => this.input13.focus()}
                          onChangeText={this.handleAgencia}
                          value={this.state.agencia}                  
                          returnKeyType='next'
                          placeholder='Agência'
                          style={General.formInput}
                        />
                      </View> 
                    </View>
                    { this.state.tipoconta === 1 && 
                      <View style={{ width: '49%' }}>
                        <View style={{  }}>
                          <Text style={General.inputLabel}>Conta Corrente</Text>
                          <TextInput
                            ref = {(input) => this.input13 = input}
                            onChangeText={this.handleCC}
                            value={this.state.contacorrente}
                            autoCorrect={false}
                            returnKeyType='next'
                            placeholder='CC'
                            style={General.formInput}
                          />
                        </View>  
                      </View>
                    } 
                    { this.state.tipoconta === 2 && 
                      <View style={{ width: '49%' }}>
                        <View style={{  }}>
                          <Text style={General.inputLabel}>Poupança</Text>
                          <TextInput
                            ref = {(input) => this.input13 = input}
                            onSubmitEditing={() => this.input14.focus()}
                            onChangeText={this.handlePoupanca}
                            value={this.state.poupanca}
                            autoCorrect={false}
                            returnKeyType='next'
                            placeholder='Poupança'
                            style={General.formInput}
                          />
                        </View>  
                      </View>
                    } 
                  </View>  

                  { this.state.tipoconta === 2 && 
                  <View style={{ width: '100%' }}>
                    <Text style={General.inputLabel}>Variação</Text>
                    <TextInput
                      ref = {(input) => this.input14 = input}
                      onChangeText={this.handleVariacao}
                      value={this.state.variacao}                  
                      returnKeyType='next'
                      placeholder='Variação'
                      style={General.formInput}
                    />
                  </View>    
                  } 
                </View> 

              <View style={{ marginTop: 10, width:'96%', marginLeft: 10}}>
                <TouchableOpacity style={General.submit} onPress={() => this.handleSaveFormUser()   } >
                  
                  {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
                  {!this.state.isLoading && <Text style={General.submitText}>{this.state.tooltip2}</Text>}
                </TouchableOpacity>
              </View>
              
              <View style={{ flexDirection: 'row', height: 200 }}></View>
            </View>
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
      </View>
      
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Meusdados_professor)


const styles = StyleSheet.create({
  content: {
    backgroundColor: '#FFF',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: '80%',
    borderRadius: 10
  },
  btn: {
    width: 50, 
    height: 50, 
    position: 'absolute', 
    backgroundColor: '#29A9AB', 
    borderRadius: 50, 
    bottom: 10, 
    right: 10, 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  box: {
    width: '100%', 
    justifyContent: 'center',
    alignItems: 'center',
    height: 83, 
    marginTop: 10
  },
  img: {
    width:'100%', height: '100%', borderRadius: 5
  },
  backdrop: {
    width: '100%', 
    height: '100%', 
    backgroundColor: 'black', 
    opacity: 0.5, 
    position: 'absolute', 
    zIndex: 2, 
    borderRadius: 5, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  categoria: {
    fontFamily: Fonts.Bold,
    fontSize: 22,
    color: 'white',
    position: 'absolute', 
    zIndex: 3, 
  },
  descricao: {
    fontFamily: Fonts.Bold,
    fontSize: 18,
    marginTop: 20,
    marginBottom: 20,
    color: '#555'
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center',
    fontFamily: Fonts.Light
  },
  contentInput: {
    paddingTop: -10,
    paddingBottom: 0,
    marginBottom: 0
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  circle: {
    height: 24,
    width: 24,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  checkedCircle: {
      width: 18,
      height: 18,
      borderRadius: 9,
      backgroundColor: '#999',
  },
  radioLabel: {
    color: '#555',
    fontFamily: Fonts.Light,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
    marginRight: 10
  },
});