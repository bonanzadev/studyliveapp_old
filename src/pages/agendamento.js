import React,{ Component } from 'react';
import { View, Text,Image,StyleSheet,TouchableOpacity,AsyncStorage   } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';
import api from '../services/api';

import Avatar from '../components/avatar';
import Modal from "react-native-modal";
import moment from "moment";
import 'moment/locale/pt-br';
    
class Agendamentos extends Component {

  state = {
    aulas: [],
    visibleModal: false,
    aulaAtual: ''
  };

  verificaStorage = async () => {
    return await AsyncStorage.getItem('@StudyLiveApp:id') || false;
  }

  loadData = async () => {

    const id = await this.verificaStorage();
            
    const response = await api.get(`/muralAluno/${id}`);
    console.log(response)
    this.setState({
      aulas: response.data.aulas,
    })

    this.renderItems()
  }

  componentWillUnmount() {
    this.willFocus.remove()
  }


  componentWillMount(){
    this.willFocus = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.loadData() 
      }
    ) 

    this.loadData()
  }
  
  renderItems = () => {
    const {navigate} =this.props.navigation;
    
    if(this.state.aulas.length > 0){
      return this.state.aulas.map((item) => {

        moment.locale('pt-BR')
        
        var dt_ini = moment(item.dt_ini_data).calendar().replace("às","de");  
        
        return (
          <TouchableOpacity onPress={() => this.setState({ visibleModal: 'aulas', aulaAtual: item })  } >
            
            <View style={styles.box}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <Avatar 
                            rounded
                            size="medium"
                            source={require("../images/who.png")}
                            onPress={() => console.log("Works!")}
                            activeOpacity={0.7}
                        />
                    </View>
                    <View style={{ marginTop: 0, marginLeft: 10}}>
                        <Text style={styles.txt1}>
                        <Text style={styles.marcar}>{item.apelido || item.nome || 'Aguardando Professor'}</Text></Text>
                        <Text style={styles.txt1}>{item.categoria}</Text>
                        <Text style={styles.txt1}>{dt_ini} às {item.dt_fim_hora} </Text>
                    </View>    
                </View>    
                <View style={{ width: '100%', borderColor: '#f5f5f5', borderWidth: 1, borderRadius: 5, minHeight: 60, padding: 5, marginTop: 10}}>
                    <Text style={[styles.txt3,{fontStyle: 'italic', fontSize: 12}]}>
                    {item.mensagem||'Nenhum mensagem deixada'}
                    </Text>
                </View> 
                <View style={{ marginTop: 10, marginLeft: 0,borderTopWidth: 0 , borderTopColor: '#f5f5f5',paddingTop: 5,}}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style={{ flexDirection: 'row', marginTop: 0}}>
                    {item.assuntos != undefined && item.assuntos.map((assunto) => {
                        return ( 
                            <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
                                <Text style={styles.txt4}>{assunto.nome}</Text>
                            </View>
                        )
                    })}
                    </ScrollView>
                </View>   
                
                <View style={{ width:'96%', marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' , borderTopColor: '#f5f5f5', borderTopWidth: 1, paddingTop: 10}}>
                        
                    <TouchableOpacity style={styles.btn} onPress={() => null  } >
                        <Text style={styles.btnTxt}>AJUDA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => this.cancelar()  } >
                        <Text style={[styles.btnTxt,{color: 'red'}]}>CANCELAR</Text>
                    </TouchableOpacity>
                    
                    
                </View>
            </View>
            
          </TouchableOpacity>
        )
      })
    } else {
      return (
        <View style={{ 
          marginLeft: 5, 
          marginRight: 0, 
          marginTop: 20,
          }}>
            
          <TouchableOpacity style={{justifyContent: 'center',alignItems: 'center',}} onPress={() => navigate('Historico')  } >

            <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 13, color: '#777' }}>Nenhuma aula ainda...</Text>      
                  
          </TouchableOpacity>
        </View>
      )
    }
  }

  renderAssuntos = () => {
    if(this.state.aulaAtual.assuntos != undefined){
      return this.state.aulaAtual.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  
  render() {
    const {navigate} =this.props.navigation;
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f5f5'}}>
        
        <ScrollView style={{ flex: 1, padding: 15 }}>
            
            { this.renderItems() }
            
        </ScrollView>   
        
        
      </View>
    )
  }
}

export default Agendamentos;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center'
  },
  labelData: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 15,
  },
  content: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  content2: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  box: {
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    width: '94%', 
    minHeight: 100, 
    
    backgroundColor: '#FFF',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    borderRadius: 10,
  },
  marcar: {
    fontSize: 14,
    fontFamily: 'Montserrat-Regular',
  },
  txt1:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 2
  },
  txt2:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
  },
  btn: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 0,
    paddingBottom: 7,
    
    borderRadius: 30
  },
  txt3:{
    fontSize: 16,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 5
  },
  txt4:{
    fontSize: 14,
    color: '#FFF',
    fontFamily: 'Montserrat-Light'
  },
  btnTxt: {
     fontSize: 14, 
     color: '#29A9AB' ,
     fontFamily: 'Montserrat-Light',
  }
});