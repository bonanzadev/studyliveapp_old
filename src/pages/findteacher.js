import React,{ Component } from 'react';
import { 
  View, 
  Text,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';
import api from '../services/api';
import AsyncStorage from '@react-native-community/async-storage';

import Avatar from '../components/avatar';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as todoActions from '../actions/todos';
import FotoAvatar from '../components/FotoAvatar';

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';


var {height, width} = Dimensions.get('window');
    
class Novaaula extends Component {

  constructor(props) {
    super(props);
    this.tooltip1 = React.createRef();
  }

  state = {
    visibleModal: false,
    visible: true,
    isLoading: true,
    isError: false,
    mensagem: '',
    qtdAulas: "",
    totalMinutos: "",
    id_usuario_aluno: '',
    idCategoria: '',
    displayCategoria: '',
    idAssunto: '',
    id_aula: '',
    assuntos: [],
    avatar: '',
    professor: '',
    channel: '',
    origem: 'aluno',
    tipousuario: 1,
    tooltip2: 'IR PARA AULA',
  };

  componentDidMount(){
  
    var dados = this.props.navigation.getParam('dados','');

    this.setState({
      qtdAulas: dados.qtdAulas,
      totalMinutos: dados.totalMinutos,
      displayCategoria: dados.displayCategoria,
      assuntos: dados.assuntos,
      id_aula: dados.id_aula,
      id_usuario_aluno: dados.id_usuario_aluno,
      mensagem: dados.mensagem,
    },() => {
      this.callFindteacher();
    })
  }

  callFindteacher = async() => {
    try { 
        
      console.log("inicia busca ",this.state)
      const response = await api.post(`/findTeacher/${this.state.id_aula}`,this.state);
      console.log("resp aula: ",response)
      if( response.data.errors === "" ){
        var dados = response.data.data;
        let av = FotoAvatar(dados.id_usuario_professor,dados.avatarProfessor);
        let pr = dados.nomeProfessor;
        let ch = dados.channel;
        this.setState({ 
          avatar: av, 
          professor: pr, 
          channel: ch
        },() => {
          this.aulaLiberada()
        });

      } else {
        this.setState({ isLoading: false, isError: true, tooltip2: response.data.errors});
      }

    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, isError: true, tooltip2: "Dificuldades Técnicas"});
    }
  }
  
  descontaCreditoAluno = async() => {
    try { 
      
      var id = await AsyncStorage.getItem('@StudyLiveApp:id') || false;
      var tipousuario = await AsyncStorage.getItem('@StudyLiveApp:tipousuario') || false;
      const response = await api.post(`/descontaCreditoAluno/${this.state.id_aula}`,this.state);
      
      if( response.data.errors == undefined || response.data.errors == "" ){
        
        this.setState({ isLoading: false, tooltip2: "REDIRECIONANDO"},() => {
          //console.log('chann ',this.state.id_aula)
          this.props.navigation.navigate("Espera", {
              id_aula: this.state.id_aula,
              uid: id,
              tipousuario: tipousuario,
              channelName: this.state.channel
          })

        });

      } else {
        this.setState({ isLoading: false, tooltip2: response.data.errors});
      }

    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, tooltip2: "Houve um erro"});
    }
  }

  aulaLiberada = () => {
    this.setState({ isLoading: false,  })
  }

  mainIcon = () => {
    return (!this.state.isLoading)?
    (
      <Icon  
        name='check'
        size={68}
        color='#34A59C'
      />
    ) : (
      <Image 
        source={require('../images/searching2.gif')} 
        style={{width: "96%", height: "96%", borderWidth: 2, borderColor: 'white', borderRadius: 100, shadowOpacity: 0.8,
        shadowRadius: 20,
        elevation: 1,
        shadowOffset: {
            height: 2,
            width: 2
        } }} 
      />
    )
  }

  renderAssuntos = () => {
    if(this.state.assuntos != undefined){
      return this.state.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#ECECFB', borderRadius: 20, padding: 2, paddingLeft: 10, height: 30, paddingRight: 10, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  render() {
    const { avatar } = this.props.todos;

    if( avatar !== "" ){
      var av = FotoAvatar( this.state.id_usuario_aluno, avatar);
    } else {

    }

    if( !this.state.isError ){
      return (
        <View style={{ flex: 1, backgroundColor: '#ECECFB'}}>
        
          <View style={{ flex: 4, marginLeft: 15, marginRight: 15, justifyContent: 'center', alignItems: 'center'}}>
            {!this.state.isLoading &&
              <View style={{  justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{ fontSize: 32,fontFamily: 'Montserrat-Bold', color: '#2FA29A' }}>Pronto!</Text>
              </View>
            }
            {this.state.isLoading &&
              <View style={{  justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{ fontSize: 32,fontFamily: 'Montserrat-Bold', color: '#777' }}>Aguarde...</Text>
                <Text style={{ fontSize: 14, marginTop: 10,fontFamily: 'Montserrat-Regular', color: '#777' }}>Estamos procurando seu Professor</Text>
              </View>  
            }
          </View>

          <View style={{ position: 'absolute', zIndex: 2, width: '100%', top:'50%', marginTop: -145, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            {this.state.isLoading &&
            <PulseIndicator size={280} color='#FFF' />
            }
          </View>
          <View style={{ position: 'absolute', zIndex: 3, width: '100%', top:'50%', marginTop: -55, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ marginRight: '5%' }}>
              <View style={styles.avatar}>
                { avatar !== '' &&
                  <Avatar 
                    rounded
                    size="large"
                    source={{uri: avatar}}
                    onPress={() => console.log("Works!")}
                    activeOpacity={0.7}
                  />  
                }

                { avatar === '' &&
                <Avatar 
                    rounded
                    size="large"
                    source={require('../images/avatar.png')}
                    onPress={() => console.log("Works!")}
                    activeOpacity={0.7}
                  />  
                }
              </View>
            </View>
            <View style={styles.looking}>
              
              {this.mainIcon()}
              
            </View>

            <View style={{ marginLeft: '5%' }}>
              <View style={styles.avatar}>
              
              {this.state.isLoading && this.state.avatar == '' &&
                <Avatar 
                  rounded
                  size="large"
                  source={require("../images/who.png")}
                  onPress={() => console.log("Works!")}
                  activeOpacity={0.7}
                />  
              }
              { !this.state.isLoading && this.state.avatar != '' &&
                <Avatar 
                  rounded
                  size="large"
                  source={{uri: this.state.avatar}}
                  onPress={() => console.log("Works!")}
                  activeOpacity={0.7}
                />  
              }
              </View>
            </View>
          </View>

          <View style={{ flex: 3, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'}}>
              <View style={{ marginTop: 25, marginLeft: 15, marginRight: 15, padding: 10, alignItems: 'center'}}>
                  {this.state.isLoading &&
                  <View style={{ width: '100%', height: 40 }}>
                    <DotIndicator size={6} color='#ECECFB' />
                  </View>  
                  }
                  {!this.state.isLoading &&
                  <View style={{ width: '100%', height: 40 }}>
                    <Text style={{ fontWeight: '300', fontSize: 16, marginTop: 0, color: '#34A59C',fontFamily: 'Montserrat-Light',}}>{this.state.professor}</Text>
                  </View>  
                  }

                  <Text style={{ fontWeight: '300', fontSize: 16, marginTop: 0,fontFamily: 'Montserrat-Light'}}>{this.state.displayCategoria}</Text>
                  <Text style={{ fontWeight: '300', fontSize: 16, marginTop: 15,fontFamily: 'Montserrat-Light'}}>{this.state.totalMinutos} minutos</Text>
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style={{ flexDirection: 'row', marginTop: 15, height: 40}}>
                    {this.renderAssuntos()}
                  </ScrollView> 
                  
                  
              </View>
          </View>
          <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'}}>
            {!this.state.isLoading &&
              <TouchableOpacity style={styles.submit} onPress={() => this.descontaCreditoAluno()}>
                {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
                {!this.state.isLoading && <Text style={styles.submitText}>{this.state.tooltip2}</Text>}
              </TouchableOpacity>
            }
            
          </View>
          
        </View>
      ) 
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: '#ECECFB'}}>
        
          <View style={{ flex: 4, marginLeft: 15, marginRight: 15, justifyContent: 'center', alignItems: 'center'}}>
            
            <View style={{  justifyContent: 'center', alignItems: 'center', padding: 15}}>
              <Text style={{ fontSize: 32,fontFamily: 'Montserrat-Bold', color: '#777' }}>Ops!</Text>
              <Text style={{ fontSize: 16, marginTop: 10,fontFamily: 'Montserrat-Regular', color: '#777' }}>{this.state.tooltip2}...</Text>
              <Text style={{ fontSize: 16, marginTop: 10,fontFamily: 'Montserrat-Regular', color: '#777' }}>O que você deseja fazer?</Text>
            </View>  

            <View style={{ width: '100%', paddingLeft: 15, paddingRight: 15 }}>
              <TouchableOpacity onPress={() => this.setState({ isError: false,isLoading: true }, () => this.callFindteacher() ) } style={styles.submit}>
                <Text style={styles.submitText}>Tentar Novamente</Text>
              </TouchableOpacity>  

              <TouchableOpacity onPress={() => this.setState({ isError: false, }, () => this.props.navigation.navigate("Agendaraula") ) } style={styles.submit}>
              <Text style={styles.submitText}>Agendar</Text>
              </TouchableOpacity>  
            </View>

          </View>
          
        </View>
      )
    }
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Novaaula)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center'
  },
  avatar: {
    borderWidth: 2, 
    borderColor: 'white',
    borderRadius: 50,
  },
  txt4:{
    fontSize: 14,
    color: '#777',
    fontFamily: 'Montserrat-Light'
  },
  looking: {
    
    width: 100, 
    height: 100, 
    
    alignItems: 'center', 
    justifyContent: 'center',
  },
  content: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  content2: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  submitText:{
    color:'#fff',
    fontSize: 16,
    fontFamily: 'Montserrat-Light'
  },
  submit: {
    justifyContent: 'center', 
    alignItems: 'center',
    backgroundColor:'#34A59C',
    borderRadius:46,
    height: 46, 
    borderWidth: 1,
    width: '100%',
    borderColor: '#00B2B2',
    shadowColor: "#000",
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 0.8,
    shadowRadius: 5,
    marginTop: 20,
    elevation: 1,
    shadowOffset: {
        height: 1,
        width: 1
    } 
    
  }
});