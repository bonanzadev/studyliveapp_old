import React,{ Component } from 'react';
import { 
    View, 
    Text,
    Image,
    Platform,
    PermissionsAndroid,
    StyleSheet,
    TouchableOpacity, 
    Dimensions,
    ImageBackground,
    SafeAreaView,
    BackHandler
} from 'react-native';

var {width, height} = Dimensions.get('window');

import Icon from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native-gesture-handler';
import firebase from 'react-native-firebase';
import HistoricoAulas from '../components/HistoricoAulas';
import Agendamentos from '../components/Agendamentos';
import Avatar from '../components/avatar';
import getToken from '../components/TokenFcm';
import Notificacao from '../components/Notificacao';
import Modal from "react-native-modal";
import IncomingAgendamento from '../components/IncomingAgendamento';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as todoActions from '../actions/todos';


async function requestCameraAndAudioPermission() {
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO]);
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the camera');
    } else {
      console.log('Camera permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
}  
    
class Home extends Component  {

  constructor(props){
    super(props);

    this.state = {
      visibleModal: false,
      id: '',
      tooltip: '',
      apelido: '',
      nome: '',
      email: '',
      cidade: '',
      uf: '',
      avatar:'',
      bgimage: '',
      agendamentos: [],
      receivedProps: false
    };

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    });
  }

  componentDidMount() {
    getToken();
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const aula = JSON.parse(notification.data.dados);
      if(aula.origem === "agendamento")
        IncomingAgendamento(aula, this.props.navigation);
      
    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notification) => {
      const aula = JSON.parse(notification.notification.data.dados);
      if(aula.origem === "agendamento")
        IncomingAgendamento(aula, this.props.navigation);
    });
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
    this.willFocus.remove();
    this.backHandler.remove();
  }

  async componentWillMount () {
    if (Platform.OS === 'android') {
      requestCameraAndAudioPermission().then(_ => {
        
      });
    }
    this.willFocus = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.loadDataRedux();   
      }
    ) 
    this.loadDataRedux();  
  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip, visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  newAulaSuccessful = () => {
    this.setState({ visibleModal: 'newAulaSuccessful' });
  }

  loadDataRedux = () => {
    
    var origem = this.props.navigation.getParam('origem','') ;

    if( origem == 'Agendaraula'){
      this.props.navigation.setParams({'origem':''}) ;
      this.newAulaSuccessful();      
    }

    const { todos  } = this.props;
    
    if(todos.bgimage == undefined ){
      var bgimage = "";
    } else {
      var bgimage = todos.bgimage;
    }
    if(todos.saldo == undefined ){
      var saldo = 0;
    } else {
      var saldo = todos.saldo;
    }
    
    this.setState({
      id: todos.id,
      apelido: todos.apelido,
      nome: todos.nome,
      email: todos.email,
      uf: todos.uf,
      cidade: todos.cidade,
      bgimage: bgimage,
      avatar: todos.avatar,
      saldo: saldo,
      id_tipousuario: todos.id_tipousuario,
    })
  }

  renderModalContent = () => (
    <View style={styles.content}></View>
  );

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'center'}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );

  renderModalNewAulaContent = () => (
    <View style={styles.newAulaSuccessful} onPress={ this.closeModal }>
      
      <View style={{ width: 100, height: 200, justifyContent: 'center', alignItems: 'center'  }}>
        <Image source={require('../images/ok.gif')} style={{width: '100%', height: '100%'}} />
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center'  }}>
        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 22, color: '#2FA29A' }}>Agendamento feito!</Text>
        <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 16, marginTop: 15, color: '#777' }}>
          Logo um professor será especialmente selecionado para você
        </Text>
      </View>

      <TouchableOpacity style={styles.textBox1} onPress={() => this.closeModal()  } >
            <Text style={{ fontSize: 18, color: '#29A9AB' ,fontFamily: 'Montserrat-Regular' }}>FECHAR</Text>
        </TouchableOpacity>
      
    </View>
  );

  closeModal = () => {
    this.setState({ visibleModal: null })
  }

  render() {
    
    const { navigate } =this.props.navigation;
    
    return (
      
      <SafeAreaView style={{ flex: 1}}>
        <Modal
          isVisible={this.state.visibleModal === 'tooltip'}
          
          useNativeDriver={true}
          animationIn="fadeIn"
          animationOut="fadeOut"
          onBackdropPress={() => this.closeModal() }
        >
          {this.renderModalTooltipContent()}
        </Modal>
        <Modal
          isVisible={this.state.visibleModal === 'newAulaSuccessful'}
          deviceWidth={width}
          backdropColor={'#EFF1EF'}
          backdropOpacity={1}
          useNativeDriver={true}
          animationIn="slideInLeft"
          animationOut="slideOutRight"
        >
          {this.renderModalNewAulaContent()}
        </Modal>
        
        <View style={styles.box1}>
          <TouchableOpacity activeOpacity={0.9} onPress={() => navigate('Meusdados') }>
            { this.state.bgimage === 'default' && 
                <Image source={require('../images/studentlearning_default.jpg')} resizeMode={'contain'} style={{ width:'100%', height: '100%'}}/>
            }
            { this.state.bgimage !== 'default' && 
              <Image source={{ uri: this.state.bgimage }} resizeMode="cover" style={{ width:'100%', height: '100%'}}/>
            }
          </TouchableOpacity>  
        </View>
        <View style={styles.shadow1}>
            <ImageBackground source={require('../images/shadow1.png')} style={{width: '100%', height: '100%'}} />
        </View>
        <View style={styles.shadow2}>
            <ImageBackground source={require('../images/shadow2.png')} style={{width: '100%', height: '100%'}} />
        </View>
        <View style={styles.menu}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()  }  style={{ width:'100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
            <Icon style={{  }}
              name="md-menu"
              size={26}
              color={'#104B59'}  
            />
          </TouchableOpacity>  
        </View>
        <View style={styles.alert}>
          <Notificacao navigation={this.props.navigation} id={this.state.id}/>
        </View>
        <View style={styles.box2}>
          <ImageBackground source={require('../images/bg1.png')} style={{width: '100%', height: '100%'}}>

            <View style={{ marginLeft: 0, marginRight: 0, marginTop: '4%', justifyContent: 'center', alignItems: 'center'  }}>
              
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', paddingLeft: 15, paddingRight: 15 }}>

                <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
                  <View style={{ borderWidth: 22, borderColor: 'white', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                    { this.state.avatar !== "default" &&
                      <Avatar 
                        rounded
                        size="large"
                        source={{uri: this.state.avatar }}
                        activeOpacity={1}
                      />
                    }
                    { this.state.avatar === "default" &&
                      <Avatar 
                        rounded
                        size="large"
                        source={require('../images/avatar.png')}
                        activeOpacity={1}
                      />
                    }
                  </View>
                  <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: '20%' }}>
                    <Text style={{fontSize: 11,textAlign: 'center', color: "white",fontFamily: 'Montserrat-Regular',}}>Crédito Atual</Text>
                    <Text style={{fontSize: 18,textAlign: 'center', color: 'white',fontFamily: 'Montserrat-Regular',}}>{this.state.saldo} Aulas</Text>
                  </View>
                </View> 

                <View style={{ marginTop: 5, width: '70%', paddingLeft: 5  }}>
                  <Text style={styles.name1} numberOfLines={1} >{this.state.apelido || this.state.nome}</Text>
                  <Text style={styles.endereco} numberOfLines={1} >{this.state.email}</Text>
                  <View style={styles.line}></View>
                  <View style={{  alignItems: 'center',marginTop: '7%' }}>
                    <TouchableOpacity style={styles.btnNovaaula} onPress={() => navigate('Novaaula') }>
                      <Icon style={{ marginRight: 10 }}
                        name="ios-rocket"
                        size={20}
                        color={'#fff'}  
                    />
                      <Text style={{ color: 'white', fontFamily: 'Montserrat-Bold'}}>NOVA AULA</Text>
                    </TouchableOpacity>
                  </View>
                </View>  
              </View>
              

            </View>
          </ImageBackground>
        </View>
        <View style={styles.box3}>
          <ImageBackground source={require('../images/bg5.png')} style={{width: '100%', height: '100%'}}>
            <Agendamentos navigation={this.props.navigation} id={this.state.id} />
          </ImageBackground>
        </View>

        <View style={styles.box4}>
          
            <HistoricoAulas navigation={this.props.navigation} id={this.state.id} />
          
        </View>
        
      </SafeAreaView>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(todoActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Home)


const styles = StyleSheet.create({
  btnNovaaula: {
    paddingLeft: 40, paddingRight: 40,
    backgroundColor:'#2FA29A', 
    borderWidth: 1, 
    borderColor: '#00B2B2',
    borderRadius: 36,
    flexDirection: 'row',
    height: 38,
    justifyContent: 'center',
    alignItems: 'center',
    
    shadowColor: "#000",
    shadowOpacity: 0.3,
    shadowRadius: 20,
    elevation: 2,
    shadowOffset: {
        height: 2,
        width: 2
    } 
  },
  marcar: {
    fontWeight: 'bold'
  },
  txt1:{
    fontSize: 13,
    color: '#999',
    textAlign: 'left',
    fontWeight: '600'
  },
  txt2:{
    fontSize: 13,
    color: '#76C3D7',
    textAlign: 'left',
    fontWeight: '600'
  },
  menu: {
    position: 'absolute', 
    zIndex: 10, 
    width: 40, 
    height: 40,
    borderRadius: 40,
    backgroundColor: '#e9e9e9',
    ...Platform.select({
      ios: {
        marginTop: 25, 
      },
      android: {
        marginTop: 10, 
      },
    }),
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOpacity: 0.8,
    shadowRadius: 20,
    borderColor: '#FFF',
    borderWidth: 0.5,
    elevation: 10,
    shadowOffset: {
        height: 20,
        width: 20
    } 
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 5,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  notifications: {
    backgroundColor: 'white',
    padding: 5,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 5,
  },
  newAulaSuccessful: {
    backgroundColor: '#EFF1EF',
    marginLeft: -5,
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    
  },
  alert: {
    position: 'absolute', 
    zIndex: 10, 
    width: 40, 
    height: 40,
    borderRadius: 40,
    right: 15,
    
    backgroundColor: '#e9e9e9',
    ...Platform.select({
      ios: {
        marginTop: 25, 
      },
      android: {
        marginTop: 10, 
      },
    }),
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOpacity: 0.8,
    shadowRadius: 20,
    borderWidth: 0.5,
    borderColor: '#FFF',
    elevation: 5,
    shadowOffset: {
        height: 20,
        width: 20
    } 
  },
  box1: {
    position: 'absolute', 
    zIndex: 1, 
    width: '100%', 
    height: (height*0.32),
    backgroundColor: '#104B59'
  },
  box2: {
    position: 'absolute', 
    zIndex: 3,  
    width: '100%', 
    height: ((height*0.23)+55), 
    marginTop: ((height*0.32)-25), 
    overflow: 'hidden', 
    borderTopRightRadius: 30, 
  },
  box3: {
    position: 'absolute', 
    zIndex: 4, 
    backgroundColor: 'white', 
    width: '100%', 
    height: ((height*0.51)+0), 
    marginTop: ((height*0.59)-50),
    overflow: 'hidden', 
    borderTopRightRadius: 50, 
  },
  box4: {
    position: 'absolute', 
    borderWidth: 1,
    borderColor: '#FFF',
    zIndex: 5, 
    backgroundColor: '#f9f9f9',
    width: '100%', 
    flexDirection: 'row',
    
    height: ((height*0.30)+35), 
    marginTop: ((height*0.87)-55),
    overflow: 'hidden', 
    borderTopRightRadius: 50, 
  },
  shadow1: {
      width: '100%',
      height: 80,
      position: 'absolute',
      zIndex: 2,
      top: 0
  },
  shadow2: {
      width: '100%',
      height: 93,
      position: 'absolute',
      zIndex: 3,
      marginTop: ((height*0.5)-70), 
  },
  name1: {
    fontSize: 22,
    fontFamily: 'Montserrat-Bold',
    marginLeft: -10,
    color: 'white',
  },
  endereco: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: 'white',
    marginTop: 5,
    marginLeft: -10,
  },
  line: { 
    
    width: '100%', 
    marginLeft: -10,
    height: 1, 
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    marginTop: 10 
  }
});