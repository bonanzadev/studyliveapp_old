import React,{ Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity,TextInput,ActivityIndicator ,TouchableWithoutFeedback ,Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import General from '../styles/general';
import AsyncStorage from '@react-native-community/async-storage';

import Categorias from '../components/Categorias';
import Assuntos from '../components/Assuntos';
import Modal from "react-native-modal";
import api from '../services/api';

import { connect } from 'react-redux';
import * as todoActions from '../actions/todos';
    
class Novaaula extends Component {

  state = {
    id_usuario_aluno: null,
    visibleModal: false,
    isLoading: false,
    qtdaula: 1,
    saldoFinal: 0,
    totalMinutos: 30,
    id_categoria: '',
    displayCategoria: 'Selecione...',
    displayAssuntos: 'Selecione...',
    mensagem: '',
    selectedBoxes: [],
  };

  componentDidMount(){
    this.verificaStorage();

    let saldoatual = this.props.todos.saldo;
    saldoFinal = saldoatual - this.state.qtdaula;

    this.setState({ saldoFinal });
  }

  verificaStorage = async () => {
    let id_usuario_aluno = await AsyncStorage.getItem('@StudyLiveApp:id') || null;
    
    this.setState({ id_usuario_aluno });
  }

  validation = () => {
    var resp = "";

    if(this.state.id_categoria==""){ resp += "\n- Escolha uma categoria"; }
    if(this.state.selectedBoxes==""){ resp += "\n- Escolha um assunto"; }
    

    if(resp != ""){
      this.communicate("Preencha os campos: \n"+resp,5000);
      return false;
    } else {
      return true;
    }
  }

  /* CHAMAR ESSA FUNÇÃO SE O ALUNO TIVER CRÉDITO */
  addAula = async () => {

    if( !this.validation() ){
      return false; 
    }

    try { 

      this.setState({ isLoading: true });

      const dados = {
          id_usuario_aluno: this.state.id_usuario_aluno,
          id_categoria: this.state.id_categoria,
          dateBR: '',
          horaBR: '',
          mensagem: this.state.mensagem || '',
          qtdaula: this.state.qtdaula,
          assuntos: this.state.selectedBoxes,
          totalMinutos: this.state.totalMinutos,
          displayCategoria: this.state.displayCategoria,
          id_aula: ''
      }
      
      const response = await api.post('/aula',dados);
      
      if( response.data.data != undefined ){
        this.setState({ isLoading: false });

        dados.id_aula = response.data.data || '';

        if( dados.id_aula != ""){
          this.props.navigation.navigate('Findteacher',{dados: dados});
        } else {
          this.setState({ isLoading: false, tooltip2: "Dificuldades Técnicas.."});
        }

      } else {
        this.setState({ isLoading: false, tooltip2: "Dificuldades Técnicas.."});
      }

    } catch (response) {
      console.log(response)
      this.setState({ isLoading: false, tooltip2: "Houve um erro"});
    }
    
  }

  /* FUNÇÃO PARA CHAMAR APP IN PURCHASE */
  addSaldo = async () => {
    this.addAula()
  }


  /* MODAL CONTENTS */
  renderModalContent = () => (
    <View style={styles.content} onPress={ this.closeModal }>
      
      <Text style={styles.contentTitle}>Selecione a Disciplina</Text>
      
      <Categorias hasTitle={false} myFunc={this.setDisciplina} />
      
    </View>
  );

  renderModalAssuntoContent = () => (
    <View style={styles.content2} onPress={ this.closeModal }>
      
      <Text style={styles.contentTitle}>Selecione o Assunto</Text>
      
      
        <Assuntos id_categoria={this.state.id_categoria} selectedBoxes={this.state.selectedBoxes} myFunc={this.setAssunto} />
      
      
    </View>
  );

  renderModalTooltipContent = () => (
    <View style={styles.tooltip} onPress={ this.closeModal }>
      
      <ScrollView>
      <Text style={{ color: 'white', fontSize: 18, textAlign:'center'}}>{this.state.tooltip}</Text>
      </ScrollView>
      
    </View>
  );
  /* FIM MODAL CONTENTS */

  plusQtdAula = () => {
    
    var atual = Number(this.state.qtdaula);
    var totalMinutos = this.state.totalMinutos;
    var saldoatual = this.props.todos.saldo

    if(atual<5){
      atual = atual + 1;
      totalMinutos = atual * 30;
      saldoFinal = saldoatual - atual;
      this.setState({ qtdaula: atual,totalMinutos,saldoFinal });
    } else {
      this.communicate("Máximo de aulas é 5",1000);
    }
  }

  minusQtdAula = () => {
    
    var atual = Number(this.state.qtdaula);
    var totalMinutos = this.state.totalMinutos;
    var saldoatual = this.props.todos.saldo;

    if(atual>1){
      atual = atual - 1;
      totalMinutos = atual * 30;
      saldoFinal = saldoatual - atual;
      this.setState({ qtdaula: atual,totalMinutos,saldoFinal });
    } else {
      this.communicate("O mínimo é 1 aula",1000);
    }
  }

  communicate = (tooltip,time) => {
    this.setState({ tooltip,visibleModal: 'tooltip' });

    setTimeout(() => {
      this.setState({ visibleModal: '' });
    }, time);
  }

  setDisciplina = (id,nome) => {
    this.setState({
        id_categoria: id,
        displayCategoria: nome
    })

    this.closeModal()
  }

  onClearAssunto = () => {
    this.setState({ selectedBoxes: [] });
  };

  setAssunto = (boxes) => {

    this.onClearAssunto();

    boxes.map((item) => {
      this.setState(state => {
        let selectedBoxes = state.selectedBoxes;
        selectedBoxes.push({
            id: item.id, nome: item.nome
        }) 
        return { selectedBoxes }; 
      })
    })

    this.closeModal();
  }

  handleMensagem = (mensagem) => { this.setState({ mensagem}) }

  openModalCategoria = () => {
    this.setState({ visibleModal: 'categoria'})
  }
  openModalAssunto = () => {
    if( this.state.id_categoria != "" ){
      this.setState({ visibleModal: 'assunto'})
    } else {
      this.communicate("Escolha uma disciplina antes",1000);
    }
  }
  closeModal = () => {
    this.setState({ visibleModal: null, toggleModal: false })
  }

  handleQtde = (qtdaula) => { this.setState({ qtdaula })}

  render() {
    
    return (
      
      <View style={{ flex: 1, backgroundColor: '#FFF'}}>

        <Modal
          isVisible={this.state.visibleModal === 'categoria'}
          useNativeDriver={true}
          animationIn="zoomInDown"
          animationOut="fadeOut"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={300}
          onBackdropPress={() => this.closeModal() }
        >
          {this.renderModalContent()}
        </Modal>

        <Modal
          isVisible={this.state.visibleModal === 'assunto'}
          onBackdropPress={() => this.closeModal() }
          useNativeDriver={true}
          animationIn="zoomInDown"
          animationOut="fadeOut"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={300}
        >
          {this.renderModalAssuntoContent()}
        </Modal>
        <Modal
          isVisible={this.state.visibleModal === 'tooltip'}
          hasBackdrop={false}
          useNativeDriver={true}
          animationIn="fadeIn"
          animationOut="fadeOut"
          onBackdropPress={() => this.closeModal() }
        >
          {this.renderModalTooltipContent()}
        </Modal>
        
        <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss}}>
          <ScrollView>
            <View style={{ width:"100%", paddingLeft: 15, paddingRight: 15, marginTop: 15  }}>
                
              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Disciplina</Text>
                  <TouchableOpacity  style={General.formInput} onPress={() => this.openModalCategoria() }>
                    <Text style={{ textAlign: 'left', fontSize: 14, fontWeight: '600', color: '#777', fontFamily: 'Montserrat-Light', }}>
                      {this.state.displayCategoria}
                    </Text>
                  </TouchableOpacity >
              </View>
              <View style={{ width: '100%', height: 180, }}>
                <Text style={General.inputLabel}>Escolha os Assuntos</Text>
                <TouchableOpacity style={styles.btn} onPress={() => {this.openModalAssunto()}}>
                  <Icon 
                    name='search'
                    size={22}
                    color='#FFF'
                  />
                </TouchableOpacity>
                <ScrollView style={General.formInputScroll}>
                  {this.state.selectedBoxes.map((item) => {
                    return <View style={styles.box}><Text style={{ fontSize: 14, fontWeight: 'bold', color: '#333' }}>{item.nome}</Text></View>
                  })}
                </ScrollView>
                  
              </View>

              <View style={{ width: '100%' }}>
                <Text style={General.inputLabel}>Mensagem ao Professor</Text>
                <TextInput
                  value={this.state.mensagem}
                  autoCorrect={false}
                  onChangeText={this.handleMensagem}
                  returnKeyType='next'
                  multiline={true}
                  textAlignVertical={'top'}
                  placeholder='Digite sua mensagem'
                  style={[General.formInput,{height: 100}]}
                />
              </View>  

              <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                <View style={{ width: '50%' }}>
                  <Text style={General.inputLabel}>Quantas Aulas?</Text>
                  <View style={General.formInputNoPadding}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between'   }}>
                      <TouchableOpacity style={{ width: '30%', justifyContent: 'center', alignItems: 'center'  }} onPress={() => this.minusQtdAula() }>
                        <Icon style={{ alignSelf: 'center', }} 
                          name='minus'
                          size={20}
                          color='#555'
                        />
                      </TouchableOpacity>  
                      <Text style={styles.inputqtdaula}>{this.state.qtdaula}</Text>
                      <TouchableOpacity style={{ width: '30%',justifyContent: 'center', alignItems: 'center'  }} onPress={() => this.plusQtdAula() }>
                        <Icon style={{ alignSelf: 'center',  }} 
                          name='plus'
                          size={20}
                          color='#555'
                        />
                      </TouchableOpacity>  
                    </View>
                  </View> 
                </View>
                <View style={{ width: '50%', paddingRight: 20 }}>
                  <View style={{  }}>
                    <Text style={[General.inputLabel,{ textAlign: 'right' }]}>Tempo Total</Text>
                    <Text style={{ textAlign: 'right', fontSize: 16, color: '#5BCCC2', fontFamily: 'Montserrat-Light', marginTop: 12 }}>
                      {this.state.totalMinutos} minutos
                    </Text>
                  </View>  
                </View>
              </View> 

            </View>
            
            <View style={{ width:"100%", marginTop: 15, justifyContent: 'center', alignItems: 'center'  }}>
              
              <View style={{ width:"100%", flexDirection: 'row', marginTop: 15  }}>
                <View style={{ width: '50%' }}>
                  <Text style={{ fontSize: 16, textAlign: 'right', fontFamily: 'Montserrat-Light', marginLeft: 10, marginTop: 10 }}>Saldo Atual</Text>
                  <Text style={{ fontSize: 16, textAlign: 'right', fontFamily: 'Montserrat-Light', marginLeft: 10, marginTop: 10 }}>Selecionado</Text>
                  {this.props.todos.saldo != 0 &&
                    <Text style={{ fontSize: 16, textAlign: 'right', fontFamily: 'Montserrat-Light', marginLeft: 10, marginTop: 10 }}>Saldo Restante</Text>
                  }
                </View>
                <View style={{ width: '50%' }}>
                  <Text style={{ fontSize: 16, color: '#5BCCC2', textAlign: 'center', fontFamily: 'Montserrat-Light', marginLeft: 10, marginTop: 10 }}>{this.props.todos.saldo} Aulas</Text>
                  <Text style={{ fontSize: 16, color: '#777', textAlign: 'center', fontFamily: 'Montserrat-Light', marginLeft: 10, marginTop: 10 }}>{this.state.qtdaula} Aulas</Text>
                  {this.props.todos.saldo != 0 &&
                    <Text style={{ fontSize: 16, color: '#5BCCC2', textAlign: 'center', fontFamily: 'Montserrat-Light', marginLeft: 10, marginTop: 10 }}>{this.state.saldoFinal} Aulas</Text>
                  }
                </View>
              </View>
              
              {this.props.todos.saldo != 0 &&
                <View style={{ marginTop: 20, width:'90%', marginLeft: 15}}>
                  <TouchableOpacity style={General.submit} onPress={() => this.addAula()}>
                    {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
                    {!this.state.isLoading && <Text style={General.submitText}>CONFIRMAR</Text>}
                  </TouchableOpacity>
                </View>
              }

              {this.props.todos.saldo == 0 &&
                <View style={{ marginTop: 20, width:'90%', marginLeft: 15}}>
                  <TouchableOpacity style={General.submit} onPress={() => this.addSaldo()}>
                    {this.state.isLoading && <ActivityIndicator style={{marginTop: 8}} size="large" color="white"/>}
                    {!this.state.isLoading && <Text style={General.submitText}>ADICIONAR SALDO</Text>}
                  </TouchableOpacity>
                </View>
              }

            </View>
            <View style={{ width: '100%', height: 50 }}></View>
          </ScrollView>   
        </TouchableWithoutFeedback>
        
      </View>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = {
  ...todoActions,
};

export default connect(mapStateToProps,mapDispatchToProps)(Novaaula)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  box: {
    width: '100%', 
    height: 40, 
    backgroundColor: 'white', 
    borderRadius: 5, 
    paddingLeft: 10, 
    paddingTop: 8,
    marginTop: 2,
    borderWidth: 1,
    borderColor: '#e9e9e9',
  },
  btn: {
    position: 'absolute', 
    bottom: 15, 
    right: 2, 
    zIndex: 10, 
    alignItems: 'center', 
    justifyContent: 'center', 
    width: 45, 
    height: 45, 
    borderRadius: 45,
    backgroundColor: '#29A9AB',
  },
  contentTitle: {
    fontSize: 18,
    marginBottom: 12,
    width:'100%',
    textAlign:'center'
  },
  content: {
    backgroundColor: '#FFF',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  content2: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    paddingTop: 25,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 25,
    width: '94%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentInput: {
    paddingTop: -10,
    paddingBottom: 0,
    marginBottom: 0
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  inputqtdaula: {
    width: '34%', 
    textAlign: 'center', 
    fontWeight: '600', 
    fontSize: 18, 
    color: '#333', 
    paddingTop: 9, 
    height: 44, 
    borderWidth: 1, 
    borderColor: '#e9e9e9', 
    backgroundColor: 'white',
    borderTopWidth: 0, 
    borderBottomWidth: 0
  }
  
});