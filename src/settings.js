import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme
}

export const APPID = '26f2bacbc2c048048fede30c0c66310d';

export const title = 'Agora ReactNative Quick Start';