import React, {Component, PureComponent} from 'react'
import {
  StyleSheet, Text, View, TouchableOpacity,BackHandler,
  Dimensions, NativeModules, Image, Animated, PanResponder, AppState
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import {Surface, ActivityIndicator} from 'react-native-paper'
import {RtcEngine, AgoraView} from 'react-native-agora'
import * as Progress from 'react-native-progress';
import {APPID} from '../settings';
import api from '../services/api';

const {Agora} = NativeModules
console.log(Agora)

if (!Agora) {
  throw new Error("Agora load failed in react-native, please check ur compiler environments")
}

const {
  FPS30,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  Adaptative
} = Agora

const {width, height} = Dimensions.get('window')

class OperateButton extends PureComponent {
  render() {
    const {onPress, source, style, imgStyle = {width: 40, height: 40}} = this.props
    return (
      <TouchableOpacity
        style={style}
        onPress={onPress}
        activeOpacity={.7}
      >
        <Image
          style={imgStyle}
          source={source}
        />
      </TouchableOpacity>
    )
  }
}

type Props = {
  channelProfile: Number,
  channelName: String,
  clientRole: Number,
  onCancel: Function,
  uid: Number,
  tipousuario: Number,
  id_aula: Number,
  totalMinutos: Number,
}

class AgoraRTCView extends Component<Props> {
  
  constructor(props){
    super(props);

    this.state = {
      peerIds: [],
      joinSucceed: false,
      isMute: false,
      hideButton: false,
      visible: false,
      selectedUid: undefined,
      animating: true,
      backdropOpacity: 0,
      seconds: 0,
      pan: new Animated.ValueXY(),
      started: false,
      startedPresenca: false
    }

    this.timer = null;
    this.timer2 = null;

    this.startTimerAula = this.startTimerAula.bind(this);
    this.startTimerPresenca = this.startTimerPresenca.bind(this);
    this.countdown = this.countdown.bind(this);
    this.usuarioPresente = this.usuarioPresente.bind(this);

    /* Handle Android's back button */
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    });
  }

  componentWillMount () {

    this.clockCall2 = "";
    this.clockPresenca = "";
    
    this.setState({ seconds: (this.props.tempoAtual*60) });

    this._panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([
        null,{ dx: this.state.pan.x, dy: this.state.pan.y }
      ]),
      onPanResponderEnd: (e, gestureState) => {
        //console.log( this.state.pan  )
          
          
        Animated.spring(this.state.pan, {
          toValue: {
            x: 0, y: 0
          }
        }).start();
          
         
      }
    })

    const config = {
      appid: APPID,
      channelProfile: this.props.channelProfile,
      clientRole: this.props.clientRole,
      videoEncoderConfig: {
        width: width,
        height: height,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: Adaptative,
      },
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault
    }
    console.log("[CONFIG]", JSON.stringify(config));
    console.log("[CONFIG.encoderConfig", config.videoEncoderConfig);
    RtcEngine.on('videoSizeChanged', (data) => {
      console.log("[RtcEngine] videoSizeChanged ", data)
    })
    RtcEngine.on('remoteVideoStateChanged', (data) => {
      console.log('[RtcEngine] onremoteVideoStateChanged', data);
    })
    RtcEngine.on('userJoined', (data) => {
      console.log('[RtcEngine] onUserJoined', data);
      const {peerIds} = this.state;
      if (peerIds.indexOf(data.uid) === -1) {
        
        this.setState({
          peerIds: [...peerIds, data.uid],
        },() => {
          clearInterval(this.timer);
          this.startTimerAula();
        })
      }
    })
    RtcEngine.on('userOffline', (data) => {
      console.log('[RtcEngine] onUserOffline', data);
      this.setState({
          peerIds: this.state.peerIds.filter(uid => uid !== data.uid)
      })
      console.log('peerIds', this.state.peerIds, 'data.uid ', data.uid)
    })
    RtcEngine.on('joinChannelSuccess', (data) => {
      console.log('[RtcEngine] onJoinChannelSuccess', data);
      
      RtcEngine.startPreview().then(_ => {
        this.setState({
          joinSucceed: true,
          animating: false
        },() => {
          //console.log('ids', this.state);
          
        })
      })
    })
    RtcEngine.on('audioVolumeIndication', (data) => {
      console.log('[RtcEngine] onAudioVolumeIndication', data);
    })
    RtcEngine.on('clientRoleChanged', (data) => {
      console.log("[RtcEngine] onClientRoleChanged", data);
    })
    RtcEngine.on('videoSizeChanged', (data) => {
      console.log("[RtcEngine] videoSizeChanged", data);
    })
    RtcEngine.on('error', (data) => {
      console.log('[RtcEngine] onError', data);
      if (data.error === 17) {
        RtcEngine.leaveChannel().then(_ => {
          this.setState({
            joinSucceed: false
          })
          const { state, goBack } = this.props.navigation;
          this.props.onCancel(data);
          goBack();
        })
      }
    })
    RtcEngine.init(config);

    /* Tipo presença: Início */
    this.setPresenca(1);
    this.startTimerPresenca();
  }

  setPresenca = async (id_tipopresenca) => {
    try { 
        
      console.log("inicia presenca ",this.props.id_aula)
      const response = await api.post(`/presenca/${this.props.id_aula}`,{
        id_usuario: this.props.uid,
        id_tipopresenca
      });
      console.log("resp presenca: ",response)

    } catch (response) {
      console.log('Erro presença: ',response)
    }
  }

  componentDidMount () {

    AppState.addEventListener('change', state => {
      if (state === 'background' || state === 'inactive') {
        RtcEngine.muteAllRemoteAudioStreams(false);
        console.log('desconectou')
        /* Tipo presença: Desconectou */
        this.setPresenca(5);
      }
    });

    console.log('pars ',this.props)

    RtcEngine.getSdkVersion((version) => {
      console.log('[RtcEngine] getSdkVersion', version);
    })

    console.log('[joinChannel] ' + this.props.channelName);
    RtcEngine.joinChannel(this.props.channelName, this.props.uid)
      .then(result => {
        
        if( this.state.peerIds.length > 1 ){
          this.startTimerAula();
        }
    });
    
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.navigation.isFocused();
  }

  componentWillUnmount () {
    /* Tipo presença: Desconectou */
    this.setPresenca(5);
    this.backHandler.remove();
    this.closeChannel()
  }

  closeChannel = () => {

    if (this.state.joinSucceed) {
      RtcEngine.leaveChannel().then(res => {
        RtcEngine.destroy()
      }).catch(err => {
        RtcEngine.destroy()
        console.log("leave channel failed", err);
      })
    } else {
      RtcEngine.destroy()
    }
  }

  handleCancel = () => {
    const { goBack } = this.props.navigation;
    RtcEngine.leaveChannel().then(_ => {
      this.setState({
        joinSucceed: false
      })
      goBack()
    }).catch(err => {
      console.log("[agora]: err", err)
    })
  }

  switchCamera = () => {
    RtcEngine.switchCamera();
  }

  toggleAllRemoteAudioStreams = () => {
    this.setState({
      isMute: !this.state.isMute
    }, () => {
      RtcEngine.muteAllRemoteAudioStreams(this.state.isMute).then(_ => {
        /**
         * ADD the code snippet after muteAllRemoteAudioStreams success.
         */
      })
    })
  }

  toggleHideButtons = () => {
    this.setState({
      hideButton: !this.state.hideButton
    })
  }

  onPressVideo = (uid) => {
    
    this.setState({
      selectedUid: uid,
      backdrop: true
    }, () => {
      this.setState({
        visible: true
      })
    })
  }

  toolBar = ({hideButton, isMute}) => {
    
  }

  remoteView = ({visible, peerIds}) => {
    if( this.props.tipousuario == 1 ){
      var txt = 'Aguardando Professor';
    } else {
      var txt = 'Aguardando Aluno';
    }

    if( peerIds.length == 0 ){
      return (<View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}><Text>{ txt }</Text></View>)
    } else {
      return (<View>{
        peerIds.map((uid, key) => (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => this.onPressVideo(uid)}
          key={key}>
          <AgoraView
              mode={1}
              key={uid}
              style={styles.remoteView}
              
              remoteUid={uid}
          />
          
        </TouchableOpacity>
        ))
        }</View>)
    }
    
  }

  selectedView = ({visible}) => {
    
    return (
    
      <TouchableOpacity
        activeOpacity={1}
        style={{flex: 1}}
        onPress={() => this.setState({
          visible: false
      })} >
        <AgoraView
          mode={1}
          style={{flex: 1}}
          
          remoteUid={this.state.selectedUid}
        />
      </TouchableOpacity>
    )
  }

  toggleBackdrop = () => {
     
    //alert(this.state.backdropOpacity)
    if( this.state.backdropOpacity === 0 ){
      
      var op = 0.8;
    } else {
      var op = 0;
    }

    
    this.setState({ backdropOpacity: op })
  }

  closeBackdrop = () => {
    this.setState({ backdrop: false })
  }

  countdown() {
  
    this.setState((prevstate) => ({ 
      seconds: prevstate.seconds-1
    }), () => {
      
      if(this.state.seconds === 0 || this.state.seconds < 0) {
        this.stopTimer();
      }
    });
  }

  usuarioPresente(){
    this.setPresenca(2)
  }

  startTimerAula() {
      
    this.timer = setInterval(() => {
      this.countdown()
    }, 1000);
    
  }

  startTimerPresenca() {

    console.log('começou press')
    this.timer2 = setInterval(() => {
      this.usuarioPresente()
    }, 20000);
    
  }

  stopTimer(){
    clearInterval(this.timer);
    clearInterval(this.timer2);

    this.closeChannel();

    /* Tipo presença: 3 - Término */
    this.setPresenca(3);

    this.props.navigation.navigate('Avaliacaoposaula',{dados: this.props})
  }

  barprogress = () => {
    
    var perc = ((this.state.seconds*100/(this.props.totalMinutos*60))/100).toFixed(2)
    var min = Math.round((this.state.seconds/60))

    if( perc > 0.6 ){
      var colorProgress = '#ccffcc';
      var txtColor = "black";
      
    } else if ( perc <= 0.6 && perc >= 0.2) {

      var colorProgress = '#ffedcc';
      

      if( this.state.backdropOpacity === 0 ){
        var txtColor = "white";
      } else {
        var txtColor = "black";
      }

    } else {

      var colorProgress = '#ffcccc';
      
      
      if( this.state.backdropOpacity === 0 ){
        var txtColor = "white";
      } else {
        var txtColor = "black";
      }

    }
    
    return (
      <View style={styles.progressBorder}>
        <View style={{ width: "100%", height: 22,borderRadius: 8, position: 'absolute', justifyContent: 'center', alignItems: 'flex-end',  }}>
          
          <Text style={{ color: txtColor, fontSize: 11, position: 'absolute', top: 2, zIndex: 6, textAlign: 'center', width: '100%'}}>{this.state.seconds} sec</Text>
          
          <Progress.Bar 
            progress={perc} 
            width={200} 
            color={colorProgress} 
            height={20}
            style={{ borderWidth: 2 }}
            useNativeDriver={true}
            borderRadius={20} />
          
        </View>
      </View>
    )
  }

  saladeespera = () => {

  }

  
  render () {
    
    
    return (
      <View style={styles.container}>
          
          <View style={styles.container}>
            {this.toolBar(this.state)}
            <View style={{ width: '100%', height:'100%',position: 'absolute', zIndex: 2, }}>
            {this.remoteView(this.state)}
            </View>
            
            <View style={{ width: 130, height: 130,position: 'absolute', zIndex: 4, bottom: 70, left: '50%', marginLeft: -65}}>
              
                <Animated.View
                  style={[
                    
                    {
                      transform: [
                        { translateX: this.state.pan.x },
                        { translateY: this.state.pan.y },
                      ]
                    }
                  ]}
                  {...this._panResponder.panHandlers}
                >
                <AgoraView style={{ width: 130, height: 130,  }} zOrderMediaOverlay={true} showLocalVideo={true} mode={1} />
                  
                </Animated.View>
              
              
            </View>
            
            {this.barprogress()}

            <TouchableOpacity style={[styles.backdrop,{opacity: this.state.backdropOpacity}]} onPress={() => this.toggleBackdrop() }>
              
                <View style={{ position: 'absolute', top: 0, width: '100%', height: '100%', marginTop: 15, position: 'absolute', justifyContent: 'flex-start', alignItems: 'center' }}>
                  
                  <View style={{ width: 240, flexDirection: 'row', marginLeft: 20   }}>
                  
                  
                  <TouchableOpacity style={styles.btn} onPress={this.toggleAllRemoteAudioStreams}>
                    { this.state.isMute == false &&
                      <Icon 
                        style={{ textAlign:'center' }}
                        name="ios-mic"
                        size={32}
                        color={'#FFF'}  
                      />
                    }
                    { this.state.isMute == true &&
                      <Icon 
                        style={{ textAlign:'center' }}
                        name="ios-mic-off"
                        size={32}
                        color={'#FFF'}  
                      />
                    }
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.btn} onPress={this.switchCamera}>
                    <Icon 
                      style={{ textAlign:'center' }}
                      name="ios-reverse-camera"
                      size={32}
                      color={'#FFF'}  
                    />
                  </TouchableOpacity>
                  
                  </View>
                </View>     
              
            </TouchableOpacity>
            
            
            {this.selectedView(this.state)}
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  absView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'space-between',
  },
  progressBorder: {
    position: 'absolute', 
    left: '50%', 
    bottom: 15, 
    marginLeft: -100, 
    zIndex: 100, 
    width: 200, 
    height: 22, 
    alignItems: 'flex-end', 
    justifyContent: 'flex-end',
    borderRadius: 8
  },
  localView: {
    flex: 1
  },
  remoteView: {
    // width: (width - 40) / 3,
    // height: (width - 40) / 3,
    
    width: '100%',
    height: '100%',
  },
  bottomView: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  backdrop: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: 'black',
    zIndex: 3,
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    flexDirection: 'row',
    padding: 20
  },
  btn: {
    justifyContent: 'center', 
    alignItems: 'center', 
    marginLeft: 15, 
    width: 45, 
    height: 45, 
    borderRadius: 45, 
    
    borderWidth: 1, 
    borderColor: '#888', 
  }
})


export default function AgoraRTCViewContainer(props) {
  const { navigation } = props
  const channelProfile = navigation.getParam('channelProfile', 1)
  const clientRole = navigation.getParam('clientRole', Host)
  const channelName = navigation.getParam('channelName', '');
  var uid = parseInt(navigation.getParam('uid',0))
  const onCancel = navigation.getParam('onCancel')
  var tipousuario = parseInt(navigation.getParam('tipousuario',0))
  var id_aula = parseInt(navigation.getParam('id_aula',0))
  var totalMinutos = parseInt(navigation.getParam('totalMinutos',0))
  var tempoAtual = parseInt(navigation.getParam('tempoAtual',totalMinutos))
  
  return (<AgoraRTCView
    channelProfile={channelProfile}
    channelName={channelName}
    clientRole={clientRole}
    uid={uid}
    tipousuario={tipousuario}
    id_aula={id_aula}
    totalMinutos={totalMinutos}
    tempoAtual={tempoAtual}
    onCancel={onCancel}
    {...props}
  ></AgoraRTCView>)
}