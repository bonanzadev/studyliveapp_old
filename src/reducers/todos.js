const INITIAL_STATE = {
    id: '',
    id_tipousuario: '',
    apelido: '',
    nome: '',
    login: '',
    email: '',
    celular: '',
    saldo: '',
    bgimage: '',
    avatar: '',
}

function todos(state = INITIAL_STATE, action ){
    console.log("action: ",action.state)
    
    
    switch(action.type){
        
        case "EDIT_USERDATAGERAL":
            return { 
                id: action.state.id || state.id,
                id_tipousuario: action.state.id_tipousuario || state.id_tipousuario,
                nome: action.state.nome || state.nome,
                apelido: action.state.apelido || state.apelido,
                login: action.state.login || state.login,
                email: action.state.email || state.email,
                celular: action.state.celular || state.celular,
                saldo: action.state.saldo || state.saldo,
                bgimage: action.state.bgimage || state.bgimage,
                avatar: action.state.avatar || state.avatar,
            } 
        break;
        case "EDIT_SALDO":
            return { 
                ... state, 
                saldo: action.state.saldo || state.saldo,
            } 
        break;
        
        default:
            return state;
    }
    
    
}

export default todos;