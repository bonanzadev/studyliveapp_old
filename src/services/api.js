import { create } from 'apisauce';

const api = create({
    baseURL: 'http://67.205.183.161:5000',
    //yarbaseURL: 'http://192.168.0.102:5000',
});

api.addResponseTransform(response => {
    if( !response.ok ) throw response;
})

export default api;