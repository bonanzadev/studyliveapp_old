const fonts = {
    Light: 'Montserrat-Light',
    Bold: 'Montserrat-Bold',
    Italic: 'Montserrat-Italic',
    Regular: 'Montserrat-Italic',
};

export default fonts;