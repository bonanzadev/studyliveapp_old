
import React from 'react';
import { StyleSheet } from 'react-native';

const general = {
  
  container: {
    borderRadius: 10,  
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#FFF',
    paddingTop: 0,
    paddingLeft: 20,
    paddingRight: 20
  },
  titlePadrao1: {
    fontSize: 14, fontWeight:'100', color: '#2FA29A',fontFamily: 'Montserrat-Light',
  },
  divisao: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: '#f5f5f5',
    height:7
  },
  txt1:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left'
  },
  line: {
    marginTop: 10,
    marginBottom: 0,
    backgroundColor: '#F0F0F0',
    height:1
  },
  lineSeparator: {
    marginTop: 0,
    marginBottom: 0,
    backgroundColor: '#F0F0F0',
    height:1
  },
  logo: {
    height:'30%',
    marginBottom: 40
  },
  box:{
    marginRight: 10,
  },
  
  color1:{
    color: '#00A79D'
  },
  title:{
    fontSize: 16,
    marginTop: 2,
    fontWeight: '600',
    marginLeft: 15
  },
  btn1:{
    backgroundColor:'#FFF',
    borderColor:'#86BFB9',
    borderRadius:50,
    borderWidth: 1,
    width: '70%',
    paddingTop: 2,
    height: 25
  },
  btn1Text:{
    color:'#86BFB9',
    fontSize: 12,
    textAlign:'center',
  },
  btn2:{
    
    borderColor:'#86BFB9',
    borderRadius:50,
    borderWidth: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 2,
    height: 25
  },
  btn2Text:{
    color:'#777',
    fontSize: 12,
    fontWeight: '600',
    textAlign:'center',
  },
  btn2TextSel:{
    color:'#28A79D',
    fontSize: 12,
    fontWeight: '600',
    textAlign:'center',
  },
  btn3:{
    backgroundColor:'#FFF',
    borderColor:'#FF7A4D',
    borderRadius:50,
    borderWidth: 1,
    width: '70%',
    paddingTop: 2,
    height: 25
  },
  btn3Text:{
    color:'#FF7A4D',
    fontSize: 12,
    textAlign:'center',
  },
  searchSection: {
  
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F0F0F0',
    paddingRight: 5,
    borderRadius: 10,
    width: '77%',
    height: 50
  },
  searchSection100: {
  
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F0F0F0',
    paddingRight: 5,
    borderRadius: 10,
    width: '100%',
    height: 50
  },
  filtros: {
    
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    width: '23%',
    color: '#00A79D',
    height: 50
  },
  searchIcon: {
      padding: 10,
  },
  input: {
      flex: 1,
      fontSize: 11,
      backgroundColor: '#F0F0F0',
      color: '#424242',
  },
  inputLabel:{
    fontSize: 16, 
    
    color: '#888',
    paddingLeft: 5, 
    marginBottom: 5,
    marginTop: 5,
    fontFamily: 'Montserrat-Regular',
  },
  formInput: {
    fontSize: 14,
    backgroundColor: '#f9f9f9',
    width: '100%',
    height: 46,
    
    
    paddingRight: 20,
    marginBottom: 10,
    borderRadius: 5,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#f0f0f0',
    color: '#333',
    justifyContent: 'center',
    alignItems: 'flex-start',
    fontFamily: 'Montserrat-Light',
  },
  formInputScroll: {
    fontSize: 14,
    backgroundColor: '#f9f9f9',
    width: '100%',
    height: '80%',
    
    marginRight: 10,
    padding: 5,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#f0f0f0',
    color: '#333',
  },
  formInputNoPadding: {
    
    fontSize: 12,
    marginLeft: 10,
    width: '94%',
    height: 44,
    borderRadius: 5,
    backgroundColor: '#f9f9f9',
    borderWidth: 1,
    borderColor: '#e9e9e9',
    color: '#333',
  },
  formInputLogin: {
    
    fontSize: 12,
    backgroundColor: '#9DD468',
    width: '94%',
    height: 55,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 40,
    paddingLeft: 25,
    borderWidth: 1,
    borderColor: '#9DD468',
    
    
    color: '#FFF',
  },
  formTextArea: {
    flex: 1,
    fontSize: 11,
    backgroundColor: '#f9f9f9',
    width: '90%',
    height: 90,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 5,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#e9e9e9',
    color: '#424242',
  },
  formInputSubmitLogin: {
      
    
    backgroundColor: '#FFF',
    width: '94%',
    height: 55,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 40,
    
    borderWidth: 1,
    borderColor: '#FFF',
    
  },
  tituloNome:{
    fontSize: 16,
    fontWeight: '400',
    marginTop: 5,
    color: '#333'
  },
  txtDados:{
    fontSize: 12,
    fontWeight: '400',
    marginTop: 3,
    color: '#777'
  },
  submit:{
    marginTop:10,
    backgroundColor:'#29A9AB',
    borderRadius:48,
    height: 48, 
    borderWidth: 1,
    borderColor: '#00B2B2',
    shadowColor: "#000",
    width: '96%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitText:{
      color:'#fff',
      fontSize: 14,
      fontFamily: 'Montserrat-Light',
  },
  btnClean:{
    marginRight:0,
    marginLeft:0,
    marginTop:10,
    paddingTop:10,
    paddingBottom:10,
    borderColor:'#86BFB9',
    borderRadius:40,
    borderWidth: 1,
  },
  btnCleanText:{
      color:'#777',
      textAlign:'center',
      fontSize: 16,
      fontWeight: 'bold'
  }
};

export default general;