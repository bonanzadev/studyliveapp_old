import React, { Component } from 'react';
import { View, Text,Image, StyleSheet,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Agora from './page/agora';
import Home from './pages/home';
import Homeprofessor from './pages/homeprofessor';
import Meusdados from './pages/meusdados';
import Meusdados_professor from './pages/meusdados_professor';
import Novaaula from './pages/novaaula';
import Historico from './pages/historico';
import Agendamento from './pages/agendamento';
import Avaliacaoposaula from './pages/avaliacaoposaula';

import Agendaraula from './pages/agendaraula';
import Findteacher from './pages/findteacher';
import Novocredito from './pages/novocredito';
import Configsprofessor from './pages/configsprofessor';
import Espera from './pages/espera';

import Splash from './pages/splash';
import Login from './pages/login';
import SideMenu from './components/sideMenu';
import { Provider } from 'react-redux'
import store from './store'

import {
  createAppContainer,
  
  createBottomTabNavigator,
  
  
} from 'react-navigation';

import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { TouchableHighlight } from 'react-native-gesture-handler';


const TabNavigator = createMaterialTopTabNavigator({
  Novaaula :{
      screen: Novaaula,
      navigationOptions: ({ navigation }) => {
          return {
              title: 'Aula Agora',
              tabBarIcon: ({tintColor}) => 
                  <Icon
                      name="ios-rocket"
                      size={28}
                      color={tintColor}  
                  />
          
          }
      }
  },
  Agendaraula :{
      screen: Agendaraula,
      navigationOptions: ({ navigation }) => {
          return {
              title: 'Agendar',
              
              tabBarIcon: ({tintColor}) => 
                  <Icon
                      name="md-clock"
                      size={28}
                      color={tintColor}  
                  />
          
          }
      }
  
  },
},{
  tabBarPosition: 'top',
  swipeEnabled: true,
  animationEnabled: true,
  shifting: true,
  tabBarOptions: {
      activeTintColor: '#5BCCC2',
      inactiveTintColor: '#999',
      showIcon: true,
      style: {
        backgroundColor: '#FFF',
        paddingBottom: 0,
        paddingTop: 0,
        height: 80
      },
      tabStyle: {
          marginTop: 0,
          marginBottom: 0,
          paddingBottom: 0,
          paddingTop: 15,
      },
      labelStyle: {
        textAlign: 'center',
        fontFamily: 'Montserrat-Light',
        fontSize: 14,
        paddingBottom: 0,
        marginTop: 5
      },
      iconStyle: {
          marginTop: 0
      },
      indicatorStyle: {
        borderBottomColor: '#5BCCC2',
        borderBottomWidth: 2,
        paddingBottom: 0,
        height: 0
      },
    },
});

const TabNavigatorAulas = createMaterialTopTabNavigator({
  Historico :{
      screen: Historico,
      navigationOptions: ({ navigation }) => {
          return {
              title: 'Histórico',
              tabBarIcon: ({tintColor}) => 
                <Icon
                    name="ios-list"
                    size={28}
                    color={tintColor}  
                />
          
          }
      }
  },
  Agendamento :{
      screen: Agendamento,
      navigationOptions: ({ navigation }) => {
          return {
              title: 'Agendamentos',
              
              tabBarIcon: ({tintColor}) => 
                  <Icon
                      name="md-clock"
                      size={28}
                      color={tintColor}  
                  />
          
          }
      }
  
  },
},{
  tabBarPosition: 'top',
  swipeEnabled: true,
  animationEnabled: true,
  shifting: true,
  tabBarOptions: {
      activeTintColor: '#5BCCC2',
      inactiveTintColor: '#999',
      showIcon: true,
      style: {
        backgroundColor: '#FFF',
        paddingBottom: 0,
        paddingTop: 0,
        height: 70
      },
      tabStyle: {
          marginTop: 0,
          marginBottom: 0,
          paddingBottom: 0,
          paddingTop: 15,
      },
      labelStyle: {
        textAlign: 'center',
        fontFamily: 'Montserrat-Light',
        fontSize: 12,
        paddingBottom: 0,
        marginTop: 3
      },
      iconStyle: {
          marginTop: 0
      },
      indicatorStyle: {
        borderBottomColor: '#5BCCC2',
        borderBottomWidth: 2,
        paddingBottom: 0,
        height: 0
      },
    },
});

const DrawerNavigator = createDrawerNavigator({
    
    Home :{
        screen: Home
    }
  }, {
    contentComponent: SideMenu,
    drawerWidth: 300,
    drawerType: 'back', overlayColor: 'rgba(0, 0, 0, 0.7)',
});
const DrawerNavigatorProf = createDrawerNavigator({
    
    Homeprofessor :{
        screen: Homeprofessor
    }
  }, {
    contentComponent: SideMenu,
    drawerWidth: 300,
    drawerType: 'back', overlayColor: 'rgba(0, 0, 0, 0.7)',
});



const AppNavigator = createStackNavigator({
    
  Splash : {
      screen: Splash,
      navigationOptions : {
          header: null,
      },
  },

  Login : {
      screen: Login,
      navigationOptions : {
          header: null,
      },
  },
  
  Home :{
      screen: DrawerNavigator,
      navigationOptions : {
        header: null,
    },
  },
  Homeprofessor :{
      screen: DrawerNavigatorProf,
      navigationOptions : {
        header: null,
    },
  },
  Findteacher :{
      screen: Findteacher,
      navigationOptions : {
        header: null,
    },
  },
  Configsprofessor :{
      screen: Configsprofessor,
      navigationOptions : {
        header: null,
    },
  },
  Espera :{
      screen: Espera,
      navigationOptions : {
        header: null,
    },
  },
  Avaliacaoposaula :{
      screen: Avaliacaoposaula,
      navigationOptions: ({ navigation }) => ({
        title: 'Pesquisa',
        headerStyle: {
            height: 54,
            backgroundColor: '#29A9AB'
        },
        headerTitle:        
            <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Light', color: 'white'}}>Avaliação</Text>,
        headerLeft: 
        <View/>,
        
        headerRight: 
            <View/>,
      }),
  },
  Novaaula :{
      screen: TabNavigator,
      navigationOptions: ({ navigation }) => ({
        title: 'Pesquisa',
        headerStyle: {
            height: 54,
            backgroundColor: '#29A9AB'
        },
        headerTitle:        
            <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Light', color: 'white'}}>Nova Aula</Text>,
        headerLeft: 
          <TouchableOpacity hitSlop={{ top: 50, bottom: 50, left: 50, right: 50 }} onPress={ () => { navigation.goBack(null) }} >
            <Icon style={{ paddingLeft: 15 }}
            name="ios-arrow-back"
            color="white"
            size={32} />
          </TouchableOpacity>,
        
        headerRight: 
            <View/>,
      }),
  },
  Historico :{
      screen: TabNavigatorAulas,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          height: 54,
          backgroundColor: '#29A9AB'
      },
      headerTitle:        
        
          <Text style={{ fontSize: 22, fontFamily: 'Montserrat-Light', color: 'white'}}>Aulas</Text>,
        
      headerLeft: 
        <TouchableOpacity hitSlop={{ top: 50, bottom: 50, left: 50, right: 50 }} onPress={ () => { navigation.goBack(null) }} >
          <Icon style={{ paddingLeft: 15 }}
          name="ios-arrow-back"
          color="white"
          size={32} />
        </TouchableOpacity>,
          
      headerRight: 
          <View/>,
      
      }),
  },
  Novocredito :{
      screen: Novocredito,
      navigationOptions: ({ navigation }) => ({
        
      headerStyle: {
          height: 54,
          backgroundColor: '#29A9AB'
      },
      headerTitle:        
          <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Light', color: 'white'}}>Novo Crédito</Text>,
      headerLeft: 
        <TouchableOpacity hitSlop={{ top: 50, bottom: 50, left: 50, right: 50 }} onPress={ () => { navigation.goBack(null) }} >
          <Icon style={{ paddingLeft: 15 }}
          name="ios-arrow-back"
          color="white"
          size={32} />
        </TouchableOpacity>,
        
      headerRight: 
            <View/>,
      }),
  },
  Meusdados :{
      screen: Meusdados,
      navigationOptions: ({ navigation }) => ({
        title: 'Meusdados',
        headerStyle: {
            height: 54,
            backgroundColor: '#29A9AB'
        },
        headerTitle:        
            <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Light', color: 'white'}}>Meus Dados</Text>,
        headerLeft: 
          <TouchableOpacity hitSlop={{ top: 50, bottom: 50, left: 50, right: 50 }} onPress={ () => { navigation.goBack(null) }} >
            <Icon style={{ paddingLeft: 15 }}
            name="ios-arrow-back"
            color="white"
            size={32} />
          </TouchableOpacity>,
        
        headerRight: 
            <View/>,
      }),
  },
  Meusdados_professor :{
      screen: Meusdados_professor,
      navigationOptions: ({ navigation }) => ({
        title: 'Meusdados',
        headerStyle: {
            height: 54,
            backgroundColor: '#29A9AB'
        },
        headerTitle:        
            <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Light', color: 'white'}}>Meus Dados</Text>,
        headerLeft: 
          <TouchableOpacity hitSlop={{ top: 50, bottom: 50, left: 50, right: 50 }} onPress={ () => { navigation.goBack(null) }} >
            <Icon style={{ paddingLeft: 15 }}
            name="ios-arrow-back"
            color="white"
            size={32} />
            </TouchableOpacity>,
        
        headerRight: 
            <View/>,
      }),
  },
  
  Agora: {
    screen: Agora,
    navigationOptions : {
        header: null,
    },
  },
  
    
},{
    initialRouteName: 'Splash',
    headerMode: 'float',
    mode: 'modal', 
    headerLayoutPreset: 'center',
    transitionConfig: () => ({
      screenInterpolator: sceneProps => {
          const { layout, position, scene } = sceneProps;
          const { index } = scene;
  
          const translateX = position.interpolate({
              inputRange: [index - 1, index, index + 1],
              outputRange: [layout.initWidth, 0, 0]
          });
  
          const opacity = position.interpolate({
              inputRange: [
                  index - 1,
                  index - 0.99,
                  index,
                  index + 0.99,
                  index + 1
              ],
              outputRange: [0, 1, 1, 0.3, 0]
          });
  
          return { opacity, transform: [{ translateX }] };
      }
  })
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const AppContainer = createAppContainer(AppNavigator);

class App extends Component {
  render() {
    return(
      <Provider store={store}>
        <AppContainer />
      </Provider>  
    ) 
  }
}

export default App;

