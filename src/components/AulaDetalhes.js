import React, { Component } from 'react';
import Avatar from './avatar';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';

import api from '../services/api';
import moment from "moment";
import 'moment/locale/pt-br'; 
import propTypes from 'prop-types';
import FotoAvatar from './FotoAvatar';
import Icon from 'react-native-vector-icons/Ionicons';

class AulaDetalhes extends Component {

  cancelar = () => {
    Alert.alert(
      'Confirmação',
      'Deseja cancelar este agendamento?',
      [
        {
          text: 'Não',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Sim', onPress: () => 
          this.cancelarAula()
        }, 
      ],
      {cancelable: false},
    );
  }

  cancelarAula = async () => {
    
    try{
        
      if( this.props.origem == "meusagendamentos" ){
        var response = await api.post(`/cancelarAulaProfessor/${this.props.aula.id}`);
      } else {
        var response = await api.delete(`/aula/${this.props.aula.id}`);
      }
  
      if(response.data.affectedRows > 0){
        this.props.myFunc();
      }
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }

    
  }
  pegarAula = async () => {

    try{
        
      const response = await api.post(`/setAulaProfessor/${this.props.aula.id}`,{id_usuario_professor: this.props.id});
  
      if(response.data.affectedRows > 0){
        this.props.myFuncNewAula();
      }
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }
    
    
  }

  renderAssuntos = () => {
    if(this.props.aula.assuntos != undefined){
      return this.props.aula.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  returnAvatar = () => {
    const { origem, aula } = this.props;

    switch( origem ){
      /* Agendamentos do professor */
      case "meusagendamentos":
        return (
          <Avatar 
            rounded
            size="xlarge"
            source={{uri: FotoAvatar(aula.id_usuario_aluno, aula.avatar)}}
            activeOpacity={0.8}
          />
        )
      break;
      /* Agendamentos do aluno */
      case "agendamentos":
        return aula.id_usuario_professor != 0 ? (
          <Avatar 
            rounded
            size="xlarge"
            source={{uri: FotoAvatar(aula.id_usuario_professor, aula.avatar)}}
            activeOpacity={0.8}
          />
        ) : (
          <Avatar 
            rounded
            size="xlarge"
            source={require("../images/who.png")}
            activeOpacity={0.8}
          />
        )
      break;
      /* Mural do professor */
      case "mural":
        return (
          <Avatar 
            rounded
            size="xlarge"
            source={{uri: FotoAvatar(aula.id_usuario_aluno, aula.avatar)}}
            activeOpacity={0.8}
          />
        )
      break;
      /* Histórico de aulas do aluno */
      case "historico":
        return (
          <Avatar 
            rounded
            size="xlarge"
            source={{uri: FotoAvatar(aula.id_usuario_professor, aula.avatar)}}
            onPress={() => console.log("Works!")}
            activeOpacity={0.8}
          />
        )
      break;
    }
  }

  renderItems = () => {
    moment.locale('pt-BR')
    var dt_ini = moment(this.props.aula.dt_ini_data).calendar().replace("às","de");  

    return(
        <View style={{ flex: 1 }}>
            <ScrollView>
                <TouchableOpacity onPress={() => this.props.myFunc()  }  style={{ position: 'absolute', zIndex: 10, top: 0, right: 0}}>
                  <Icon style={{ textAlign:'center' }}
                        name="ios-remove-circle-outline"
                        size={34}
                        color={'#999'}  
                    />
                </TouchableOpacity>  
                <View style={{ width: '100%', height: 200, marginBottom: 15, alignItems: 'center', justifyContent: 'center' }}>
                  { this.returnAvatar() }
                </View>
                <View style={{ width: '100%', marginBottom: 0, justifyContent: 'flex-start'}}>
                    <Text style={[styles.txt3,{fontWeight: 'bold', fontSize: 18}]}>
                    
                        { this.props.aula.apelido || this.props.aula.nome ||'Aguardando Professor'}
                    
                    </Text>
                    <Text style={styles.txt3}>
                    {this.props.aula.categoria}
                    </Text>
                    <Text style={styles.txt3}>
                    {dt_ini} às {this.props.aula.dt_fim_hora}
                    </Text>

                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style={{ flexDirection: 'row', marginTop: 10}}>
                    {this.renderAssuntos()}
                    </ScrollView> 
            
                    <View style={{ width: '100%', backgroundColor: '#f9f9f9', borderTopWidth: 1,borderBottomWidth: 1, borderTopColor: '#f5f5f5',borderBottomColor: '#f5f5f5', minHeight: 60, padding: 5, marginTop: 15}}>
                        <Text style={[styles.txt3,{fontStyle: 'italic', fontSize: 12}]}>
                        {this.props.aula.mensagem||'Nenhuma mensagem deixada'}
                        </Text>
                    </View>

                </View>  
                
                <View style={{ width:'96%', marginTop: 0, flexDirection: 'row', justifyContent: 'space-between' , borderTopColor: '#f5f5f5', borderTopWidth: 0, paddingTop: 10}}>
                    {this.props.origem != 'mural' && this.props.origem != 'meusagendamentos' &&    
                    <TouchableOpacity style={styles.btn} onPress={() => null  } >
                        <Text style={styles.btnTxt}>AJUDA</Text>
                    </TouchableOpacity>
                    }
                    {this.props.origem == 'historico' && 
                        <TouchableOpacity style={styles.btn} onPress={() => null  } >
                            <Text style={styles.btnTxt}>AVALIAR</Text>
                        </TouchableOpacity>
                    }
                    {this.props.origem == 'agendamentos' || this.props.origem == 'meusagendamentos'&& 
                        <TouchableOpacity style={styles.btn} onPress={() => this.cancelar()  } >
                            <Text style={[styles.btnTxt,{color: 'red'}]}>CANCELAR</Text>
                        </TouchableOpacity>
                    }
                    {this.props.origem == 'mural' && 
                        <TouchableOpacity style={[styles.btn,{width: '100%'}]} onPress={() => this.pegarAula()  } >
                          <Text style={styles.btnTxt}>PEGAR AULA</Text>
                      </TouchableOpacity>
                    }
                    
                </View>
            
            </ScrollView>
        </View>
    )};


    render(){

        return (
            <View style={{ flex: 1}}>
                {this.renderItems()}
            </View>
        );
    } 
};

AulaDetalhes.propTypes = {
  myFunc: propTypes.func,
  myFuncNewAula: propTypes.func,
};


export default AulaDetalhes;

const styles = StyleSheet.create({
  container: {
      
    flexDirection: 'column',
    alignItems: 'stretch',
    
    paddingTop: 0,
    paddingLeft: 20,
    marginTop: 0
  },
  
  vertodos:{
    fontSize: 12,
    color: '#777',
    fontFamily: 'Montserrat-Light'
  },
  txt2:{
    fontSize: 13
    ,
    color: '#FFF',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light'
  },
  box: {
    
    padding: 10,
    backgroundColor: 'white',
    paddingRight: 15,
    width: '85%', 
    
    borderRadius: 30,
    borderWidth: 1,    
    borderColor: '#FFF',
    
  },
  boxRow:{
    flexDirection: 'row',
    marginRight: 10,
    backgroundColor: '#F5f5f5',
    height: 75,
    padding: 10,
    paddingRight: 15,
    borderRadius: 30,
    maxWidth: 250
  },
  txt1:{
    fontSize: 13,
    color: '#999',
    textAlign: 'left',
    marginBottom: 2,
    fontFamily: 'Montserrat-Light'
  },
  marcar:{
    fontFamily: 'Montserrat-Regular'
  },
  txt3:{
    fontSize: 16,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 5
  },
  txt4:{
    fontSize: 14,
    color: '#FFF',
    fontFamily: 'Montserrat-Light'
  },
  btn: {
    
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 7,
    paddingBottom: 7,
    
    borderRadius: 30
  },
  btnTxt: {
     fontSize: 16, 
     color: '#29A9AB' ,
     fontFamily: 'Montserrat-Light',
  }
});
