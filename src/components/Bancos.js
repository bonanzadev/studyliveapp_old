import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  KeyboardAvoidingView,
} from 'react-native';
import propTypes from 'prop-types';

import General from '../styles/general';
import api from '../services/api';
import Icon from 'react-native-vector-icons/FontAwesome';

class Bancos extends Component {

    state = { 
        data: [], 
        filteredData: [],
        error: '',
        searchText: "",
        isLoading: true,
        refreshing: false,
        idBanco: '',
        displayBanco: '',
    };

    selectItem = (item) => {
    
        let concat = item.codigo+" - "+item.nome
        this.props.myFunc(item.id,concat);
    }

    search = (searchText) => {
        this.setState({searchText: searchText});
      
        let filteredData = this.state.data.filter(function (item) {
          return item.nome.toUpperCase().includes(searchText.toUpperCase()) || item.codigo.includes(searchText);
        });
      
        this.setState({filteredData: filteredData});
        
    };

    /* Listando dados  */
    listDados = async () => {
        this.setState({isLoading: true,filteredData: []});
        
        try{
          
            const response = await api.get(`/banco`)
             
            const dados = response.data;
            
            this.setState({data: dados});

        } catch (response) {
            console.log("data: ",response)    
          this.setState({
                error: 'Nenhuma categoria cadastrada...'
          });
          
        }
        this.setState({isLoading: false});
    }


    _renderItem = ({item}) => {

        return (
            <TouchableOpacity key={item.id} style={styles.subcategoria_box} onPress={() => this.selectItem(item)}>
                
                <Text style={styles.subcategoria_title}>{item.codigo} - {item.nome}</Text>
                
            </TouchableOpacity>
        )
    }

    componentWillMount(){
        this.listDados();
    }

    render(){
        return(
            <KeyboardAvoidingView style={styles.container} behavior="padding" disabled>
                <View style={{flexDirection: 'row', marginBottom: 0, alignItems: 'center', justifyContent:'center',paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#F5F5F9'}}>
                    <View style={General.searchSection100}>
                        <Icon style={General.searchIcon} 
                        name='search'
                        size={20}
                        color='black'
                        />
                        <TextInput
                            style={General.input}
                            onChangeText={this.search}
                            placeholder="Buscar por..."
                            underlineColorAndroid="transparent"
                        />
                    </View>
                </View>

                <ScrollView style={{ height: '80%', }}>
                    
                    <FlatList
                        style={{ marginTop: 5}}
                        data={this.state.filteredData && this.state.filteredData.length > 0 ? this.state.filteredData : this.state.data}
                        showsVerticalScrollIndicator={false}
                        
                        renderItem={
                            this._renderItem
                        }
                        keyExtractor={item => item.id_banco}
                    />
                    
                </ScrollView>
            </KeyboardAvoidingView>
        )
    } 
};

Bancos.propTypes = {
    myFunc: propTypes.func
};


export default Bancos;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    subcategoria_box: {
        width: '100%', 
        height: 40, 
        borderRadius: 3, 
        backgroundColor: '#f5f5f5', 
        borderColor: '#F3f3f3',
        borderWidth: 1,
        justifyContent: 'center',
        marginTop: 5
    },
    subcategoria_title: {
        marginLeft: 10, 
        color: '#777',
        fontSize: 12,
        fontFamily: 'Montserrat-Light',
    },
    subcategoria_icon: {
        color: '#777',
        marginLeft: 5,
        fontSize: 20
    },
    btn: {
        width: 50, 
        height: 50, 
        position: 'absolute', 
        backgroundColor: '#29A9AB', 
        borderRadius: 50, 
        bottom: 10, 
        right: 10, 
        justifyContent: 'center', 
        alignItems: 'center',
    }
});

