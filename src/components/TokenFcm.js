import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import api from '../services/api';
export default getToken = async () => {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
    const newToken = await firebase.messaging().getToken();
    const id = await AsyncStorage.getItem('@StudyLiveApp:id') || false;
    let tokenFcm = await AsyncStorage.getItem('@StudyLiveApp:tokenFcm')|| false;
    if (!tokenFcm || tokenFcm == "" || tokenFcm == undefined || tokenFcm != newToken) {
      if (newToken) {
        try {
          const response = await api.put(`/updateToken/${id}`,{
            tokenFcm: newToken
          });
          if(response.data.affectedRows > 0)
            AsyncStorage.setItem('@StudyLiveApp:tokenFcm', newToken);
          
        } catch (error) {
          this.getToken();
        }
      }
    }
  } else {
    try {
      await firebase.messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }
}



