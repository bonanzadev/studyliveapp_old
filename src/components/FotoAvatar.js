import domain from '../services/domain';

function FotoAvatar(id,avatar) {

    if( avatar !== undefined && avatar !==  null && avatar != ""){
        if( avatar.indexOf(".jpg") == -1){
            return domain+"/photos/"+id+"_avatar"+avatar+".jpg"
        } else {
            return domain+"/photos/"+avatar;
        }
    }
} 

export default FotoAvatar;