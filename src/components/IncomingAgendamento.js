import AsyncStorage from '@react-native-community/async-storage';
import {StackActions, NavigationActions } from 'react-navigation';
export default IncomingAgendamento = async (aula, navigation) => {
  var id = await AsyncStorage.getItem('@StudyLiveApp:id') || false;
  var tipousuario = await AsyncStorage.getItem('@StudyLiveApp:tipousuario') || false;
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ 
        routeName: "Espera", 
        params: { 
          id_aula: aula.id,
          uid: id,
          totalMinutos: (aula.qtdaula*30),
          tipousuario: tipousuario,
          channelName: aula.channel,
          contadorEmSegundos: aula.contadorEmSegundos,
          origem:'agendamento'
        }
      })
  ],
  });

  navigation.dispatch(resetAction)
  // navigation.navigate("Agora", {
  //   id_aula: aula.aula.id,
  //   uid: id,
  //   totalMinutos: (aula.aula.qtdaula*30),
  //   tipousuario: tipousuario,
  //   channelName: aula.aula.channel
  // });
}



