import React, { Component } from 'react';
import Avatar from './avatar';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Swiper from 'react-native-swiper'
import General from '../styles/general';
import api from '../services/api';
import AulaDetalhes from '../components/AulaDetalhes';
import FotoAvatar from './FotoAvatar';
import Modal from "react-native-modal";
import moment from "moment";
import 'moment/locale/pt-br'; 
import propTypes from 'prop-types';

class Agendamentos extends Component {

  state = {
    aulas: [],
    avatar1:'',
    aulaAtual: [],
    receivedProps: this.props.receivedProps
  };

  loadData = async () => {

    try{
        
      const id = this.props.id;

      const response = await api.get(`/muralAluno/${id}`);
      
      this.setState({
        aulas: response.data.aulas,
      },() => {
        this.renderItems()
      })
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }
    
  }

  componentWillUnmount() {
    this.willFocus.remove()
  }


  componentDidMount(){
    this.willFocus = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.loadData() 
      }
    ) 

    this.loadData()
  }

  renderAssuntos = () => {
    if(this.state.aulaAtual.assuntos != undefined){
      return this.state.aulaAtual.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  closeModal = () => {
    this.loadData()
    this.setState({ visibleModal: null })
  }

  renderModalAulaContent = () => {
    
    return(
      <View style={styles.content}>
        <ScrollView>
          
          <AulaDetalhes 
            aula={this.state.aulaAtual} 
            origem={'agendamentos'} 
            myFunc={this.closeModal}  
          />
          
        </ScrollView>
      </View>
    )
  };

  renderItems = () => {
    const {navigate} =this.props.navigation;
    if(this.state.aulas.length > 0){
      return this.state.aulas.map((item) => {

        moment.locale('pt-BR')
        var dt_ini = moment(item.dt_ini_data).calendar().replace("às","de");  
        
        return (
              <View style={{ 
                  marginLeft: 0, 
                  marginRight: 0, 
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 30,
                  }}>
                    
                  <TouchableOpacity style={styles.box} onPress={() => this.setState({ visibleModal: 'agendamento', aulaAtual: item })  } >
                      <View style={{ flexDirection: 'row', paddingRight: 60, paddingLeft: 5 }}>
                          <View style={{ marginLeft: 0, paddingTop: 3 }}>
                            { item.id_usuario_professor != 0 &&
                              <Avatar 
                                rounded
                                size="medium"
                                source={{ uri: FotoAvatar(item.id_usuario_professor,item.avatar) }}
                                onPress={() => console.log("Works!")}
                                activeOpacity={0.7}
                              />
                            }
                            { item.id_usuario_professor == 0 &&
                              <Avatar 
                                rounded
                                size="medium"
                                source={require("../images/who.png")}
                                onPress={() => console.log("Works!")}
                                activeOpacity={0.7}
                              />
                            }
                          </View>
                          <View style={{ marginTop: 0, marginLeft: 10, }}>
                              <Text style={styles.txt1}>
                              <Text style={styles.marcar}>{item.apelido || item.nome || 'Aguardando Professor'}</Text>
                              </Text>
                              <Text style={styles.txt1}>
                              {item.categoria}
                              </Text>
                              <Text style={styles.txt1}>
                              {dt_ini} às {item.dt_fim_hora} 
                              </Text>
                          </View>  
                          </View>    
                          <View style={{ flexDirection: 'row', width: '95%', flexWrap: 'wrap',  paddingLeft: 5, justifyContent: 'flex-start', alignItems: 'center' }}>
                            {!item.assuntos.map((assunto, i) => {
                              return ( 
                                  <View key={i} style={{ marginRight: 5, backgroundColor: '#9BD3CE', borderRadius: 20, padding: 2, paddingLeft: 8, paddingRight: 8, justifyContent: 'center'}}>
                                      <Text style={styles.txt2}>{assunto.nome}</Text>
                                  </View>
                              ) 
                          })}
                              
                          </View>
                          
                          
                  </TouchableOpacity>
                </View>
              )
          })
    } else {
      return (
        <View style={{ 
          marginLeft: 0, 
          marginRight: 0, 
          marginTop: 30,
          }}>
            
          <TouchableOpacity style={{justifyContent: 'center',alignItems: 'center',}} onPress={() => navigate('Agendaraula')  } >
            
            <View style={{ width: 50, height: 50, marginBottom: 15}}>
              <Image source={require('../images/rocket.png')} resizeMode="contain" style={{ width:'100%', height:'100%'}} />
            </View>
            <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 13, color: '#777' }}>Nenhum agendamento cadastrado...</Text>      
            <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 16, color: '#2FA29A', marginTop: 10 }}>COMEÇAR</Text>      
                  
          </TouchableOpacity>
        </View>
      )
    }
    
  }

  render(){
    
    return (
        <View style={{ flex: 1}}>
            <Modal
              isVisible={this.state.visibleModal === 'agendamento'}
              onBackdropPress={() => this.setState({ visibleModal: '' }) }
              useNativeDriver={true}
              animationIn="zoomInDown"
              animationOut="zoomOutUp"
              animationInTiming={300}
              animationOutTiming={600}
              backdropTransitionInTiming={1000}
            >
              {this.renderModalAulaContent()}
            </Modal>
            <View style={{ marginLeft: 20, marginTop: 15, height: 15}}>
                <View style={{ position: 'absolute', marginTop: 3 }}>
                    
                    <Text style={General.titlePadrao1}>MEUS AGENDAMENTOS</Text>
                    
                </View>
                <View style={{ position: 'absolute', right: 0, marginRight: 30, marginTop: 5}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Historico') }>
                    <Text style={styles.vertodos}>VER TODOS</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ width: '100%', height: 175,   justifyContent: 'center', alignItems: 'center' }}>
              <Swiper 
                
                
                loop={false}
                showsButtons={false}
                dot={<View style={{backgroundColor: '#FFF', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
                activeDot={<View style={{backgroundColor: '#76C3D7', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
                nextButton={<Icon style={{ textAlign:'center', marginRight: 5 }}
                                name="ios-arrow-forward"
                                size={24}
                                color={'#CCC'}  
                            />}
                prevButton={<Icon style={{ textAlign:'center', marginTop: 2, marginLeft: 5, }}
                                name="ios-arrow-back"
                                size={24}
                                color={'#CCC'}  
                            />}
                paginationStyle={{
                  bottom: 30,
                }}
              >
                {this.renderItems()}
              </Swiper>
            </View>
        </View>
      );
  } 
};

Agendamentos.propTypes = {
  myFunc: propTypes.func
};

export default Agendamentos;

const styles = StyleSheet.create({
  container: {
      
    flexDirection: 'column',
    alignItems: 'stretch',
    
    paddingTop: 0,
    paddingLeft: 20,
    marginTop: 0
  },
  content: {
    backgroundColor: 'white',
    padding: 25,
    
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  vertodos:{
    fontSize: 12,
    color: '#777',
    fontFamily: 'Montserrat-Light'
  },
  txt2:{
    fontSize: 13
    ,
    color: '#FFF',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light'
  },
  box: {
    
    padding: 10,
    backgroundColor: 'white',
    paddingRight: 15,
    width: '85%', 
    marginTop: 5,
    borderRadius: 30,
    borderWidth: 1,    
    borderColor: '#FFF',
    
  },
  boxRow:{
    flexDirection: 'row',
    marginRight: 10,
    backgroundColor: '#F5f5f5',
    height: 75,
    padding: 10,
    paddingRight: 15,
    borderRadius: 30,
    maxWidth: 250
  },
  txt1:{
    fontSize: 13,
    color: '#999',
    textAlign: 'left',
    marginBottom: 2,
    fontFamily: 'Montserrat-Light'
  },
  marcar:{
    fontFamily: 'Montserrat-Regular',
    color: '#888'
  },
  txt3:{
    fontSize: 16,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 5
  },
  txt4:{
    fontSize: 14,
    color: '#FFF',
    fontFamily: 'Montserrat-Light'
  },
  btn: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 7,
    paddingBottom: 7,
    
    borderRadius: 30
  },
  btnTxt: {
     fontSize: 16, 
     color: '#29A9AB' ,
     fontWeight: '600'
  }
});
