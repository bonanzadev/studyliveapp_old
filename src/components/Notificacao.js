import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';
import api from '../services/api';
import Modal from "react-native-modal";

class Notificacao extends Component {

  state = {
      isLoading: false,
      visibleModal: '',
      qtd: 0,
      id: '',
      data: []
  };

  componentDidMount(){
    this.loadData( this.props.id )
  }

  componentWillReceiveProps(){
    this.loadData( this.props.id )
  }

  loadData = async (id) => {
    this.setState({isLoading: true});
    
    try{
      
      const response = await api.get(`/notificacao/${id}`);
      const response2 = await api.get(`/qtdNotificacao/${id}`);

      Promise.all([
          response,
          response2,
      ]).then((values) => {

        const promisse1 = values[0];
        const promisse2 = values[1];

        console.log('a ',promisse2)

        this.setState({
            data: promisse1.data.data || [],
            qtd: promisse2.data.data.qtd || 0,
            isLoading: false,
        });

      });
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }
  }

  openNotifications = async () => {
    try{

      const id = this.props.id;
      
      this.setState({ visibleModal: 'notifications', qtd: 0 });

      await api.post(`/notificacao/${id}`);

    } catch (response) {
        
      this.setState({error: true, tooltip: "Houve um erro"});
    }
  }

  closeModal = () => {
    this.setState({ visibleModal: null })
  }

  render(){
    const { data, qtd, }  = this.state;
    
    if( data === undefined ){
      data = [];
    }
    
    return (
        <View>
            <Modal
                isVisible={this.state.visibleModal === 'notifications'}
                useNativeDriver={true}
                hideModalContentWhileAnimating={true}
                backdropColor="#000"
                backdropOpacity={0.8}
                onBackdropPress={() => this.closeModal() }
            >
              <View style={styles.notifications}>
                { data.length > 0 && 
                  <ScrollView style={{ width: '100%' }}>
                    {data.map((item,i) => {
                      return(
                        <View key={item.i} style={{ width: '100%', minHeight: 70, backgroundColor: '#f0f0f0', paddingBottom: 7, marginBottom: 1 }}>
                          <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                            <Icon style={{ marginTop: 5 }}
                              name={item.icon}
                              size={22}
                              color={'#000'}  
                            />
                            <Text style={{ color: '#333', fontSize: 14, fontWeight: '600', marginLeft: 10, marginTop: 5, fontFamily: 'Montserrat-Regular',}}>{item.titulo}</Text>
                          </View>
                          <Text style={{ color: '#777', fontSize: 13, marginLeft: 10, marginTop: 5, fontFamily: 'Montserrat-Light',}}>{item.descricao}</Text>
                        </View>
                      )
                    })}
                  </ScrollView>
                }
                { data.length == 0 && 
                  <View style={{ width: '100%', minHeight: 70, backgroundColor: '#f0f0f0', justifyContent: 'center', alignItems: 'center', marginBottom: 1 }}>
                    <Text style={{ color: '#555', fontSize: 16, fontWeight: '600',}}>Nenhuma notificação no momento</Text>
                  </View>
                }
              </View>
            </Modal>
            { qtd > 0 && this.props.origem === undefined &&
              <View style={styles.qtd}>
                <Text style={{ color: 'white', fontSize: 12, }}>{qtd}</Text>
              </View>
            }
            { this.props.origem === undefined &&
              <TouchableOpacity onPress={() => this.openNotifications() } hitSlop={{top: 50, left: 50, bottom: 50, right: 50}}  style={{ width:'92%', height: '92%', justifyContent: 'center', alignItems: 'center'}}>
                <Image source={require('../images/liveAnimated.gif')} resizeMode="cover"  style={{ width:'100%', height: '100%', position: 'absolute', zIndex: 30}} />
              </TouchableOpacity> 
            } 
            { this.props.origem === 'professor' && qtd > 0 &&
              <TouchableOpacity onPress={() => this.openNotifications() } hitSlop={{top: 50, left: 50, bottom: 50, right: 50}}  style={styles.notificationsProf}>
                <Text style={{ color: 'white', fontSize: 12, }}>{qtd}</Text>
              </TouchableOpacity> 
            } 

        </View>
      );
  } 
};

export default Notificacao;

const styles = StyleSheet.create({
  notifications: {
    backgroundColor: 'white',
    padding: 5,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 5,
  },
  notificationsProf: {
    position: 'absolute', 
    zIndex: 21, 
    bottom: -5, 
    right: -10,
    top: -5,
    width: 25, 
    height: 25, 
    borderRadius: 25, 
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center'
  },
  qtd: {
    width: 20, 
    height: 20, 
    backgroundColor: 'red', 
    borderRadius: 20, 
    justifyContent: 'center', 
    alignItems: 'center', 
    color: 'white', 
    position: 'absolute',
    zIndex: 40,
    right: -25,
    top: -10
  }
});
