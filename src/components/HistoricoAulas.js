import React, { Component } from 'react';
import Avatar from './avatar';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import General from '../styles/general';
import api from '../services/api';
import Modal from "react-native-modal";
import AulaDetalhes from '../components/AulaDetalhes';
import moment from "moment";
import 'moment/locale/pt-br';
import FotoAvatar from './FotoAvatar';
import propTypes from 'prop-types';

class HistoricoAulas extends Component {

  state = {
    aulas: [],
    avatar1:'',
    aulaAtual: []
  };

  componentDidMount(){
    this.loadData()
  }

  componentWillUnmount() {
    this.willFocus.remove()
  }
  componentDidMount(){
    this.willFocus = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.loadData() 
      }
    ) 

    this.loadData()
  }

  loadData = async () => {

    try{
        
      const id = this.props.id;

      const response = await api.get(`/historicoAulasAluno/${id}`);
      
      this.setState({
        aulas: response.data.aulas,
      },() => {
        this.renderItems()
      })
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }
  }

  renderItems = () => {
    const {navigate} =this.props.navigation;
    if(this.state.aulas.length > 0){

      return this.state.aulas.map((item, i) => {
        moment.locale('pt-BR')
        
        var dt_ini = moment(item.dt_ini_data).calendar().replace("às","de");  
        var avatar = FotoAvatar(item.id_usuario_professor,item.avatar)
        return (
          <TouchableOpacity key={i} onPress={() => this.setState({ visibleModal: 'aulas', aulaAtual: item })  } >
            <View style={styles.boxRow}>
                <View>
                  
                    <Avatar 
                    rounded
                    size="medium"
                    source={{uri: avatar}}
                    onPress={() => console.log("Works!")}
                    activeOpacity={0.7}
                    />
                  
                </View>
                <View style={{ marginTop: 0, marginLeft: 10}}>
                    <Text style={styles.txt1}>
                    <Text style={styles.marcar}>{ item.apelido || item.nome || 'Aguardando Professor'}</Text></Text>
                    <Text style={styles.txt1}>{item.categoria}</Text>
                    <Text style={styles.txt1}>{dt_ini} às {item.dt_fim_hora} </Text>
                    
                </View>
            </View>
          </TouchableOpacity>
        )
      })
    } else {
      return (
        <View style={{ 
          marginLeft: 5, 
          marginRight: 0, 
          marginTop: 20,
          }}>
            
          <TouchableOpacity style={{justifyContent: 'center',alignItems: 'center',}} onPress={() => navigate('Historico')  } >

            <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 13, color: '#777' }}>Nenhuma aula ainda...</Text>      
                  
          </TouchableOpacity>
        </View>
      )
    }
  }

  renderAssuntos = () => {
    if(this.state.aulaAtual.assuntos != undefined){
      return this.state.aulaAtual.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  closeModal = () => {
    this.loadData()
    this.setState({ visibleModal: null })
  }

  renderModalAulaContent = () => {
    
    return(
      <View style={styles.content}>
        <ScrollView>
          
          <AulaDetalhes aula={this.state.aulaAtual} origem={'historico'} myFunc={this.closeModal} />
          
          
        </ScrollView>
      </View>
    )
  };

  render(){

    const {navigate} =this.props.navigation;

    return (
        <View style={styles.container}>
            <Modal
            isVisible={this.state.visibleModal === 'aulas'}
            onBackdropPress={() => this.setState({ visibleModal: '' }) }
            useNativeDriver={true}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={300}
            animationOutTiming={600}
            backdropTransitionInTiming={1000}
          >
            {this.renderModalAulaContent()}
          </Modal>
            <View style={{ marginLeft: 5, marginRight: 0, height: 30}}>
                <View style={{ position: 'absolute', marginTop: 10 }}>
                    
                <Text style={General.titlePadrao1}>HISTÓRICO</Text>
                    
                </View>
                <View style={{ position: 'absolute', right: 0, marginRight: 30, marginTop: 12}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Historico') }>
                    <Text style={styles.vertodos}>VER TODOS</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{height: 115, marginTop: 10, paddingLeft: 0}}>
                <ScrollView horizontal={true}  style={{flexDirection: 'row' }}>
                { this.renderItems()}    
                </ScrollView>
            </View>
        </View>
      );
  } 
};

HistoricoAulas.propTypes = {
  myFunc: propTypes.func
};

export default HistoricoAulas;

const styles = StyleSheet.create({
  container: {
      
    flexDirection: 'column',
    width: '100%',
    
    paddingTop: 0,
    paddingLeft: 20,
    marginTop: 0
  },
  vertodos:{
    fontSize: 12,
    color: '#777',
    fontFamily: 'Montserrat-Light'
  },
  marcar:{
    fontFamily: 'Montserrat-Medium'
  },
  content: {
    backgroundColor: 'white',
    padding: 25,
    
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  boxRow:{
    flexDirection: 'row',
    marginRight: 10,
    backgroundColor: '#F5f5f5',
    borderColor: '#FFF',
    borderWidth: 1,
    height: 80,
    padding: 10,
    paddingRight: 15,
    borderRadius: 50,
    
  },
  txt1:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 2
  },
  txt2:{
    fontSize: 12,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
  },
  txt3:{
    fontSize: 16,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    marginBottom: 5
  },
  txt4:{
    fontSize: 14,
    color: '#FFF',
    fontFamily: 'Montserrat-Light',
  },
  btn: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 7,
    paddingBottom: 7,
    
    borderRadius: 30
  },
  btnTxt: {
     fontSize: 16, 
     color: '#29A9AB' ,
     fontFamily: 'Montserrat-Light',
  }
});
