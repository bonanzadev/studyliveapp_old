import React, { Component } from 'react';
import {
  View
} from 'react-native';
import { CheckBox } from 'react-native-elements';

class CheckBoxItem extends Component {
    state = {
      check: this.props.checked || false,
    }
  
    onValueChange = () => {
      
      this.setState(previous => {
        return  { check: !previous.check }
      }, () => this.props.onUpdate()); 
    
    } 
  
    render() {
      return (
        <View style={{ width: '100%' }}>
          <CheckBox 
            containerStyle={{ marginTop: 0, marginLeft: 0, marginRight: 0 }}
            title={this.props.label}
            checked={this.state.check} 
            onPress={this.onValueChange} 
          />
        </View>
      );
    }
}
export default CheckBoxItem;
