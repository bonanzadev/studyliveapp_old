import React, { Component } from 'react';
import {
  View
} from 'react-native';

import General from '../styles/general';

class Divisao extends Component {
  render(){
    const {bordaTop,bordaBottom} = this.props

    if(bordaTop != undefined){
      var resp = {
        marginTop: bordaTop
      }
    }
    if(bordaBottom != undefined){
      var resp = {
        marginBottom: bordaBottom
      }
    }
    if(bordaTop != undefined && bordaBottom != undefined){
      var resp = {
        marginTop: bordaTop,
        marginBottom: bordaBottom
      }
    }
    
    
    return (
      <View style={[General.divisao,resp]}></View>
    );
    
  } 
};

export default Divisao;
