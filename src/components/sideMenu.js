
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import { NavigationActions } from 'react-navigation';
import {ScrollView, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';

class SideMenu extends Component {
    state = {
        tipousuario: '',
        id: ''
    }

    componentDidMount(){
        
        this.verificaStorage()
    }

    verificaStorage = async () => {
        let id = await AsyncStorage.getItem('@StudyLiveApp:id') || null;
        let tipousuario = await AsyncStorage.getItem('@StudyLiveApp:tipousuario') || null;

        this.setState({ tipousuario, id });
    }    

    navigateToScreen = (route) => () => {
        if(route=="Meusdados"){
            
            if(this.state.tipousuario==2){
                route = "Meusdados_professor";
            } 
        }

        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        
        if( route == 'Login' ) {
            this.logout();
        }
        
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.closeDrawer();
    }

    logout = async () => {
        await AsyncStorage.removeItem('@StudyLiveApp:id');
        //await AsyncStorage.removeItem('@StudyLiveApp:login');
        await AsyncStorage.removeItem('@StudyLiveApp:senha');
    }

    render () {
    return (
        <View style={[ styles.menu ]}>
        <ScrollView>
                
            <View style={ styles.row}>
                <TouchableOpacity onPress={this.navigateToScreen('Historico')}>
                    <View style={{ flexDirection: 'row'  }}>
                        <View style={{ marginLeft: 12, width: 35  }}>
                            <View style={{  flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start',  }}>
                            <Icon style={styles.searchIcon} 
                                    name='graduation-cap'
                                    size={24}
                                    color='#FFF'
                                />
                            </View>        
                        </View>
                        <View style={{ marginLeft: 15, marginTop: 0  }}>
                            <Text style={ styles.label2 }>Histórico de Aulas</Text>
                        </View>
                    </View>  
                </TouchableOpacity>   
            </View>  
                
             
            <View style={ styles.row}>
                <TouchableOpacity onPress={this.navigateToScreen('Meusdados')}>
                    <View style={{ flexDirection: 'row'  }}>
                        <View style={{ marginLeft: 12, width: 35  }}>
                            <View style={{  flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start',  }}>
                            <Icon style={styles.searchIcon} 
                                    name='user-circle-o'
                                    size={24}
                                    color='#FFF'
                                />
                            </View>        
                        </View>
                        <View style={{ marginLeft: 15, marginTop: 0  }}>
                            <Text style={ styles.label2 }>Meus dados</Text>
                        </View>
                    </View>
                </TouchableOpacity>    
            </View>     
            <View style={ styles.row}>
                <TouchableOpacity onPress={this.navigateToScreen('Login')}>
                    <View style={{ flexDirection: 'row'  }}>
                        <View style={{ marginLeft: 12, width: 35  }}>
                            <View style={{  flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start',  }}>
                            <Icon style={styles.searchIcon} 
                                    name='sign-out'
                                    size={24}
                                    color='#FFF'
                                />
                            </View>        
                        </View>
                        <View style={{ marginLeft: 15, marginTop: 0  }}>
                            <Text style={ styles.label2 }>Sair</Text>
                        </View>
                    </View>
                </TouchableOpacity>    
            </View>    
            
        </ScrollView>

      </View>


    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;

const styles = StyleSheet.create({

    menu: {
      alignItems: 'stretch',
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      backgroundColor: '#34A59C',
      paddingTop: 20
    },
    user: {
      width: '100%',
      height: 90,
      paddingLeft: 20, 
      paddingTop: 20, 
    
      flexDirection: 'row'
    },
    row: {
      flex: 0, 
      height: 40,
      paddingLeft: 20, 
      marginTop: 20,
      width: '100%',
      justifyContent: 'flex-start',
      borderColor: '#FFF',
      
      flexDirection: 'row'
    },
    label1: {
      fontSize: 18,
      color: 'white',
      fontWeight: '600'
    },
    label3: {
      fontSize: 12,
      color: 'white',
      fontWeight: '300',
      marginLeft: 2
    },
    label2: {
      fontSize: 16,
      color: '#FFF',
      fontFamily: 'Montserrat-Regular',
    },
    line: {
      marginTop: 20,
      marginBottom: 0,
      backgroundColor: '#F0F0F0',
      height:10
    },
  });
  