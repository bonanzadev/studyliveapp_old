import React, { Component } from 'react';
import Avatar from './avatar';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  
  ActivityIndicator,
  Dimensions
} from 'react-native';
import api from '../services/api';
import AulaDetalhes from '../components/AulaDetalhes';
import Modal from "react-native-modal";
import moment from "moment";
import 'moment/locale/pt-br'; 
import propTypes from 'prop-types';
import FotoAvatar from './FotoAvatar';

var {width, height} = Dimensions.get('window');

class Mural extends Component {

  state = {
      aulas: [],
      avatar1:'',
      aulaAtual: [],
      refreshing: false,
      isLoading: false
  };

  loadData = async () => {
    this.setState({isLoading: true});
    
    try{
        
      const id = this.props.id;

      const response = await api.get(`/muralProfessor/${id}`);
      
      console.log('mural ',response)
      this.setState({
          aulas: response.data.aulas || [],
          refreshing: false,
          isLoading: false
      })
      
    } catch (response) {
      
      this.setState({error: true, tooltip: "Houve um erro"});
    }
  }

  componentDidMount(){
    this.loadData()
  }

  renderAssuntos = () => {
    if(this.state.aulaAtual.assuntos != undefined){
      return this.state.aulaAtual.assuntos.map((assunto) => {
        return ( 
          <View style={{ marginRight: 5, backgroundColor: '#76C3D7', borderRadius: 20, padding: 2, paddingLeft: 5, paddingRight: 5, justifyContent: 'center'}}>
              <Text style={styles.txt4}>{assunto.nome}</Text>
          </View>
        )
      })  
    }
  }

  renderModalNewAulaContent = () => (
      <View style={styles.newAulaSuccessful} onPress={ this.closeModal }>
      
      <View style={{ width: 100, height: 200, justifyContent: 'center', alignItems: 'center'  }}>
          <Image source={require('../images/ok.gif')} style={{width: '100%', height: '100%'}} />
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center'  }}>
          <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 22, color: '#2FA29A' }}>Agendamento feito!</Text>
          <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 16, marginTop: 15, color: '#777' }}>
          Agora é com você! Te desejamos uma excelente aula
          </Text>
      </View>

      <TouchableOpacity style={{ position: 'absolute', zIndex: 2, bottom: 50 }} onPress={() => this.closeModal() } >
              <Text style={{ fontSize: 18, color: '#29A9AB' ,fontFamily: 'Montserrat-Regular' }}>FECHAR</Text>
          </TouchableOpacity>
      
      </View>
  );

  closeModalNewAula = () => {
      
      this.setState({ visibleModal: null })
      this.setState({ visibleModal: 'newAulaSuccessful' });
      this.props.myFunc();
      this.loadData();
  }
  closeModal = () => {
      
      this.setState({ visibleModal: null });
      this.loadData();
      this.props.myFunc();
  }

  renderModalAulaContent = () => {
    
    return(
      <View style={styles.content}>
        <ScrollView>
          
          <AulaDetalhes 
            aula={this.state.aulaAtual} 
            origem={'mural'} 
            id={this.props.id}
            myFunc={this.closeModal}
            myFuncNewAula={this.closeModalNewAula}  />
          
          
        </ScrollView>
      </View>
    )
  };

  renderItems = () => {
    
    
    if(this.state.aulas != ""){
      
      return this.state.aulas.map((item, i) => {

        moment.locale('pt-BR')
        var dt_ini = moment(item.dt_ini_data).calendar().replace("às","de");  

        return (
                
          <TouchableOpacity key={i} activeOpacity={0.9} style={styles.box} onPress={() => this.setState({ visibleModal: 'agendamento', aulaAtual: item })  } >
            <View style={{ flexDirection: 'row', paddingRight: 60, paddingLeft: 5 }}>
              <View style={{ marginLeft: 0, paddingTop: 3 }}>
                  { item.avatar !== "" && 
                    <Avatar 
                      rounded
                      size="large"
                      source={{ uri: FotoAvatar(item.id_usuario_aluno,item.avatar) }}
                      onPress={() => console.log("Works!")}
                      activeOpacity={0.9}
                    />
                  }
                  { item.avatar === "" && 
                    <Avatar 
                      rounded
                      size="large"
                      source={require("../images/user-icon.jpg")}
                      onPress={() => console.log("Works!")}
                      activeOpacity={0.9}
                    />
                  }
                  </View>
                  <View style={{ marginTop: 0, marginLeft: 10, }}>
                      <Text style={styles.txt1}>
                      <Text style={styles.marcar}>{item.apelido || item.nome}</Text>
                      </Text>
                      <Text style={styles.txt1}>
                      {item.categoria}
                      </Text>
                      <Text style={styles.txt1}>
                      {dt_ini} às {item.dt_fim_hora} 
                      </Text>
                  </View>  
              </View>    
              <View style={{ width: '100%', borderRadius: 5, minHeight: 30, padding: 5, marginTop: 10}}>
                  <Text style={[styles.txt3,{fontStyle: 'italic', fontSize: 13}]}>
                  {item.mensagem||'Nenhum mensagem deixada'}
                  </Text>
              </View> 
              <View style={{ marginTop: 10, marginLeft: 0,borderTopWidth: 0 , borderTopColor: '#f5f5f5',paddingTop: 5,}}>
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style={{ flexDirection: 'row', marginTop: 0}}>
                  {item.assuntos.map((assunto,i) => {
                    return ( 
                      <View key={i} style={{ marginRight: 5, backgroundColor: '#9BD3CE', borderRadius: 20, padding: 2, paddingLeft: 8, paddingRight: 8, justifyContent: 'center'}}>
                          <Text style={styles.txt2}>{assunto.nome}</Text>
                      </View>
                    ) 
                  })}
                  </ScrollView>
              </View>
          </TouchableOpacity>
        )   
      })
    } else {
      return (
        <TouchableOpacity style={styles.box} onPress={() => this.clicked  } >
          <Text style={{ color: 'black' }}>Nenhuma aula registrada</Text>
        </TouchableOpacity>
      )
    }
    
  }

  render(){
    
    return (
        <View style={{ flex: 1}}>
            <Modal
              isVisible={this.state.visibleModal === 'agendamento'}
              onBackdropPress={() => this.setState({ visibleModal: '' }) }
              useNativeDriver={true}
              animationIn="zoomInDown"
              animationOut="zoomOutUp"
              animationInTiming={300}
              animationOutTiming={600}
              backdropTransitionInTiming={1000}
            >
              {this.renderModalAulaContent()}
            </Modal>
            <Modal
            isVisible={this.state.visibleModal === 'newAulaSuccessful'}
            deviceWidth={width}
            backdropColor={'#EFF1EF'}
            backdropOpacity={1}
            useNativeDriver={true}
            animationIn="slideInLeft"
            animationOut="slideOutRight"
            >
            {this.renderModalNewAulaContent()}
            </Modal>
            { this.state.aulas == "" && 
              <View style={{ width: '100%', height: '80%', justifyContent: 'center', alignItems: 'center', }}>
                <Icon style={{  }}
                    name="ios-school"
                    size={32}
                    color={'#29A9AB'}  
                />
                <Text style={{ fontFamily: 'Montserrat-Light', fontSize: 13, color: '#777', marginTop: 5 }}>Nada no mural ainda...</Text>
              </View>
            }
            { this.state.aulas !== "" && 
              <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <ScrollView style={{ width: '100%' }}>
                    {this.state.isLoading && <ActivityIndicator size="large" color="green"/>}    
                    {this.renderItems()}
                    
                    <View style={{ width: '100%', height: 50 }}></View>
                </ScrollView>
              </View>
            }
        </View>
      );
  } 
};

Mural.propTypes = {
  myFunc: propTypes.func
};

export default Mural;

const styles = StyleSheet.create({
  container: {
      
    flexDirection: 'column',
    alignItems: 'stretch',
    
    paddingTop: 0,
    paddingLeft: 20,
    marginTop: 0
  },
  newAulaSuccessful: {
    backgroundColor: '#EFF1EF',
    marginLeft: -5,
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    
  },
  content: {
    backgroundColor: 'white',
    padding: 25,
    
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  vertodos:{
    fontSize: 12,
    color: '#777',
    fontFamily: 'Montserrat-Light'
  },
  txt2:{
    fontSize: 13
    ,
    color: '#FFF',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light'
  },
  box: {
    marginTop: 10,
    padding: 10,
    backgroundColor: 'white',
    paddingRight: 15,
    
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    borderWidth: 1,    
    borderColor: '#FFF',
    
  },
  boxRow:{
    flexDirection: 'row',
    marginRight: 10,
    backgroundColor: '#F5f5f5',
    height: 75,
    padding: 10,
    paddingRight: 15,
    borderRadius: 30,
    maxWidth: 250
  },
  txt1:{
    fontSize: 14,
    color: '#999',
    textAlign: 'left',
    marginBottom: 2,
    fontFamily: 'Montserrat-Light'
  },
  marcar:{
    fontFamily: 'Montserrat-Regular'
  },
  txt3:{
    fontSize: 16,
    color: '#999',
    textAlign: 'left',
    fontFamily: 'Montserrat-Italic',
    marginBottom: 5
  },
  txt4:{
    fontSize: 14,
    color: '#FFF',
    fontFamily: 'Montserrat-Light'
  },
  btn: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 7,
    paddingBottom: 7,
    
    borderRadius: 30
  },
  btnTxt: {
     fontSize: 16, 
     color: '#29A9AB' ,
     fontWeight: '600'
  }
});
