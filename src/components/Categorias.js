import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import propTypes from 'prop-types';

import General from '../styles/general';
import api from '../services/api';
import Icon from 'react-native-vector-icons/FontAwesome';

class Categorias extends Component {

    state = { 
        data: [], 
        filteredData: [],
        error: '',
        searchText: "",
        isLoading: true,
        refreshing: false,
        idCategoria: '',
        displayCategoria: '',
    };

    editNovaaula = (id,nome) => {
    
        this.props.myFunc(id,nome);
    }

    /* Listando dados  */
    listDados = async () => {
        this.setState({isLoading: true,filteredData: []});
        
        try{
          
            const response = await api.get(`/categoria`)
             
            const dados = response.data;
            
            this.setState({data: dados});

        } catch (response) {
            console.log("data: ",response)    
          this.setState({
                error: 'Nenhuma categoria cadastrada...'
          });
          
        }
        this.setState({isLoading: false});
    }


    _renderItem = () => {

        return this.state.data.map((item) => {
            return (item.id==this.state.idCategoria)?
            (
                <View key={item.id} style={General.box} >
                    <View elevation={5} style={{ 
                            shadowColor: "#28A79D",
                            shadowOpacity: 0.8,
                            shadowRadius: 20,
                            borderRadius: 10,
                            backgroundColor:'#28A79D',
                            elevation: 5,
                            shadowOffset: {
                                height: 2,
                                width: 2
                            } 
                        }}>
                        <Image source={{uri: item.thumb }} resizeMode="cover" style={{ width: 160, height: 120, borderRadius: 10}} />
                    </View>
                    <View style={{ marginTop: 5}}>
                        <Text style={General.btn2TextSel}>{item.nome}</Text>
                    </View>
                </View>
            ) : (
                <View key={item.id} style={General.box}>
                    <TouchableOpacity onPress={() => this.editNovaaula(item.id, item.nome)}>
                        <View>
                            <Image source={{uri: item.thumb }} resizeMode="cover" style={{ width: 160, height: 120, borderRadius: 10,}} />
                        </View>
                        <View style={{ marginTop: 5}}>
                            <Text style={General.btn2Text}>{item.nome}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        })
    }

    componentWillMount(){
        this.listDados();
    }

    render(){
        return (!this.props.hasTitle)?
        (
            <View>
                <View style={{flex: 0, height: 160}}>
                    <ScrollView horizontal={true}  style={{flexDirection: 'row' }}>
                        
                        {this._renderItem()}
                        
                    </ScrollView>
                </View>
            </View>
        ) : (
            <View style={General.container}>
                <View style={{flex:0, flexDirection: 'row', justifyContent: 'flex-start', height: 50}}>
                    <View style={{ width: '100%',flexDirection: 'row' }}>
                        <Icon 
                            style={{ marginTop: 3}} 
                            name='search'
                            size={20}
                            color='#777'
                        />
                        <Text style={General.title}>Filtrar por Categoria</Text>
                    
                    </View>
                </View>
                <View style={{flex: 0, height: 160}}>
                    <ScrollView horizontal={true}  style={{flexDirection: 'row' }}>
                        
                        {this._renderItem()}
                        
                    </ScrollView>
                </View>
            </View>
        )
    } 
};

Categorias.propTypes = {
    myFunc: propTypes.func
};


export default Categorias

