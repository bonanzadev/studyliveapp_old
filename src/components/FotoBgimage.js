import domain from '../services/domain';

function FotoBgimage(id,bgimage) {
    if( bgimage != null ){
        if( bgimage.indexOf(".jpg") == -1){
            return domain+"/photos/"+id+"_bgimage"+bgimage+".jpg"
        } else {
            return domain+"/photos/"+bgimage;
        }
    } else {
        return "";
    }
} 

export default FotoBgimage;