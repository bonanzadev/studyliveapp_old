import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  FlatList,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import propTypes from 'prop-types';

import General from '../styles/general';
import api from '../services/api';
import Icon from 'react-native-vector-icons/FontAwesome';

import CheckBoxItem from '../components/CheckBoxItem';

class Assuntos extends Component {

    state = { 
        data: [], 
        filteredData: [],
        selectedBoxes: [],
        error: '',
        selected: '',
        searchText: "",
        isLoading: true,
        refreshing: false,
    };

    componentDidMount() {
        
        if( this.props.selectedBoxes ) {
            this.props.selectedBoxes.map((item) => {
                this.setState(state => {
                    let selectedBoxes = state.selectedBoxes;
                    selectedBoxes.push({
                        id: item.id, nome: item.nome
                    }) 
                    return { selectedBoxes }; 
                })
            })
        }
    }

    editNovaaula = () => {
        this.props.myFunc(this.state.selectedBoxes);
    }

    /* Listando dados  */
    listDados = async () => {
        
        this.setState({isLoading: true,filteredData: []});
        
        try{
          
            const response = await api.get(`/assunto/${this.props.id_categoria}`)
             
            const dados = response.data;
            
            this.setState({data: dados});

        } catch (response) {
            console.log("data: ",response)    
          this.setState({
                error: 'Nenhum assunto cadastrado...'
          });
          
        }
        this.setState({isLoading: false});
    }

    search = (searchText) => {
        this.setState({searchText: searchText});
      
        let filteredData = this.state.data.filter(function (item) {
          return item.nome.toUpperCase().includes(searchText.toUpperCase());
        });
      
        this.setState({filteredData: filteredData});
        
    };

    onUpdate = (id,nome) => {
        
        this.setState(previous => {
            let selectedBoxes = previous.selectedBoxes;
            //let index = selectedBoxes[id].indexOf(id) 
            let index = selectedBoxes.findIndex(obj => obj.id === id)
            if (index === -1) {
                selectedBoxes.push({
                    id: id, nome: nome
                }) 
            } else {
                //selectedBoxes[id].splice(index, 1)
                selectedBoxes.splice(index, 1)
            }
            return { selectedBoxes }; 

        }); 
    }

    _renderItem = ({item}) => {

        
        let index = this.state.selectedBoxes.findIndex(obj => obj.id === item.id)

        if (index != -1) {
            var check = true;
        } else {
            var check = false;
        }

        //console.log("check: ",check)
        
        return (
            <View key={item.id} style={styles.subcategoria_box}>
                
                <CheckBoxItem label={item.nome} checked={check} onUpdate={this.onUpdate.bind(this,item.id,item.nome)}/>
                
            </View>
        )
    }

    componentWillMount(){
        this.listDados();
    }

    render(){
        return(
            <KeyboardAvoidingView style={styles.container} behavior="padding" disabled>
                <View style={{flexDirection: 'row', marginBottom: 0, alignItems: 'center', justifyContent:'center',paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#F5F5F9'}}>
                    <View style={General.searchSection100}>
                        <Icon style={General.searchIcon} 
                        name='search'
                        size={20}
                        color='black'
                        />
                        <TextInput
                            style={General.input}
                            onChangeText={this.search}
                            placeholder="Buscar por..."
                            underlineColorAndroid="transparent"
                        />
                    </View>
                </View>

                <ScrollView style={{ height: '80%', }}>
                    
                    <FlatList
                        style={{ marginTop: 5}}
                        data={this.state.filteredData && this.state.filteredData.length > 0 ? this.state.filteredData : this.state.data}
                        showsVerticalScrollIndicator={false}
                        
                        renderItem={
                            this._renderItem
                        }
                        keyExtractor={item => item.id_assunto}
                    />
                    
                </ScrollView>
                <TouchableOpacity style={styles.btn} onPress={() => {this.editNovaaula()}}>
                    <Icon 
                        style={{ marginTop: 3}} 
                        name='check'
                        size={28}
                        color='#FFF'
                    />
                </TouchableOpacity>
            </KeyboardAvoidingView>
        )
    } 
};

Assuntos.propTypes = {
    myFunc: propTypes.func
};


export default Assuntos;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    subcategoria_box: {
        flexDirection:'row', 
        width:"100%",  
    },
    subcategoria_title: {
        marginTop: 0, marginLeft: 15, color: '#777'
    },
    subcategoria_icon: {
        color: '#777',
        marginLeft: 5,
        fontSize: 20
    },
    btn: {
        width: 50, 
        height: 50, 
        position: 'absolute', 
        backgroundColor: '#29A9AB', 
        borderRadius: 50, 
        bottom: 10, 
        right: 10, 
        justifyContent: 'center', 
        alignItems: 'center',
    }
});

