import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import General from '../styles/general';
import Icon from 'react-native-vector-icons/Ionicons';

class CreditoAtual extends Component {
  render(){
    const {navigate} =this.props.navigation;
    
    return this.props.tipo == 1 ? (
        <View style={{ width: '100%',flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            
            <View style={{ marginTop: 5, marginRight: 30 }}>
                <Text style={{fontSize: 11,textAlign: 'left', color: "white",fontFamily: 'Montserrat-Regular',}}>Crédito Atual</Text>
                <Text style={{fontSize: 18,textAlign: 'left', color: 'white',fontFamily: 'Montserrat-Regular',}}>{this.props.saldo} Aulas</Text>
            </View>
            <View style={{  alignItems: 'center',marginTop: 5, }}>
                <TouchableOpacity style={styles.btn} onPress={() => navigate('Novaaula') }>
                  <Icon style={{ marginRight: 10 }}
                    name="ios-rocket"
                    size={20}
                    color={'#fff'}  
                />
                  <Text style={styles.txt}>NOVA AULA</Text>
                </TouchableOpacity>
            </View>
            
        </View>
      ) : (
        <View style={{ width: '100%',flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10, height: 60 }}>
            
            <View style={{ marginTop: 5, marginLeft: 20 }}>
                <Text style={{fontSize: 11, color: "white",fontFamily: 'Montserrat-Regular',}}>Saldo a Receber</Text>
                <Text style={{fontSize: 16, color: 'white',fontFamily: 'Montserrat-Regular',}}>{this.props.saldo||0} Aulas</Text>
            </View>
            
            <View style={{ marginTop: 5, marginRight: 15 }}>
                <TouchableOpacity style={styles.btn2} onPress={() => navigate('Novaaula') }>
                  <Icon style={{ marginRight: 10 }}
                    name="ios-add"
                    size={20}
                    color={'#fff'}  
                />
                  <Text style={styles.txt}>SOLICITAR CRÉDITO</Text>
                </TouchableOpacity>
            </View>

        </View>  
      );
  } 
};

export default CreditoAtual;

const styles = StyleSheet.create({
  btn: {
    paddingLeft: 35, paddingRight: 35,
    backgroundColor:'#2FA29A', 
    borderWidth: 1, 
    borderColor: '#00B2B2',
    borderRadius: 36,
    flexDirection: 'row',
    height: 38,
    justifyContent: 'center',
    alignItems: 'center',
    
    shadowColor: "#000",
    shadowOpacity: 0.3,
    shadowRadius: 20,
    elevation: 2,
    shadowOffset: {
        height: 2,
        width: 2
    } 
  },
  btn2: {
    paddingLeft: 15, paddingRight: 15,
    borderWidth: 1, 
    borderColor: '#666',
    borderRadius: 36,
    flexDirection: 'row',
    height: 38,
    justifyContent: 'center',
    alignItems: 'center',
    
    shadowColor: "#000",
    shadowOpacity: 0.3,
    shadowRadius: 20,
    elevation: 2,
    shadowOffset: {
        height: 2,
        width: 2
    } 
  },
  txt: {
    color: 'white',
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
  }
});