import React, { Component } from 'react';
import Avatar from './avatar';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import propTypes from 'prop-types';
import Fonts from '../styles/fonts';
import FotoAvatar from './FotoAvatar';
import api from '../services/api';
class Incoming extends Component {

  constructor(props){
    super(props);

    this.clockCall = "";
  }
  state = {
      visibleModal: '',
      aula: [],
      timer: 21,
      tooltip2: 'PEGAR AULA',
      isLoading: false
  };

  componentDidMount(){    
    this.startIncomingAula();
  }

  componentWillReceiveProps(){
    clearInterval(this.clockCall);
    this.startIncomingAula()
  }

  startIncomingAula = () => {
    this.setState({ 
      visibleModal: 'oportunidade',
      aula: this.props.dadosAula,
      // aula: JSON.parse(this.props.dadosAula),
    },() => { this.startTimer() });
  }

  joinAula = async() => {
    var id = await AsyncStorage.getItem('@StudyLiveApp:id') || false;
    var tipousuario = await AsyncStorage.getItem('@StudyLiveApp:tipousuario') || false;
    try{
      const response = await api.post(`/setAulaProfessor/${this.state.aula.id}`, {
        id_usuario_professor: id
      });
      
      if(response.data.affectedRows != undefined && response.data.affectedRows > 0){
        this.setState({ visibleModal: '' },() => {
          
          this.props.navigation.navigate("Agora", {
            id_aula: this.state.aula.id,
            uid: id,
            totalMinutos: (this.state.aula.qtdaula*30),
            tipousuario: tipousuario,
            channelName: this.state.aula.channel
          })
        });
      } else {
        this.setState({ tooltip2: 'JÁ POSSUI PROFESSOR...' },() => {
          setTimeout(() => {
            this.setState({ visibleModal: '' });
          }, 2000 );
        });
      }
      

    } catch (response) {
        console.log("data: ",response)    
      this.setState({
            error: 'Nenhuma categoria cadastrada...'
      });
      
    };
  }

  closeModal = () => {
    clearInterval(this.clockCall);
    this.setState({ visibleModal: '', timer: 20 });
  }

  renderAssuntos = () => {
    if(this.state.aula.assuntos !== undefined){
      return this.state.aula.assuntos.map((assunto) => {
        return (
          <Text style={{ color: 'white', fontFamily: Fonts.Bold, marginRight: 15, backgroundColor: '#222', borderRadius: 30, paddingTop: 8,paddingRight: 10, paddingLeft: 10 }}>{assunto.nome}</Text> 
        );
      });
    }
  }

  renderModalIncomingAulaContent = () => (
    <View style={{ width: '100%',height: 290, backgroundColor: '#000', borderColor: '#555', borderWidth: 1, borderRadius: 25, elevation: 5, }} onPress={ this.closeModal }>
      
      <View style={{ 
        width: 60, 
        height: 60, 
        position: 'absolute', 
        zIndex: 20, 
        backgroundColor: '#000', 
        borderRadius: 60, 
        borderColor: '#555', 
        borderWidth: 1,  
        marginTop: -40,
        right: 5,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 16,color: '#FFF' }}>
          {this.state.timer}s
        </Text>
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: "row", width: '100%', marginLeft: 15, marginRight: 15  }}>
        <View style={{ borderWidth: 9, borderColor: 'white', borderRadius: 50, marginRight: 10, marginLeft: 60,  marginTop: 5 }}>
          <Avatar 
            rounded
            size="medium"
            source={{uri: FotoAvatar(this.state.aula.id_usuario_aluno,this.state.aula.avatar) }}    
          />
        </View> 
        <Text style={{ fontFamily: Fonts.Light, fontSize: 16, marginTop: 15, color: '#CCC', marginRight: 80  }}>
          <Text style={{ color: 'white', fontFamily: Fonts.Bold }}>{this.state.aula.nomeAluno}</Text> 
          &nbsp;está procurando uma aula de&nbsp;
          <Text style={{ color: 'white', fontFamily: Fonts.Bold }}>{this.state.aula.displayCategoria}</Text> 
        </Text>
      </View>
  
      <View style={{ width: '94%', borderRadius: 5,  marginLeft: '3%', height: 80, backgroundColor: '#222', marginTop: 20, padding: 10, justifyContent: 'center', alignItems: 'center' }}>
        <ScrollView showsHorizontalScrollIndicator={false}  style={{ width: '100%' }}>
          
          <Text style={{ color: 'white' }}>{this.state.aula.mensagem}</Text>
            
        </ScrollView >
      </View>
      <View style={{ width: '100%', height: 40, marginTop: 10, paddingLeft: 20, paddingRight: 5, }}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style={{ width: '100%', flexDirection: 'row' }}>
          
          {this.renderAssuntos()}
            
        </ScrollView >
      </View>

      <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 15  }}>
        <TouchableOpacity style={{  }} onPress={() => this.joinAula()  } >
          <Text style={{ fontSize: 18, color: '#29A9AB' ,fontFamily: 'Montserrat-Regular' }}>{this.state.tooltip2}</Text>
        </TouchableOpacity>
      </View>  
      
    </View>
  );

  startTimer = () => {
    this.clockCall = setInterval(() => {
     this.decrementClock();
    }, 1000);
  }
   
  decrementClock = () => {  
     
    this.setState((prevstate) => ({ 
        timer: prevstate.timer-1
    }), () => {
        if(this.state.timer === 0 || this.state.timer < 0) {
          clearInterval(this.clockCall);
          this.closeModal();
        } 
    })
  };

  render(){
    
    return (
        <View style={{ flex: 1}}>
            <Modal
              isVisible={this.state.visibleModal === 'oportunidade'}
              onBackdropPress={() => this.closeModal() }
              useNativeDriver={true}
              animationIn="zoomInDown"
              animationOut="zoomOutUp"
              animationInTiming={300}
              animationOutTiming={600}
              backdropTransitionInTiming={1000}
            >
              {this.renderModalIncomingAulaContent()}
            </Modal>
        </View>
      );
  } 
};

Incoming.propTypes = {
  myFunc: propTypes.func
};

export default Incoming;

const styles = StyleSheet.create({
  container: {
      
    flexDirection: 'column',
    alignItems: 'stretch',
    
    paddingTop: 0,
    paddingLeft: 20,
    marginTop: 0
  },
  
});
